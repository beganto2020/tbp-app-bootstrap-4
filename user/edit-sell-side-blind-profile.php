<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Edit Sell side Blind Profile  </span></h4>
            </div>

             <div class="heading-elements">
              <div class="heading-btn-group">
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
		<div class="content">
								<div class="row">
								
								<div class="col-md-8">
								<p class="company-name" contenteditable="true">Salesforce.com (SFDC) Q4 FY19</p>
							<br/>
								
								
								<p  contenteditable="true">Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
								
								<div class="col-md-4">
								<h3 class="company-name">Business Details</h3>
									<ul  contenteditable="true">
									<li><span class="text-semibold">HQ. :</span> US East</li>
									<li><span class="text-semibold">Offices :</span> US, India, Latin America</li>
									<li><span class="text-semibold">Employees :</span> 200+</li>
									</ul>
								</div>
								
									<div class="col-md-6">
									<h3 class="company-name">Service Offerings</h3>
									<div  contenteditable="true">
									
									<li>Product Devlopment, testing and Maintenance</li>
									<li>Security & infrastructure Services</li>
									<li>Prepackaged software implementation and Support</li>
									<li>Digital Assets Management</li>
									<li>Microsoft, Oracle, and SAP Maintenance</li>
									<li>Software devlopment  & Maintenance</li>
									
									</ul></div>
									
									
										<h3 class="company-name">Customers</h3>
										<div contenteditable="true">
									<ul>
									<li>100+ mid market and enterprise customers </li>
									<li>Marquee accounts, including fortune 500 companies </li>
									<li>Primary verticle
									<ul>
									<li>BFSI</li>
									<li>Education</li>
									<li>Health Care</li>
									</ul>
									
									</li>
									
									</ul>
									
									</div></div>
									
									<div class="col-md-6">
									
									
										<table class="table">
											<h3 class="company-name">Financials</h3>
											
											<tr>
											<td style="border:none;"></td>
												<td style="border:none; font-size:24px;"><b><u>2017</u></b></td>
												</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Revenue :</span></td>
												<td contenteditable="true">$110m - $120m</td>
											</tr>
											<tr>
												<td><span class="text-semibold"> Revenue Growth : </span></td>
												<td contenteditable="true">10%YoY </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Gross Margin :	</span></td>
												<td contenteditable="true">30-40%  </td>
											</tr>
												<tr>
												<td><span class="text-semibold"> EBITDA Margin :</span></td>
												<td contenteditable="true">5-10%</td>
											</tr>
												
										
										</table>
										
										
										
										
										
										<h3 class="company-name">Investment Consideration</h3>
										
										<div contenteditable="true"><ul>
										<li>Diversified Offerings</li>
										<li>No Concentration, Largest customer is <10% of revenue</li>
										<li>Founder owned and operated</li>
										<li>Capital efficent, No Outside Capital</li>
										<li>40% recurring revenue</li>
										<li><b>Scale:</b> Sizeable enough to win deals in the mid-market but there are many oppourtunities to scale further via M&amp;A</li>
										
										<li><b>Profitable growth:</b> The company can improve this by partnering with the right private equity frim</li> 
										</ul></div>
										
									</div>
										
											
								</div>
 <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a href="edit-sell-side-blind-profile.php" class="btn btn-success a-btn-slide-text">
        <i style="color:#fff; padding-right:15px; font-size:15px; text-align:center;" class="fa fa-floppy-o" aria-hidden="true"></i>
        <span><strong>Update</strong></span>            
    </a>
						
						
	
	
                </div>
            </div>
        </div> 
							</div>
                        

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   