<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Company Information</span> </h4>
            </div>

              <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                   
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                <div class="panel panel-flat">
                    	<div class="panel-heading">
							
                        <div class="panel-body">
						

									<div class="steps-basic" action="#" >
						

							<img src="../images/comapny-information.jpg" class="img-responsive"/>



							<div class=" content">
							    <div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="display-block text-semibold">Company Description <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="Describe the company in a few sentences without disclosing the Company identity"><i class="fa fa-info-circle"></i></a></label>
									<textarea rows="5" cols="5" class="form-control"></textarea>
								</div>
							</div>
						  </div>

						  <div class="row" style="margin-top: 10px;">
							<div class="col-md-4">
								<div class="form-group">
									<label class="display-block text-semibold">What region is the company’s HQ locate in? <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="This will be shared in the blind profile"><i class="fa fa-info-circle"></i></a></label>
									<input type="text" name="" class="form-control">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label class="display-block text-semibold">Fiscal year end <sup>*</sup> </label>
									<select class="form-control">
													<option value=''>--Select Month--</option>
													<option value='1'>Janaury</option>
													<option value='2'>February</option>
													<option value='3'>March</option>
													<option value='4'>April</option>
													<option value='5'>May</option>
													<option value='6'>June</option>
													<option value='7'>July</option>
													<option value='8'>August</option>
													<option value='9'>September</option>
													<option value='10'>October</option>
													<option value='11'>November</option>
													<option value='12'>December</option>
												</select>
								</div>
							</div>
								<div class="col-md-4">
									<div class="form-group">
	<label class="display-block text-semibold">Have the historical financials been audited?  <sup>*</sup></label>
		<span>&nbsp;</span>
		<label class="radio-inline">
		<input type="radio" name="radio1" class="styled" checked="checked">
		Yes
		</label>

		<label class="radio-inline">
		<input type="radio" name="radio1" class="styled">
			No
		</label>

					</div>
							</div>
						  </div>
						    <div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="display-block text-semibold">Company financials (in USD)<sup>*</sup></label>
									<p class="content-group-sm">Enter both Revenue and EBITDA for at least the last twelve months (LTM) or the most recent year of actuals.<br>Enter figures in millions (for example, 250,000 would be 0.25M).</p>
								</div>
							</div>
						  </div>
						  		<div class="row">
							   <div class="col-md-8" > 

						  <div class="row">
							<div class="col-md-2">&nbsp;</div>
							<div class="col-md-2"><strong>FY 2018 Actuals</strong></div>
							<div class="col-md-2"><strong>FY 2019 Actuals</strong></div>
							<div class="col-md-2"><strong>FY 2020 Projected</strong></div>
						  </div>
						  <div class="row">
							<div class="col-md-2"><strong>Revenue</strong></div>
							<div class="col-md-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">$</span>
										<input type="text" class="form-control">
										<span class="input-group-addon">M</span>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">$</span>
										<input type="text" class="form-control">
										<span class="input-group-addon">M</span>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">$</span>
										<input type="text" class="form-control">
										<span class="input-group-addon">M</span>
									</div>
								</div>
							</div>
						  </div>
						  <div class="row">
							<div class="col-md-2"><strong>EBITDA</strong></div>
							<div class="col-md-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">$</span>
										<input type="text" class="form-control">
										<span class="input-group-addon">M</span>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">$</span>
										<input type="text" class="form-control">
										<span class="input-group-addon">M</span>
									</div>
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">$</span>
										<input type="text" class="form-control">
										<span class="input-group-addon">M</span>
									</div>
								</div>
							</div>
						  </div>



</div>
		<div class="col-md-4">
				
							<div class="form-group">
	<label class="display-block text-semibold">What is your relationship to this company?  <sup>*</sup></label>
		<span>&nbsp;</span>
		<label class="radio-inline">
		<input type="radio" name="radio-inline-left1" class="styled" checked="checked">
		CEO/CFO
		</label>

		<label class="radio-inline">
		<input type="radio" name="radio-inline-left1" class="styled">
		Investor/Board Member
		</label>
		
		<label class="radio-inline" style="margin-left: 10px;">
		<input type="radio" name="radio-inline-left1" class="styled">
			Advisor
		</label>
		<label class="radio-inline">
		<input type="radio" name="radio-inline-left1" class="styled">
		Other
		</label>

					</div>
		</div>			
		</div>	
						

					  	<div class="row">
										  		
											<div class="col-md-6">
												<div class="form-group">
												<label  class="display-block text-semibold">Target Verticals <sup>*</sup></label>
												<label>In which industries do you have strong customer success and presence</label>
												<fieldset class="custom-fieldset">
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														Agriculture
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Manufacturing
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Fintech & BFSI
													</label>
														
													<label class="checkbox-inline">
														<input type="checkbox">
														Engineering & Construction
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Education
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Telecomm
													</label>
													
												</fieldset>
											</div>
											</div>
											<div class="col-md-6">
												<label>&nbsp;</label>
													<br>
													<br>
													<fieldset class="custom-fieldset">
													<legend>Hardware Ecosystems</legend>
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														IBM
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														VMWare
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Dell
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														EMC
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Cisco
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Juniper
													</label>
													
												</fieldset>


											</div>
										</div>

											<div class="row">
												<div class="col-md-6">
													<label class="display-block text-semibold">Industry Classification <sup>*</sup></label>
												<label class="display-block text-semibold">What is the best way to classify your Company <sup>*</sup>  <a href="#" data-toggle="tooltip" data-placement="top" title="Which industries do you have strong customer success and presence"><i class="fa fa-info-circle"></i></a></label>

														<fieldset class="custom-fieldset">
													<legend>Software</legend>
													<span style="display-inline; margin-right: 15px;"><a href="#" data-toggle="modal" data-target="#modal-software-tree"><!-- <img src="assets/images/Hierarchy.png" width="40" height="40"> --></a></span>
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														IT
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Cloud Infrastructure
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														CRM
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Data Analytics
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														HR/HCM
													</label>
													
													
													<label class="checkbox-inline">
														<input type="checkbox">
														Supply Chain
													</label>
													
												</fieldset>
												</div>
												<div class="col-md-6">
													<label>&nbsp;</label><br>
													<label>&nbsp;</label>

														<fieldset class="custom-fieldset">
													<legend>IT Services</legend>
													<span><a href="#" data-toggle="modal" data-target="#modal-it-tree"></a></span>
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														IT
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														CRM
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														HR/HCM
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Supply Chain
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Cloud
													</label>
													<label class="checkbox-inline">
														&nbsp;
													</label>
													
												</fieldset>
												</div>





										
										</div>

							</fieldset>
							
							
							
						</form>
							
	                
                        
                        </div>
		
		  <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a class="btn btn-success" href="create_sell_side_screen-details.php" role="button">Previous</a>
						
						<a class="btn btn-success mr-1" href="create_sell_side_screen-preview.php" role="button">Next</a>
                </div>
            </div>
        </div>     
                        	
                        </div>
                    </div>
					



					
</div></div></div>
					

<?php require_once('footer.php'); ?>