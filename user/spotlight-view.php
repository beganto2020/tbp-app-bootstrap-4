<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Spotlights View </span> - Beganto</h4>
            </div>

             <div class="heading-elements col-md-3">
							
						</div>
          </div>
		
		
            <div class="">
                
                    <div class="panel panel-flat">
				
                    	<div class="panel-heading">
                    <div class="content ibox-content">
		 



      <div class="col-md-6"><img class="spotlight-img" alt="" src="http://truebluepartners.com/wp-content/uploads/2019/07/salesforce.png"></div>
       <div class="col-md-6"><img  class="spotlight-img-right" alt="" src="http://truebluepartners.com/wp-content/uploads/2019/07/mapanything.png"/></div>
  

<h1>Salesforce Acquires MapAnything</h1>

<h2>Deal Financials</h2>
<a href=""><img src="../images/pdf.png" style="max-width:50px;float: right"></a>
<p><strong>Total Consideration to Shareholders ($m):</strong> 225.0</p>

<p><strong>Implied Equity Value ($m):</strong> 261.6</p>

<h2>Transaction Overview</h2>

<ul>
  <li>On April 17, 2019, Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</li>
  <li>MapAnything previously raised over $82.9 million of equity capital in three rounds of equity financing from notable investors including Salesforce Ventures, ServiceNow Ventures and GM Ventures.</li>
  <li>On November 16, 2018, during a series C funding round lead by GM Ventures, the company&rsquo;s pre-money valuation stood at $220 million prior to receiving $42.5 million, representing a post valuation of $262.5 million.</li>
</ul>

<h2>MapAnything Overview</h2>

<ul>
  <li>Founded in 2009 by CEO John Stewart, MapAnything is based in Charlotte, North Carolina with roughly 150 employees.</li>
  <li>The company&rsquo;s MapAnything Platform leverages mapping and optimization technologies to integrate map-based visualization, route optimization and asset tracking to improve the efficiency and productivity of field sales and service teams across numerous enterprises.</li>
  <li>MapAnything has a large customer base totaling over 1,900 companies in industries such as financial services, healthcare, technology, retail, manufacturing, nonprofits and the public sector.</li>
</ul>

<h2>Salesforce&rsquo;s Appetite for Native &amp; Complimentary Cloud Applications</h2>

<ul>
  <li>Over the last several years, Salesforce has demonstrated a pattern of acquiring applications natively built on the Salesforce Platform with prior investments from Salesforce Ventures to enhance the capabilities of its various clouds.</li>
  <li>Along with being a Salesforce SI Partner and ISV Premier Partner, MapAnything is a native Salesforce application that received equity investments from Salesforce Ventures in all three rounds of financing.</li>
  <li>Similarly, on March 12, 2018, Salesforce announced its acquisition of CloudCraze, a provider of B2B enterprise commerce solutions built natively on the Salesforce Platform. Salesforce ventures invested in CloudCraze back in 2016 for its series A funding round.</li>
  <li>In December 2015, Salesforce announced its acquisition of another Salesforce Ventures portfolio company, SteelBrick, for $360 million. SteelBrick expanded the Sales Cloud with natively built CPQ and QTC solutions.</li>
  <li>Read more about Salesforce&rsquo;s ecosystem-focused strategy in our recent report, Investments and M&amp;A Trends in the Salesforce Ecosystem.</li>
</ul>

<h2>Salesforce Transaction History</h2>





<img alt="" src="http://truebluepartners.com/wp-content/uploads/2019/07/tbl-11072019-1.jpg"  class="img-fluid" width="100%" />
<a href=""><img src="../images/pdf.png" class="img-reponsive" style="max-width:50px;float: right"></a>
</div>

</div>
                    </div></div></div>
                
       
<?php require_once('footer.php'); ?>

   