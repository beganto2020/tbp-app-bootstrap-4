<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Deal Tracker  </span> - Beganto</h4>
            </div>

             <div class="heading-elements">
              <div class="heading-btn-group">
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table dataTables-example" >
                    <thead>
                          <tr>
                            <th><strong>Date</strong></th>
                            <th> <strong>Buyers/Investors</strong></th>
                            <th> <strong>Target/Issuer</strong></th>
                            <th> <strong>Type</strong></th>
                            
                            <th> <strong>Transaction Value</strong></th>
                            <th> <strong>EV/REV</strong></th>
                            <th> <strong>EV/EBITDA</strong></th>
                            <th> <strong>Classification</strong></th>
                            <th> <strong>Action</strong></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
						  <td>02/23/2020</td>
                            <td>Trifecta Serpentine , Technologies Limited</td>
                            <td>Passman SAS</td>
                            <td>Private Placement</td>
                          
                            <td>11.6</td>
                            <td>2.3</td>
                            <td>4</td>
                              <td>
                                <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                                  <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                                  <li>Software</li>
                                  <li>CRM</li>
                                  <li>Events</li>
                                  <li>Utilities</li>
                                  <li class="active">Workday</li>
                                </ul>
                                <br>
                                <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                                  <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                                  <li>Software</li>
                                  <li>IT</li>
                                  <li>SAP</li>
                                  <li>Utilities</li>
                                  <li class="active">Workday</li>
                                </ul>
                                <a href="#" data-toggle="modal" data-target="#exampleModal3" data-popup="tooltip" title="Alter Classification"><i class="icon-tree6"></i></a>
                            </td>
                              <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href=""> View Similar Deals </a></li>
                                                      <li><a href="spotlights.html">View Spotlight</a></li>
                                                        
                                                    </ul>
                                            </li>
                                        </ul>
                                    </td>
                          </tr>
                          <tr>
                            <td>02/23/2020</td>
                            <td>Serpentine Technologies Limited</td>
                            <td>Whispr AI IVS</td>
                            <td>Merger/Acquisition</td>
                     
                            <td>11.6</td>
                            <td>2.3</td>
                            <td>4</td>
                                   <td>
                                <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                                  <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                                  <li>Software</li>
                                  <li>CRM</li>
                                  <li>Events</li>
                                  <li>Utilities</li>
                                  <li class="active">Workday</li>
                                </ul>
                                <br>
                                <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                                  <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                                  <li>Software</li>
                                  <li>IT</li>
                                  <li>SAP</li>
                                  <li>Utilities</li>
                                  <li class="active">Workday</li>
                                </ul>
                                <a href="#" data-toggle="modal" data-target="#exampleModal3" data-popup="tooltip" title="Alter Classification"><i class="icon-tree6"></i></a>
                            </td>
                                <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href=""> View Similar Deals </a></li>
                                                      <li><a href="spotlights.html">View Spotlight</a></li>
                                                       
                                                    </ul>
                                            </li>
                                        </ul>
                                    </td>
                          </tr>
                        </tbody>
													
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   