<!----View sell side screen--->


<!-- Modal -->
  <div class="modal fade matches" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header  green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways</h4>
        </div>
        <div id="exTab2">	
<ul class="nav nav-tabs">
			<li class="active">
        <a  href="#sell-tab1" data-toggle="tab">Blind Profile</a>
			</li>
			<li><a href="#sell-tab2" data-toggle="tab">Activity</a>
			</li>
			<li><a href="#sell-tab3" data-toggle="tab">Comments</a>
			<li><a href="#sell-tab4" data-toggle="tab">Documents</a>

			</li>
		</ul>

			<div class="tab-content ">
			  <div class="tab-pane active" id="sell-tab1">
          <div class="content" style="margin-top:20px;">
								<div class="row">
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-12">
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
							<br/>
								
								
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
								
								<div class="col-md-6">
								<h3 class="company-name">Business Details</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US East</li>
									<li><span class="text-semibold">Offices :</span> US, India, Latin America</li>
									<li><span class="text-semibold">Employees :</span> 200+</li>
									</ul>
								
									<h3 class="company-name">Service Offerings</h3>
									<div>
									
									<li>Product Devlopment, testing and Maintenance</li>
									<li>Security & infrastructure Services</li>
									<li>Prepackaged software implementation and Support</li>
									<li>Digital Assets Management</li>
									<li>Microsoft, Oracle, and SAP Maintenance</li>
									<li>Software devlopment  & Maintenance</li>
									
									</ul></div>
									
									
										<h3 class="company-name">Customers</h3>
										<div>
									<ul>
									<li>100+ mid market and enterprise customers </li>
									<li>Marquee accounts, including fortune 500 companies </li>
									<li>Primary verticle
									<ul>
									<li>BFSI</li>
									<li>Education</li>
									<li>Health Care</li>
									</ul>
									
									</li>
									
									</ul>
									
									</div></div>
									
									<div class="col-md-6">
									
									
										<table class="table">
											<h3 class="company-name">Financials</h3>
											
											<tr>
											<td style="border:none;"></td>
												<td style="border:none; font-size:24px;"><b><u>2017</u></b></td>
												</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Revenue :</span></td>
												<td>$110m - $120m</td>
											</tr>
											<tr>
												<td><span class="text-semibold"> Revenue Growth : </span></td>
												<td>10%YoY </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Gross Margin :	</span></td>
												<td>30-40%  </td>
											</tr>
												<tr>
												<td><span class="text-semibold"> EBITDA Margin :</span></td>
												<td>5-10%</td>
											</tr>
												
										
										</table>
										
										
										
										
										
										<h3 class="company-name">Investment Consideration</h3>
										
										<div><ul>
										<li>Diversified Offerings</li>
										<li>No Concentration, Largest customer is <10% of revenue</li>
										<li>Founder owned and operated</li>
										<li>Capital efficent, No Outside Capital</li>
										<li>40% recurring revenue</li>
										<li><b>Scale:</b> Sizeable enough to win deals in the mid-market but there are many oppourtunities to scale further via M&amp;A</li>
										
										<li><b>Profitable growth:</b> The company can improve this by partnering with the right private equity frim</li> 
										</ul></div>
										
									</div>
										
											
								</div>
							</div>
		  
		  
		  
		  </div>



				<div class="tab-pane" id="sell-tab2">
          <table class="table table-togglable">
												<thead>
													<tr>
												
														<th>Date</th>
														<th>Name</th>
														<th>Email Id</th>
														<th>Status</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr>
													
													<td>11-02-2020</td>
													<td>jhone</td>
													<td>jhone@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													

<tr>
													
													<td>05-04-2020</td>
													<td>Taden Paul</td>
													<td>paul@example.com</td>
													<td><span class="badge badge-default">Draft</span></td>
													
														
													</tr>
<tr>
													
													<td>08-17-2020</td>
													<td>Harry</td>
													<td>Harry@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>03-03-2019</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													<tr>
													
													<td>03-03-2019</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-18-2020</td>
													<td>Jhone trend</td>
													<td>trend@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													</table>

				</div>
        <div class="tab-pane" id="sell-tab3">
          <div class="panel-body">
              <textarea class="form-control" placeholder="Enter Your here Comments..." rows="3"></textarea><br>
              <a href="#" class="btn btn-success btn-sm pull-right">Comment</a>
              <div class="clearfix"></div>
              <hr>
              <ul class="media-list">
                <li class="media">
                
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Rexona Kumi</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, <a href="#"># consectetur adipiscing</a> .</p>
                  </div>
                </li>
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ John Doe</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor <a href="#"># ipsum dolor</a> adipiscing elit.</p>
                  </div>
                </li>
                <li class="media">
                
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Madonae Jonisyi</strong>
                    <p>Lorem ipsum dolor <a href="#"># sit amet</a> sit amet, consectetur adipiscing elit.</p>
                  </div>
                </li>
              </ul><span class="text-danger">237K users active</span>
            
          </div>
				</div>
				
			<div class="tab-pane" id="sell-tab4">
<table class="table">
              <thead>
                <tr >
                  <th style="font-weight: bolder;">Date</th>
                  <th style="font-weight: bolder;">Title</th>
                  
                  <th style="font-weight: bolder;">Download</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>05-04-2020</td>
                  <td>Salesforce.com turnover documents of 2017</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				<tr>
                  <td>08-17-2020</td>
                  <td>Salesforce.com updatted list of company management</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
                <tr>
                  <td>04-26-2020</td>
                  <td>List of company stock holders</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				
                  <tr>
                  <td>02-11-2020</td>
                  <td>Documents of market exchange reports</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
              </tbody>
            </table>


</div>			
				
				
			</div>
        
      </div>
      <div class="modal-footer">
	  <button type="button" style="background-color:#000; color:#fff;" class="btn btn-default"><i class="fa fa-ban" aria-hidden="true"></i> Decline</button>
        <button type="button" style="background-color:green; color:#fff;" class="btn btn-default"><i class="fa fa-pencil"  aria-hidden="true"></i> Signed NDA</button>
      </div>
    </div>
	
	
  </div> </div>

</div></div></div>
					<!-- /basic modal -->



 <div class="modal fade matches" id="myModal12" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header  green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Up with TBP Finder Fee agreement</h4>
        </div>
      <div class="modal-body">
        
      <form class="well form-horizontal" action=" " method="post"  id="contact_form">
<fieldset>

<!-- Form Name -->


<!-- Text input-->

<div class="form-group">
   
  <div class="col-md-12 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-user"></i></span>
  <input  name="first_name" placeholder="First Name" class="form-control"  type="text">
    </div>
  </div>
</div>



  <div class="form-group"> 
  
    <div class="col-md-12 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-list"></i></span>
    <select style="display:block !important;" name="department" class="form-control selectpicker">
      <option value="">Select your Business</option>
      <option>Agriculture  </option>
      <option>Manufacturing</option>
      <option>Fintech & BFSI  </option>
      <option>Engineering & Construction  </option>
      <option>Education  </option>
      <option>Telecomm</option>
      <option>Software Services &amp; IT department</option>
      
    </select>
  </div>
</div>
</div>
  


<!-- Text input-->

<div class="form-group">
  
    <div class="col-md-12 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
  <input name="user_password" placeholder="Password" class="form-control"  type="password">
    </div>
  </div>
</div>

<!-- Text input-->

<div class="form-group">
   
    <div class="col-md-12 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
  <input name="confirm_password" placeholder="Confirm Password" class="form-control"  type="password">
    </div>
  </div>
</div>

<!-- Text input-->
       <div class="form-group">
   
    <div class="col-md-12 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
    </div>
  </div>
</div>


<!-- Text input-->
       
<div class="form-group">
  
    <div class="col-md-12 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
  <input name="contact_no" placeholder="(639)" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- Select Basic -->

<!-- Success message -->
<div class="alert alert-success" role="alert" id="success_message">Success <i class="fa fa-thumbs-up"></i> Success!.</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-12"><br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="fa fa-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
  </div>
</div>

</fieldset>
</form>
</div>
	  
	  
      </div>
     
    </div></div>
	
	<!----View sell side screen--->	
	
	
	<!--- buy side screen----->
	
	<!-- Modal -->
  <div class="modal fade matches" id="buymyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header  green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Velocity Media Partners limited Requires well capitalized firm</h4>
        </div>
        <div id="exTab2">	
<ul class="nav nav-tabs">
			<li class="active">
        <a  href="#buy-tab1" data-toggle="tab">Blind Profile</a>
			</li>
			<li><a href="#buy-tab2" data-toggle="tab">Activity</a>
			</li>
			<li><a href="#buy-tab3" data-toggle="tab">Comments</a>
			<li><a href="#buy-tab4" data-toggle="tab">Documents</a>

			</li>
		</ul>

			<div class="tab-content ">
			  <div class="tab-pane active" id="buy-tab1">
          <div class="content">
								<div class="row">
								<br/>
								<div class="col-md-12"><p style="float:right;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></p></div>
								<div class="col-md-12">
								
								
								<h3> Project Brilliant is a 2000+ well captilized global digital transformation service firm.</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
								
								
								<div class="col-md-12">
								<h2 class="company-name">Company Financials</h2>
								<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>
								</div>
								
								</div>
								<div class="row">
								
								
									<div class="col-md-6">
									<h3>Target Offerings</h3>
									<ul>
									
									<li>Cloud Services
									<ul>
									<li>PaaS infrastructure
									<ul>
									<li>Microsoft Stack</li></ul></li>
									
									<li>SaaS cloud ERP infrastructure
									<ul>
									<li>SAP Hana</li>
									<li>Success factor</li>
									<li>Workday</li>
									<li>Salesforce</li></ul></li>
									
									<li>SaaS Cloud verticle Applications
									<ul>
									<li>BFSI</li>
									<li>CPG</li>
									<li>Retail</li></ul></li></ul></li>
									
									
									</ul>
									
									
									
										
										<h3>Targets Verticals</h3>
										
										<ul>
										<li>Technology</li>
										<li>BFSI</li>
										<li>Retail</li>
										<li>CPG</li>
										<li>Utilities</li>
										
										</ul>
										
										
									
									
									
										
									</div>
									
									<div class="col-md-6">
									
										
										<h3>Targets Customers</h3>
									<ul>
									<li>US Based </li>
									<li>Global 2000 companies </li>
									<li>Referenceable Client Base</li>
									
									</ul>
										
									
									<h3>Target Business</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US </li>
									
									</ul>
										
										<h3>Investment considerations</h3>
										
										<ul>
										<li>Owner/Founder run companies</li>
										<li>Preferable no instituional capital</li>
										<li>Managment team to stay for at least 2 years post transaction</li>
										<li>Company should have well trained Empolyees</li>
										<li>Company should had great reputation in market</li>
										<li>Company should be in growth turnover</li>
										
										
										</ul>
										
									</div>
									
									
										
											
								</div>
				</div></div>



				<div class="tab-pane" id="buy-tab2">
          <table class="table table-togglable">
												<thead>
													<tr>
												
														<th>Date</th>
														<th>Name</th>
														<th>Email Id</th>
														<th>Status</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr>
													
													<td>11-02-2020</td>
													<td>jhone</td>
													<td>jhone@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													

<tr>
													
													<td>09-21-2020</td>
													<td>Taden Paul</td>
													<td>paul@example.com</td>
													<td><span class="badge badge-default">Draft</span></td>
													
														
													</tr>
<tr>
													
													<td>10-21-2020</td>
													<td>Harry</td>
													<td>Harry@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-17-2020</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													<tr>
													
													<td>08-17-2020</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-17-2020</td>
													<td>Jhone trend</td>
													<td>trend@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													</table>

				</div>
        <div class="tab-pane" id="buy-tab3">
          <div class="panel-body">
              <textarea class="form-control" placeholder="Enter Your here Comments..." rows="3"></textarea><br>
              <a href="#" class="btn btn-success btn-sm pull-right">Comment</a>
              <div class="clearfix"></div>
              <hr>
              <ul class="media-list">
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Rexona Kumi</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, <a href="#"># consectetur adipiscing</a> .</p>
                  </div>
                </li>
                <li class="media">
                 
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ John Doe</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor <a href="#"># ipsum dolor</a> adipiscing elit.</p>
                  </div>
                </li>
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Madonae Jonisyi</strong>
                    <p>Lorem ipsum dolor <a href="#"># sit amet</a> sit amet, consectetur adipiscing elit.</p>
                  </div>
                </li>
              </ul><span class="text-danger">237K users active</span>
            
          </div>
				</div>
				
			<div class="tab-pane" id="buy-tab4">
<table class="table">
              <thead>
                <tr >
                  <th style="font-weight: bolder;">Date</th>
                  <th style="font-weight: bolder;">Title</th>
                  
                  <th style="font-weight: bolder;">Download</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>08-17-2020</td>
                  <td>Required turnover documents of 2017</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				<tr>
                  <td>10-20-2020</td>
                  <td>shared terms & condition document</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
                <tr>
                  <td>09-21-2020</td>
                  <td>List of company stock holders</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				
                  <tr>
                  <td>10-20-2020</td>
                  <td>Required stock market exchange reports</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
              </tbody>
            </table>


</div>			
				
				
			</div>
        
      </div>
      <div class="modal-footer">
	  <button type="button" style="background-color:#000; color:#fff;" class="btn btn-default"><i class="fa fa-ban" aria-hidden="true"></i> Decline</button>
        <button type="button" style="background-color:green; color:#fff;" class="btn btn-default"><i class="fa fa-pencil"  aria-hidden="true"></i> Signed NDA</button>
      </div>
    </div>
	
	
  </div> </div>


					
 
</div>

<!---buy side screen--->
