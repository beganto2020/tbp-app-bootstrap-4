<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-6">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">View Buy Side Screens</span></h4>
            </div>

             <div class="heading-elements col-md-6">
              <div class="heading-btn-group">
			  <div class="form-group thanks-button text-right">
                        
						<div class="dropdown float-right mr-1">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Create a New Screen
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="create_sell_side_screen.php">Sell-Side Screen</a>
							<a class="dropdown-item" href="create_buy_side_screen.php">Buy-Side Screen</a>
						  </div>
						</div>
						<a class="btn  btn-default float-right mr-1" href="sell-side-blind-profile.php" role="button">Create Blind Profile</a>
						
                </div>
			  
			  
                              
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
				
	
					<div class="panel panel-flat">
                    	<div class="panel-heading">
							
                        
						<div>
 

<div id="exTab2">
  <div class="panel-default"> 
    <div class="panel-heading">
      <div class="panel-title">
        <ul class="nav nav-tabs">
          <li>
            <a href="#1" data-toggle="tab">Screen Details</a>
          </li>
          <li class="active"><a href="#2" data-toggle="tab">Blind Profile</a>
          </li>
          <li><a href="#3" data-toggle="tab">Active Matches</a>
          </li>
		  
		  <li><a href="#4" data-toggle="tab">New Matches</a>
          </li>
		  
		  <li><a href="#5" data-toggle="tab">Archived Matches</a>
          </li>
		  
		  
        </ul>
      </div>
    </div>
    
    <div class="panel-body">
      <div class="tab-content ">
        <div class="tab-pane" id="1">
		<div class="content">
								<div class="row">
								
								
								
								<div class="col-md-12">
								<p class="company-name">Velocity Media Partners</p>
								<p>Velocity Want to buy big capital companies with great turnover. $5M+ ARR SaaS business in construction industry</p>
								
								
								
								<h3 class="company-name">Company Description</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
									<div class="col-md-12">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold"> ideal size of the company you are targeting</span></td>
												<td>500+ </td>
											</tr>
											<tr>
												<td><span class="text-semibold">kind of debt do you provide </span></td>
												<td>Senior debt </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Are you interested in looking at turnaround/restructuring opportunities?</span></td>
												<td>Yes  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Industry classification : </span></td>
												<td>Agriculture > Cisco  </td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td>Software > IT > Cloud</td>
											</tr>
											
											
											
										
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td>Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2 class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>

						
						
					</div>
											
								</div>
</div>

							</div>
		
         
		  
		  <div class="row">
        	<div class="col-md-12">
			
			
			
			
            	<div class="form-group thanks-button text-right">
				
				
			<a href="edit-buy-side-screen.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
				
				<a href="#"><button type="button" style="background-color:#e6e5e5; color:#000;" class="btn btn-default legitRipple">Archive</button></a>
				
                </div>
            </div>
        </div> 
		  
		  
		  
        </div>
		
		
        <div class="tab-pane active" id="2">
          
		  
		  <div class="content" style="margin-bottom: 20px;">
								<div class="row" style="margin-bottom: 20px;">
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-8">
								<p class="company-name">Velocity Media Partners</p>
								<br/>
								<h3> Project Brilliant is a 2000+ well captilized global digital transformation service firm.</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
								<div class="col-md-4">
								<h2 class="company-name">Company Financials</h2>
								<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>
								</div>
								
								</div>
								<div class="row">
								
								
									<div class="col-md-4">
									<h3>Target Offerings</h3>
									<ul>
									
									<li>Cloud Services</li>
									
									<li>PaaS infrastructure</li>
									
									<li>Microsoft Stack</li>
									
									<li>SaaS cloud ERP infrastructure</li>
									
									<li>SAP Hana</li>
									<li>Success factor</li>
									<li>Workday</li>
									<li>Salesforce</li>
									
									<li>SaaS Cloud verticle Applications</li>
									
									<li>BFSI</li>
									<li>CPG</li>
									<li>Retail</li></ul>
									
									
									</div>
									
									
									<div class="col-md-4">
									
										
										<h3>Targets Verticals</h3>
										
										<ul>
										<li>Technology</li>
										<li>BFSI</li>
										<li>Retail</li>
										<li>CPG</li>
										<li>Utilities</li>
										
										</ul>
										
										<h3>Targets Customers</h3>
									<ul>
									<li>US Based </li>
									<li>Global 2000 companies </li>
									<li>Referenceable Client Base</li>
									
									</ul>
									
									
									
										
									</div>
									
									<div class="col-md-4">
									
										
										
										
									
									<h3>Target Business</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US </li>
									
									</ul>
										
										<h3>Investment considerations</h3>
										
										<ul>
										<li>Owner/Founder run companies</li>
										<li>Preferable no instituional capital</li>
										<li>Managment team to stay for at least 2 years post transaction</li>
										<li>Company should have well trained Empolyees</li>
										<li>Company should had great reputation in market</li>
										<li>Company should be in growth turnover</li>
										
										
										</ul>
										
									</div>
									
									
										
											
								</div>
<div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				<a href="edit-buy-side-blind-profile.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
				
				
                </div>
            </div>
        </div> 
							</div>
		  
		  
		  
        </div>
		
		
        <div class="tab-pane" id="3">
          <div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														<th>Download</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#buymyModal">Comvest Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal"> $1.3M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">05-04-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#buymyModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$1.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#buymyModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$3.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#buymyModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$3.5M</td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#buymyModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$3.5M</a></td>
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#buymyModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$3.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#buymyModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$1.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
												</tbody>
											</table>
										</div>
        </div>
		
		<div class="tab-pane" id="4">
          <div class="row">
											<h1>Thank You!</h1>
										<p>Now that you have created a Buy Side Screen, you will be assigned a TBP account manager who will schedule a time will walk you though the next steps.</p>
										<p>Meanwhile, to assist the process you can complete the <a href="create-buy-side-blind.php" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Click here to open form">following forms to start work on a Blind Profile</a>. Your Blind Profile will be published after review with TBP.</p>
										<div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														<th>Email</th>
														
														<th>Actions</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#buymyModal">03-11-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td>
														
														
														
														
														<a data-toggle="modal" href="#buymyModal">Sell side screen</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														
														<td> <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal12">Pursue</button>
						 
						</div></td>
														
						
						
													</tr>
													
													
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#buymyModal">10-20-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#buymyModal">buy side engagment </a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														<td> <div id="pageMessages"><button class="btn btn-success" onclick="createAlert('','your profile moved to archived.','','success',true,true,'pageMessages');">Pursue</button></div>
						  
						</td>
														
														
						
						
													</tr>
													
												</tbody>
											</table>
											
											
										</div>
								</div>
									
		
        </div>
		
		<div class="tab-pane" id="5">
          <div>
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Target<sup>,</sup>s Revenue</th>
														<th>Target<sup>,</sup>s EBITDA</th>
														<th>Active It</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#buymyModal">Comvest Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal"> $1.3M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">05-04-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#buymyModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#buymyModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#buymyModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$3.5M</td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#buymyModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$3.5M</a></td>
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#buymyModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#buymyModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#buymyModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#buymyModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#buymyModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
												</tbody>
											</table>
											</div>
        </div>
		
      </div>
    </div>
  </div>
</div>          
                        		
                        </div>
							
	               
                        		
                        </div>
                    </div>
                        		
                        </div>
                   
            
       
       
<?php require_once('footer.php'); ?>

   