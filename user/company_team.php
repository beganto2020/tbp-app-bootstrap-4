<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i><span class="text-semibold">Company Team </span> - Beganto</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

            <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                                <a href="spotlight-create.php" class="dt-button buttons-selected btn btn-default">Add New Team Member</a>
                               
                                
                            </div>
            </div>
			
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table dataTables-example" >
                    <thead>
                     
                                
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Title</th>
                                    <th>Phone</th>
                                    <th>Role</th>
                                    
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Sunil Grover</a></td>
                                    <td>sunilgrover@gmail.com</td>
                                    <td><a href="spotlight-view.php" target="_blank">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways </a></td>
                                    <td>+1(669)231-8770</td>
                                    <td>Manager</td>
                                    
                                 
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown dropleft">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu">
                                                   
                                              
                                                     <li><a href=""> Delete</a></li>
                                                      <li><a href="#">Change Role</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_new-team-member">Invite Team Member</a></li> 
                                                       
                                                    </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                       <tr>
                                    <td>Jhonty</a></td>
                                    <td>Jhonty@gmail.com</td>
                                    <td>Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways </td>
                                    <td>+1(669)231-8770</td>
                                    <td>Marketing Manager</td>
                                    
                                 
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href=""> Delete</a></li>
                                                      <li><a href="#">Change Role</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_new-team-member">Invite Team</a></li> 
                                                       
                                                    </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                               <tr>
                                    <td>Sam paul</a></td>
                                    <td>paul@gmail.com</td>
                                    <td>Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways </td>
                                    <td>+1(669)231-8770</td>
                                    <td>Sales Manager</td>
                                    
                                 
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href=""> Delete</a></li>
                                                      <li><a href="#">Change Role</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_new-team-member">Invite Team Member</a></li> 
                                                       
                                                    </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                   <tr>
                                    <td>Sunil Grover</a></td>
                                    <td>sunilgrover@gmail.com</td>
                                    <td>Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways </td>
                                    <td>+1(669)231-8770</td>
                                    <td>Manager</td>
                                    
                                 
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href=""> Delete</a></li>
                                                      <li><a href="#">Change Role</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_new-team-member">Invite Team Member</a></li> 
                                                       
                                                    </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   