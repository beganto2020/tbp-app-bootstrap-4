<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Edit Buy side Screen </span></h4>
            </div>

             <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
		<div class="steps-basic  " action="#" >
							
							
							
							
							<div class="content">
								<div class="row">
								
								
								
								<div class="col-md-12">
								<p class="company-name">Velocity Media Partners</p>
								<p contenteditable="true">Velocity Want to buy big capital companies with great turnover. $5M+ ARR SaaS business in construction industry</p>
								
								
								
								<h3 class="company-name">Company Description</h3>
								<p contenteditable="true">Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
									<div class="col-md-12">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold"> ideal size of the company you are targeting</span></td>
												<td contenteditable="true">500+ </td>
											</tr>
											<tr>
												<td><span class="text-semibold">kind of debt do you provide </span></td>
												<td contenteditable="true">Senior debt </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Are you interested in looking at turnaround/restructuring opportunities?</span></td>
												<td contenteditable="true">Yes  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Industry classification : </span></td>
												<td contenteditable="true">Agriculture > Cisco  </td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td contenteditable="true">Software > IT > Cloud</td>
											</tr>
											
											
											
										
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td contenteditable="true">Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td contenteditable="true"> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2 class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td contenteditable="true">10-25M</td>
					
					<td contenteditable="true">$1-5M</td>
					
					</tr>
					
					
					</table>

						
						
					</div>
											
								</div>
</div>

							</div>
							
							
							<div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a href="edit-buy-side-blind-profile.php" class="btn btn-success a-btn-slide-text">
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
        <span><strong>Update</strong></span>            
    </a>
						
						
	
	
                </div>
            </div>
        </div> 

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   