<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Primary Objective </span></h4>
            </div>

             <div class="heading-elements">
              <div class="heading-btn-group">
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
				<div class="steps-basic  " action="#" >
							<img src="../images/primary-1.jpg" class="img-responsive"/>
							
							<div class="content" style="margin-top:20px; border: 1px solid #ddd; margin-bottom: 20px; padding-bottom: 30px;">
								

												<div class="row">
											<br>
							<div class="col-md-12">
								<div class="form-group">
									<label class="display-block text-semibold">What is the primary objective of this screen?<sup >*</sup></label>

<input type="radio" id="prim_obj1" name="tbp" class="styled" onclick="showData()">
  <label for="prim_obj1">I am sourcing platform investment opportunities for my firm</label><br>

  
  <input type="radio" id="prim_obj2" name="tbp" class="styled" onclick="showData()">
  <label for="prim_obj2">	I am sourcing add-on opportunities for existing companies </label><br>

  <input type="radio" id="prim_obj3" name="tbp" class="styled" onclick="showData()">
  <label for="prim_obj3">	I am sourcing equity investment opportunities</label><br>

   <input type="radio" id="prim_obj4" name="tbp" class="styled" onclick="showData()">
  <label for="prim_obj4">I am sourcing opportunities to lend</label><br>
  <!-------------------------------------- if sell business is Selected -------------------------->



</div>
	</div>
						  </div>
	</div>


							
						</div>
							
	   <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        
						
						<a class="btn btn-success float-right mr-1" href="create_buy_side_screen-details.php" role="button">Next</a>
                </div>
            </div>
        </div>             
                        		
                        </div>
                    </div>
					
                        

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   