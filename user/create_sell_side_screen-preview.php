<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Preview</span> </h4>
            </div>

              <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                   
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                <div class="panel panel-flat">
                 
                    	<div class="panel-heading">
							
                        <div class="panel-body">
						
									<div class="steps-basic  " action="#" >
							
							
							<img src="../images/preview.jpg" class="img-responsive"/>
							<div class="content">
								<div class="row">
									
								
								
								<div class="col-md-12">
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
								<p class="subtitle">Over 150,000 companies, both big and small, are growing their business with Salesforce</p>
								<p><b>Headquater : </b> Business Address. SALESFORCE TOWER 415 MISSION STREET 3RD FL. SAN FRANCISCO CA 94105.</p>
								
								
								<h3 class="company-name">Company Description</h3>
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div></div>
								
									<div class="row">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold">Ideal timeline for selecting a termsheet and starting diligence?</span></td>
												<td>As soon as Possible </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Company undergoing a turnaround/restructuring? </span></td>
												<td>Yes </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Management team expect to stay on after the deal?	</span></td>
												<td>Management stay  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold">Targets Vertical area of focus : </span></td>
												<td>Software</td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td>Software > IT > Cloud</td>
											</tr>
											
											
												
											
											
											
											<tr>
												<td><span class="text-semibold">What is your relationship to this company? * </span></td>
												<td>CEO/CFO</td>
											</tr>
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td>Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2  class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Historical financials audited</b></th>
						
						<th><b>Fiscal year end</b> </th>
						
						

						</tr>
						
					<tr>
					<td>Yes</td>
					
					<td>January</td>
					
					</tr>
					
					
					</table>
	<br/><br/><br/>
						
						<table class="table table-striped table-bordered">
						<tr>
						<th>FY </th>
						<th><b>2018 Actuals</b></th>
						<th><b>2019 Actuals</b></th>
						<th><b>2020 Projected</b></th>
						

						</tr>
						
					<tr>
					<td><b>Revenue</b></td>
					<td>$13</td>
					<td>$15</td>
					<td>$18</td>
					</tr>
					
					<tr>
					<td><b>EBITDA</b></td>
					<td>$13</td>
					<td>$25</td>
					<td>$38</td>
					</tr>
					</table>
					</div>
											
								</div>

											
								</div>

							</div>


							
							
						</div>
							
	                
                <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a class="btn btn-success" href="create_sell_side_screen-company-information.php" role="button">Previous</a>
						
						<a class="btn btn-success mr-1" href="view-sell-side-screen.php#4" role="button">Finish</a>
                </div>
            </div>
        </div>            		
                        </div>
                    </div>
					


					
</div>  	
			

<?php require_once('footer.php'); ?>