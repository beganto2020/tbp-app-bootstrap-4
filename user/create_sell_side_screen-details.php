<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Screen Details</span> </h4>
            </div>

              <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                   
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                  <div class="panel panel-flat">
                    	<div class="panel-heading">
							
                        <div class="panel-body">
						

									<div class="steps-basic  " action="#" >
							 
							<img src="../images/screen-2.jpg" class="img-responsive"/>
							
							<div class=" content">
								
							    <div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="display-block text-semibold">Screen Title <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="Input a title that you want for internal reference.  This will not be shared with others in TBP network"><i class="fa fa-info-circle"></i></a></label>
									<input type="text" class="form-control">
								</div>
							</div>
								<div class="col-md-6">
								<div class="form-group">
									<label class="display-block text-semibold">External Headline <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="You can preview your screen in the next step before submitting"><i class="fa fa-info-circle"></i></a></label>
									<input type="text" class="form-control">
								</div>
							</div>
						
						  </div>

  							<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="display-block text-semibold">What is your ideal timeline for selecting a termsheet and starting diligence? <sup>*</sup></label>
									<select class="form-control">
										  <option>As soon as possible</option>
										  <option>Next 3 months</option>
										  <option>3-6 months</option>
										  <option>No immediate plans</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
	<label class="display-block text-semibold">Does the management team expect to stay on after the deal?  <sup>*</sup></label>
		<span>&nbsp;</span>
		<label class="radio-inline">
		<input type="radio" name="radio-inline-left1" class="styled" checked="checked">
		Management will stay
		</label>

		<label class="radio-inline">
		<input type="radio" name="radio-inline-left1" class="styled">
			Management will exit
		</label>

		<label class="radio-inline">
		<input type="radio" name="radio-inline-left1" class="styled">
			Flexible
		</label>

		
		</div>
							</div>
						  </div>

<div class="row">
	<div class="col-md-6">
								<div class="form-group">
									<label class="display-block text-semibold">What is your ideal timeline for selecting a termsheet and starting diligence? <sup>*</sup></label>
									<select class="form-control">
										  <option>As soon as possible</option>
										  <option>Next 3 months</option>
										  <option>3-6 months</option>
										  <option>No immediate plans</option>
									</select>
								</div>
							</div>
	<div class="col-md-6">

				<div class="form-group">
	<label class="display-block text-semibold">Is the company undergoing a turnaround/restructuring?  <sup>*</sup></label>
		<span>&nbsp;</span>
		<label class="radio-inline">
		<input type="radio" name="radio2" class="styled" checked="checked">
		Yes
		</label>

		<label class="radio-inline">
		<input type="radio" name="radio2" class="styled">
			No
		</label>

		

		
		</div>
	</div>

</div>

<div class="row">
	<div class="col-md-12">

				<div class="form-group">
	<label class="display-block text-semibold">Transaction Objective <sup>*</sup>  <a href="#" data-toggle="tooltip" data-placement="top" title="Please describe in a few words your motivations and objective for this transaction"><i class="fa fa-info-circle"></i></a></label>
		<textarea class="form-control"></textarea>

		

		
		</div>
	</div>

	
</div>
</div>

							
							
						</div>
							
	                
			
			 <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button">
                        <a class="btn btn-success" href="create_sell_side_screen.php" role="button">Previous</a>
						
						<a class="btn btn-success" href="create_sell_side_screen-company-information.php" role="button">Next</a>
                </div>
            </div>
        </div>  


					   </div>
						
			 		   
					
                    </div>
					
					
</div>
                    
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   