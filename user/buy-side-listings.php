<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Buy Side Screens</span> </h4>
            </div>

           <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                                <a href="create_buy_side_screen.php" class="dt-button buttons-selected btn btn-default legitRipple">New  </a>
                                <a href="#" class="dt-button buttons-selected btn btn-default legitRipple">Import</a>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table dataTables-example" >
                    <thead>
													<tr>
													
														<th>Title</th>
														
														<th>Objective</th>
														<th>Check Size</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														<th>Classification</th>
														<th>New Matches</th>
														<th>Active Matches</th>
														
														<th>Status</th>
														
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													
													<td><a href="view-buy-side-screen-main-screen.php">Velocity wants ERP and CRM Software company in information technology</a></td>
													
													<td><a href="view-buy-side-screen-main-screen.php">Provide Capital</a></td>
													
														
														
														<td><a href="view-buy-side-screen-main-screen.php">55</a></td>
														
														
														
														<td><a href="view-buy-side-screen-main-screen.php">$1-5M</a></td>
															       
														<td><a href="view-buy-side-screen-main-screen.php">$6m</a></td>
														
														<td><a href="view-buy-side-screen-main-screen.php">Industrials + Construction, Technology</a></td>
														
										<td><a href="view-buy-side-screen-main-screen.php"> 8</a></td>
										<td><a href="view-buy-side-screen-main-screen.php"> 18</a></td>
														
										<td><span class="badge badge-default">Active</span></td>
														
										<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
												   
												    <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>  Archive</a></li>
            <li><a class="dropdown-item" href="view-buy-side-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> New Matches</a></li>
           <li> <a class="dropdown-item" href="view-buy-side-screen-active-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Active Matches</a></li>
			<li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Blind Profile</a></li>
									   
			
                                        </ul></li></ul>
                                    </td>
													</tr>
													
									<tr class="bg-default">
													
													<td><a href="view-buy-side-screen-main-screen.php">Velocity wants ERP and CRM Software company in information technology</a></td>
													
													<td><a href="view-buy-side-screen-main-screen.php">Provide Capital</a></td>
													
														
														
														<td><a href="view-buy-side-screen-main-screen.php">55</a></td>
														
														
														
														<td><a href="view-buy-side-screen-main-screen.php">$1-5M</a></td>
															       
														<td><a href="view-buy-side-screen-main-screen.php">$6m</a></td>
														
														<td><a href="view-buy-side-screen-main-screen.php">Industrials + Construction, Technology</a></td>
														
										<td><a href="view-buy-side-screen-main-screen.php"> 8</a></td>
										<td><a href="view-buy-side-screen-main-screen.php"> 18</a></td>
														
										<td><span class="badge badge-default">Active</span></td>
														
										<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
												   
												    <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>  Archive</a></li>
            <li><a class="dropdown-item" href="view-buy-side-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> New Matches</a></li>
           <li> <a class="dropdown-item" href="view-buy-side-screen-active-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Active Matches</a></li>
			<li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Blind Profile</a></li>
												   
             
												   
			
												   
			
                                        </ul></li></ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													
													<td><a href="view-buy-side-screen-main-screen.php">Velocity wants ERP and CRM Software company in information technology</a></td>
													
													<td><a href="view-buy-side-screen-main-screen.php">Provide Capital</a></td>
													
														
														
														<td><a href="view-buy-side-screen-main-screen.php">55</a></td>
														
														
														
														<td><a href="view-buy-side-screen-main-screen.php">$1-5M</a></td>
															       
														<td><a href="view-buy-side-screen-main-screen.php">$6m</a></td>
														
														<td><a href="view-buy-side-screen-main-screen.php">Industrials + Construction, Technology</a></td>
														
										<td><a href="view-buy-side-screen-main-screen.php"> 8</a></td>
										<td><a href="view-buy-side-screen-main-screen.php"> 18</a></td>
														
										<td><span class="badge badge-default">Active</span></td>
														
										<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
												   
												    <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>  Archive</a></li>
            <li><a class="dropdown-item" href="view-buy-side-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> New Matches</a></li>
           <li> <a class="dropdown-item" href="view-buy-side-screen-active-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Active Matches</a></li>
			<li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Blind Profile</a></li>
												   
             
												   
			
												   
			
                                        </ul></li></ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													
													<td><a href="view-buy-side-screen-main-screen.php">Velocity wants ERP and CRM Software company in information technology</a></td>
													
													<td><a href="view-buy-side-screen-main-screen.php">Provide Capital</a></td>
													
														
														
														<td><a href="view-buy-side-screen-main-screen.php">55</a></td>
														
														
														
														<td><a href="view-buy-side-screen-main-screen.php">$1-5M</a></td>
															       
														<td><a href="view-buy-side-screen-main-screen.php">$6m</a></td>
														
														<td><a href="view-buy-side-screen-main-screen.php">Industrials + Construction, Technology</a></td>
														
										<td><a href="view-buy-side-screen-main-screen.php"> 8</a></td>
										<td><a href="view-buy-side-screen-main-screen.php"> 18</a></td>
														
										<td><span class="badge badge-default">Active</span></td>
														
										<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
												   
												    <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>  Archive</a></li>
            <li><a class="dropdown-item" href="view-buy-side-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> New Matches</a></li>
           <li> <a class="dropdown-item" href="view-buy-side-screen-active-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Active Matches</a></li>
			<li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Blind Profile</a></li>
												   
            
                                        </ul></li></ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													
													<td><a href="view-buy-side-screen-main-screen.php">Velocity wants ERP and CRM Software company in information technology</a></td>
													
													<td><a href="view-buy-side-screen-main-screen.php">Provide Capital</a></td>
													
														
														
														<td><a href="view-buy-side-screen-main-screen.php">55</a></td>
														
														
														
														<td><a href="view-buy-side-screen-main-screen.php">$1-5M</a></td>
															       
														<td><a href="view-buy-side-screen-main-screen.php">$6m</a></td>
														
														<td><a href="view-buy-side-screen-main-screen.php">Industrials + Construction, Technology</a></td>
														
										<td><a href="view-buy-side-screen-main-screen.php"> 8</a></td>
										<td><a href="view-buy-side-screen-main-screen.php"> 18</a></td>
														
										<td><span class="badge badge-default">Active</span></td>
														
										<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
												   
												    <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>  Archive</a></li>
            <li><a class="dropdown-item" href="view-buy-side-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> New Matches</a></li>
           <li> <a class="dropdown-item" href="view-buy-side-screen-active-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Active Matches</a></li>
			<li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Blind Profile</a></li>
												   
             
												   
			
												   
			
                                        </ul></li></ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													
													<td><a href="view-buy-side-screen-main-screen.php">Velocity wants ERP and CRM Software company in information technology</a></td>
													
													<td><a href="view-buy-side-screen-main-screen.php">Provide Capital</a></td>
													
														
														
														<td><a href="view-buy-side-screen-main-screen.php">55</a></td>
														
														
														
														<td><a href="view-buy-side-screen-main-screen.php">$1-5M</a></td>
															       
														<td><a href="view-buy-side-screen-main-screen.php">$6m</a></td>
														
														<td><a href="view-buy-side-screen-main-screen.php">Industrials + Construction, Technology</a></td>
														
										<td><a href="view-buy-side-screen-main-screen.php"> 8</a></td>
										<td><a href="view-buy-side-screen-main-screen.php"> 18</a></td>
														
										<td><span class="badge badge-default">Active</span></td>
														
										<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
												   
												    <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>  Archive</a></li>
            <li><a class="dropdown-item" href="view-buy-side-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> New Matches</a></li>
           <li> <a class="dropdown-item" href="view-buy-side-screen-active-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Active Matches</a></li>
			<li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Blind Profile</a></li>
												   
             
												   
			
												   
			
                                        </ul></li></ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													
													<td><a href="view-buy-side-screen-main-screen.php">Velocity wants ERP and CRM Software company in information technology</a></td>
													
													<td><a href="view-buy-side-screen-main-screen.php">Provide Capital</a></td>
													
														
														
														<td><a href="view-buy-side-screen-main-screen.php">55</a></td>
														
														
														
														<td><a href="view-buy-side-screen-main-screen.php">$1-5M</a></td>
															       
														<td><a href="view-buy-side-screen-main-screen.php">$6m</a></td>
														
														<td><a href="view-buy-side-screen-main-screen.php">Industrials + Construction, Technology</a></td>
														
										<td><a href="view-buy-side-screen-main-screen.php"> 8</a></td>
										<td><a href="view-buy-side-screen-main-screen.php"> 18</a></td>
														
										<td><span class="badge badge-default">Active</span></td>
														
										<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
												   
												    <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>  Archive</a></li>
            <li><a class="dropdown-item" href="view-buy-side-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> New Matches</a></li>
           <li> <a class="dropdown-item" href="view-buy-side-screen-active-screen.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i> Active Matches</a></li>
			<li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>Blind Profile</a></li>
												   
             
												   
			
												   
			
                                        </ul></li></ul>
                                    </td>
													</tr>	</tbody>
													
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   