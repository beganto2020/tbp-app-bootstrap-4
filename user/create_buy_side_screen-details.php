<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Screen Details </span></h4>
            </div>

             <div class="heading-elements">
              <div class="heading-btn-group">
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
				
		          <div class="steps-basic  " action="#" >
							
							



							<img src="../images/screen-2.jpg" class="img-responsive"/>



							<div class="content">
								<div class="row " id="hello" >

									
							
						</div>
							    <div class="row ">
							<div class="col-md-6">
								<div class="form-group">
									<label class="display-block text-semibold">Screen Title <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="Input a title that you want for internal reference.  This will not be shared with others in TBP network."><i class="fa fa-info-circle"></i></a></label>
									<input type="text" class="form-control">
								</div>
							</div>
								<div class="col-md-6">
								<div class="form-group">
									<label class="display-block text-semibold">External Headline <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="You can preview your screen in the next step before submitting."><i class="fa fa-info-circle"></i></a></label>
									<input type="text" class="form-control">
								</div>
							</div>
						
						  </div>
						  		
  							


  							
<div class="row">
	<div class="col-md-6">
								<div class="form-group">
									<label class="display-block text-semibold">What is the ideal size of the company you are targeting ? <sup>*</sup></label>
									<input type="text" name="" class="form-control">
								</div>
							</div>
	<div class="col-md-6">

				<div class="form-group">
	<label class="display-block text-semibold">	Target Revenue<sup>*</sup></label>
		<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
																<10M


													</label>

													<label class="checkbox-inline">
														<input type="checkbox">10-25M
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
																25-50M


													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
																50-100M


													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
															>100M


													</label>

		

		
		</div>
	</div>

</div>
 

 
<div class="row">
	<div class="col-md-6">

						<div class="form-group">
	<label class="display-block text-semibold">	Target EBITDA  <sup>*</sup></label>
<label class="checkbox-inline"><input type="checkbox">>$1M</label>		
<label class="checkbox-inline"><input type="checkbox">$1-5M</label>
<label class="checkbox-inline"><input type="checkbox">$5-10M</label>
<label class="checkbox-inline"><input type="checkbox">$10-20M</label>
<label class="checkbox-inline"><input type="checkbox">>$20M</label>

		
		</div>
	</div>
	
	<div class="col-md-6">

						<div class="form-group">
	<label class="display-block text-semibold">What is the typical percentage ownership you typically seek</label>
		
<label class="radio-inline"><input name="percentage" type="radio"><10%</label>
<label class="radio-inline"><input name="percentage" type="radio">10-19%</label>
<label class="radio-inline"><input name="percentage" type="radio">20-29%</label>
<label class="radio-inline"><input name="percentage" type="radio">30-39%</label>
<label class="radio-inline"><input name="percentage" type="radio">40-49%</label>
<label class="radio-inline"><input name="percentage" type="radio">>50%</label>

		
		</div>
	</div>
	
	<div class="col-md-6">

						<div class="form-group">
	<label class="display-block text-semibold">What is the dollar value range of the add on opportunity you are targeting</label>
		
<label class="radio-inline"><input name="dollarvalue" type="radio">$1-5M</label>
<label class="radio-inline"><input name="dollarvalue" type="radio">$5-10M</label>
<label class="radio-inline"><input name="dollarvalue" type="radio">$10-20M</label>
<label class="radio-inline"><input name="dollarvalue" type="radio">$20-50M</label>
<label class="radio-inline"><input name="dollarvalue" type="radio">>$50M</label>

		
		</div>
	</div>
	
	<div class="col-md-6">
								<div class="form-group">
	<label class="display-block text-semibold">Does the Company expect the targets management team to stay on after the deal?<sup>*</sup></label>
		
		<label class="radio-inline"><input name="managment" type="radio">Management will stay</label>

		<label class="radio-inline"><input name="managment" type="radio">Management will exit</label>

		<label class="radio-inline"><input name="managment" type="radio">Flexible</label>

		
		</div>
							</div>
							
							
		<div class="col-md-6">

				<div class="form-group">
	<label class="display-block text-semibold">Is the company undergoing a turnaround/restructuring?  <sup>*</sup></label>
		
		<label class="radio-inline"><input name="turn" type="radio">Yes</label>

		<label class="radio-inline"><input name="turn" type="radio">No</label>

		

		
		</div>
	</div>					

	<div class="col-md-6">
	<div class="form-group">
	<label class="display-block text-semibold">What kind of debt do you provide ?<sup>*</sup></label>

<label class="radio-inline"><input type="radio" name="radio-inline-left1" class="styled" checked="checked">	A/R financing</label>
<label class="radio-inline"><input type="radio" name="radio-inline-left1" class="styled">Junior debt</label>
<label class="radio-inline"><input type="radio" name="radio-inline-left1" class="styled" checked="checked">Mezzanine Financing</label>
<label class="radio-inline"><input type="radio" name="radio-inline-left1" class="styled" checked="checked">Senior debt</label>

	</div>
	</div>
</div>

<div class="row" style="margin-top:30px;">
	<div class="col-md-6">

						<div class="form-group">
	<label class="display-block text-semibold">	Are you interested in looking at turnaround/restructuring opportunities?  <sup>*</sup></label>
		<label class="radio-inline"><input type="radio" name="interested" checked="checked">Yes </label>
<label class="radio-inline"><input type="radio" name="interested">No</label>


		
		</div>
	</div>
<!--- ==================if Selected primary debt====------>
	<div class="col-md-6">
	
	</div>
</div>


<div class="row" style="margin-top:30px;">
	<div class="col-md-12">

						<div class="form-group">
	<label class="display-block text-semibold">	Transaction Objective <sup>*</sup></label>
	<textarea class="form-control"></textarea>

		
		</div>
	</div>

	
</div>



							</div>

						</div>
							
	                <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a class="btn btn-success" href="create_buy_side_screen.php" role="button">Previous</a>
						
						<a class="btn btn-success" href="create_buy_side_screen-company-information.php" role="button">Next</a>
                </div>
            </div>
        </div> 
                        		
                        </div>
                    </div>
                        		
                        </div>
                    </div>
					
                        

                    </div>
                
            
       
       
<?php require_once('footer.php'); ?>

   