<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Tbp-app Dashboard v.2</title>

    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.css" rel="stylesheet">

    <link href="../css/animate.css" rel="stylesheet">
    <link href="../css/style.css" rel="stylesheet">
	<link href="../css/external.css" rel="stylesheet">
	

<link href="../css/datatables.min.css" rel="stylesheet">
</head>

<body>
    <div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <img alt="image" class="rounded-circle" src="../images/avtar2.jpg"/>
                        
                            <span class="block m-t-xs font-bold">Hello User</span>
                            
                    </div>
                    <div class="logo-element">
                        <img src="../images/logo-1.png"/>
                    </div>
                </li>
                <li>
                    <a href="index.html"><i class="fa fa-th-large"></i> <span class="nav-label">My Account</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                       <li><a href="#"><i class="fa fa-user"></i> <span>My profile</span></a></li>
								<li><a href="#"><i class="fa fa-envelope"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Messages</span></a></li>
								<li class="divider"></li>
								<li><a href="#"><i class="fa fa-cog"></i> <span>Account settings</span></a></li>
								<li><a href="#"><i class="fa fa-power-off"></i> <span>Logout</span></a></li>
					
                    </ul>
                </li>
               
               
                
               
                
                
               <li><a href="index.php"><i class="fa fa-home"></i> <span>Home</span></a></li>
								<li><a href="company_profile.php"><i class="fa fa-building-o"></i> <span>Company Profile</span></a></li>
								<li><a href="company_team.php"><i class="fa fa-users"></i> <span>Team</span></a></li>  
                                <li><a href="engagement-listings.php"><i class="fa fa-briefcase"></i> <span>Engagement</span></a></li>                                
								<li><a href="buy-side-listings.php"><i class="fa fa-credit-card-alt"></i><span>Buy Side Screens</span></a></li> 
								<li><a href="sell-side-listings.php"><i class="fa fa-search-plus"></i><span>Sell Side Screens</span></a></li>
								
								<li><a href="project.php"><i class="fa fa-tasks"></i><span>Project</span></a></li>
								
								
								<li>
									<a href="deal-listings.php"><i class="fa fa-handshake-o"></i> <span>Deal Tracker</span></a>
								</li>                                
								<li>
									<a href="spotlights-listings.php"><i class="fa fa-lightbulb-o"></i> <span>Spotlights</span></a>
								</li>
								
								<li>
									<a href="#"><i class="fa fa-shopping-bag"></i> <span>Assets For Sale</span><span class="fa arrow"></span></a>
									<ul class="nav nav-second-level">
										<li><a href="list-from-tbp.html">List From TBP</a></li>
										<li><a href="active-projects.html">Active Projects</a></li>
									</ul>
								</li> 
								
								<li style="display:none;">
									<a href="activity-stream.html"><i class="fa fa-skiing-nordic"></i> <span>Activity Stream</span></a>
								</li>
								
            </ul>

        </div>
    </nav>
	
	
	