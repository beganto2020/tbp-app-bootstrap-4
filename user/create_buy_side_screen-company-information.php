<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Comapny Information</span></h4>
            </div>

             <div class="heading-elements">
              <div class="heading-btn-group">
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
				
		          <div class="steps-basic  " action="#" >
							
							

							<img src="../images/comapny-information.jpg" class="img-responsive"/>
							<div style="content">
							    <div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="display-block text-semibold">Company Description <sup>*</sup>  <a href="#" data-toggle="tooltip" data-placement="top" title="Describe the company in a few sentences without disclosing the Company identity."><i class="fa fa-info-circle"></i></a></label>
									<textarea rows="3" cols="3" class="form-control"></textarea>
								</div>
							</div>
						  </div>
	
						

					  	<div class="row" style="margin-top: 30px;">
										  		
											<div class="col-md-6">
												<div class="form-group">
												<label class="display-block text-semibold">Select the industry classification that would best match the target you are looking for <sup>*</sup></label>
												<fieldset class="custom-fieldset">
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														Agriculture
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Manufacturing
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Fintech & BFSI
													</label>
													<br/><br/>
													<label class="checkbox-inline">
														<input type="checkbox">
														Engineering & Construction
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Education
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Telecomm
													</label>
													
												</fieldset>
											</div>
											</div>
											<div class="col-md-6">
												<label>&nbsp;</label>
													<fieldset class="custom-fieldset">
													<legend>Hardware Ecosystems</legend>
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														IBM
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														VMWare
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Dell
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														EMC
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Cisco
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Juniper
													</label>
													
												</fieldset>


											</div>
										</div>

											<div class="row">
												<div class="col-md-6">
													<label class="display-block text-semibold">Targets offerings and verticals <sup>*</sup></label>
												<label class="display-block text-semibold">Do you have a preference for acquiring/investing in a company with customers in any of the following verticals  <sup>*</sup></label>

														<fieldset class="custom-fieldset">
													<legend>Software</legend>
													<span><a href="#" data-toggle="modal" data-target="#modal-software-tree"></a></span>
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														IT
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Cloud Infrastructure
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														CRM
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Data Analytics
													</label>
<br/>
													<label class="checkbox-inline">
														<input type="checkbox">
														HR/HCM
													</label>
													<label class="checkbox-inline" style="margin-left:10px;">
														<input type="checkbox">
														Supply Chain
													</label>
													
												</fieldset>
												</div>
												<div class="col-md-6">
													<label>&nbsp;</label><br>
													<label>&nbsp;</label>

														<fieldset class="custom-fieldset">
													<legend>IT Services</legend>
													<span><a href="#" data-toggle="modal" data-target="#modal-it-tree"></a></span>
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														IT
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														CRM
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														HR/HCM
													</label>
													
													<label class="checkbox-inline">
														<input type="checkbox">
														Supply Chain
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Cloud
													</label>
													
													<label class="checkbox-inline">
													<input type="checkbox">
														&nbsp;
													</label>
													
												</fieldset>
												</div>





										
										</div>

							</fieldset>
							
							
							
						</div>
							
	   <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a class="btn btn-success float-right" href="create_buy_side_screen-details.php" role="button">Previous</a>
						
						<a class="btn btn-success float-right mr-1" href="create_buy_side_screen-preview.php" role="button">Next</a>
                </div>
            </div>
        </div>              
                        		
                        </div>
							
	               
                        		
                        </div>
                    </div>
                        		
                        </div>
                    </div>
					
                        

                    </div>
                
            
       
       
<?php require_once('footer.php'); ?>

   