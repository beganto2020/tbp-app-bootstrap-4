<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Spotlights </span> </h4>
            </div>

              <div class="heading-elements">
              <div class="heading-btn-group">
                   
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table dataTables-example" >
                    <thead>
                <tr >
                  <th>Date</th>
                  <th>Title</th>
                  <th>Companies Involved</th>
                  <th>Classification</th>
                  <th>Deal</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>05/09/2018</td>
                  <td><a href="spotlight-view.php" target="_blank">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways </a></td>
                  <td><a href="company.php">Company1 </a>, <a href="company.php">Company2</a></td>
                  <td>
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>CRM</li>
                        <li>Events</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                      
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>IT</li>
                        <li>SAP</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                  </td>
                
                  <td><a href="#">Salesforce Acquire Velocity</a></td>
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Spotlight"></i></a></td>
                </tr>
                <tr>
                  <td>05/09/2018</td>
                  <td><a href="spotlight-view.php" target="_blank">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways</a></td>
                  <td><a href="company.php">Company1 </a>, <a href="company.php">Company2</a></td>
                    <td>
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>CRM</li>
                        <li>Events</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                      
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>IT</li>
                        <li>SAP</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                  </td>
                
                   <td><a href="#">Salesforce Acquire Velocity</a></td>
                
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Spotlight"></i></a></td>
                </tr>
                <tr>
                  <td>05/09/2018</td>
                  <td><a href="spotlight-view.php" target="_blank">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways </a></td>
                    <td><a href="company.php">Company1 </a>, <a href="company.php">Company2</a></td>
                    <td>
                      <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                        <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                        <li>Software</li>
                        <li>CRM</li>
                        <li>Events</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                     
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>IT</li>
                        <li>SAP</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                  </td>
                   <td><a href="#">Salesforce Acquire Velocity</a></td>
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Spotlight"></i></a></td>
                </tr>
                  <tr>
                  <td>05/09/2018</td>
                  <td><a href="spotlight-view.php" target="_blank">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways </a></td>
                  <td><a href="company.php">Company1 </a>, <a href="company.php">Company2</a></td>
                    <td>
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>CRM</li>
                        <li>Events</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                     
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>IT</li>
                        <li>SAP</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                  </td>
                   <td><a href="#">Salesforce Acquire Velocity</a></td>
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Spotlight"></i></a></td>
                </tr>
                <tr>
                  <td>05/09/2018</td>
                  <td><a href="spotlight-view.php" target="_blank">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways</a></td>
                    <td><a href="company.php">Company1 </a>, <a href="company.php">Company2</a></td>
                    <td>
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>CRM</li>
                        <li>Events</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                      
                      <ul class="breadcrumb breadcrumb-arrows">
                        <li><i class="fa fa-arrow-circle-right"></i></li>
                        <li>Software</li>
                        <li>IT</li>
                        <li>SAP</li>
                        <li>Utilities</li>
                        <li class="active">Workday</li>
                      </ul>
                  </td>
                   <td><a href="#">Salesforce Acquire Velocity</a></td>
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Spotlight"></i></a></td>
                </tr>
              </tbody>
													
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   