<?php require_once('header.php'); ?>


        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>
            <div class="wrapper wrapper-content">
        
				<div class="page-header-content">
            <div class="page-title">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">View Company Profile</span></h4>
            <a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

           
			
          </div>
		
                <div class="panel panel-flat">
				
                    	<div class="panel-heading">
							
                        
						<div>
  

<div class="container">
      <div class="row">
	  
	  <div class="col-md-4" style="margin-top:50px;">
                        <div class="card card-profile">
                            <div class="card-avatar">
                                <a href="#"> <img  class="img-responsive" src="../images/Salesforce_logo.jpg"> </a>
                            </div>
                            <div class="table">
                                <h4 class="card-caption">Salesforce.com</h4>
                                <h6 class="category text-muted">American cloud-based software company</h6>
                                
                                
							 
								
                            </div>
                        </div>
                    </div>
	  
	  
	 
            
			
			
	<div class="col-md-6">
                        <div class="card card-profile">
                          
                                
                                
							 <table class="table table-striped table-bordered">
                    <tbody>	
								<tr>
                        <td><b>Company Name:</b></td>
                        <td style="text-align:center;">Salesforce.com, inc</td>
                      </tr>
                      <tr>
                        <td><b>Company Type:</b></td>
                        <td style="text-align:center;">Operating Company</td>
                      </tr>
                      <tr>
                        <td><b>Company Phone:</b></td>
                        <td style="text-align:center;">+1(669)231-8770</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td><b>Contact Phone:</b></td>
                        <td style="text-align:center;">+1(669)231-8770</td>
                      </tr>
                        <tr>
                        <td><b>Company is interested in</b></td>
                        <td style="text-align:center;">Buy Side Projects</td>
                      </tr>
                      <tr>
                        <td><b>Address</b></td>
                        <td style="text-align:center;">99 South Almaden Blvd, Suite 600<br/>
San Jose, CA
95113
</td>
                      </tr>  </tbody>
                  </table>
								
                            </div>
                        </div>
                    </div>		
              
            </div>
                 
            
          </div>
        </div>
      </div>
    </div>

				
                
              
      

    <?php require_once('footer.php'); ?>