<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-6">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">View Sell Side Screens</span> </h4>
            </div>

             <div class="heading-elements col-md-6">
              <div class="heading-btn-group">
			  <div class="form-group thanks-button text-right">
                        
						<div class="dropdown float-right mr-1">
						  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Create a New Screen
						  </button>
						  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="create_sell_side_screen.php">Sell-Side Screen</a>
							<a class="dropdown-item" href="create_buy_side_screen.php">Buy-Side Screen</a>
						  </div>
						</div>
						<a style="margin-left:20px;" class="btn  btn-default float-right mr-1" href="sell-side-blind-profile.php" role="button">Create Blind Profile</a>
						
                </div>
			  
			  
                              
                               
                                
                            </div>
            </div>
          </div>
		
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
               
			<div class="panel panel-flat">
                    	<div class="panel-heading">
							
						<div>
 
<div id="exTab2">
  <div class="panel-default"> 
    <div class="panel-heading">
      <div class="panel-title">
        <ul class="nav nav-tabs">
          <li>
            <a href="#1" data-toggle="tab">Screen Details</a>
          </li>
          <li class="active"><a href="#2" data-toggle="tab">Blind Profile</a>
          </li>
          <li><a href="#3" data-toggle="tab">Active Matches</a>
          </li>
		  
		  <li><a href="#4" data-toggle="tab">New Matches</a>
          </li>
		  
		  <li><a href="#5" data-toggle="tab">Archived Matches</a>
          </li>
		  
		  
        </ul>
      </div>
    </div>
    
    <div class="panel panel-body">
      <div class="tab-content ">
        <div class="tab-pane" id="1">
		<div class="row">
									
								
								
								<div class="col-md-12">
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
								<p class="subtitle">Over 150,000 companies, both big and small, are growing their business with Salesforce</p>
								<p><b>Headquater : </b> Business Address. SALESFORCE TOWER 415 MISSION STREET 3RD FL. SAN FRANCISCO CA 94105.</p>
								
								
								<h3 class="company-name">Company Description</h3>
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
									<div class="col-md-12">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold">Ideal timeline for selecting a termsheet and starting diligence?</span></td>
												<td>As soon as Possible </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Company undergoing a turnaround/restructuring? </span></td>
												<td>Yes </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Management team expect to stay on after the deal?	</span></td>
												<td>Management stay  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold">Targets Vertical area of focus : </span></td>
												<td>Software</td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td>Software > IT > Cloud</td>
											</tr>
											
											
												
											
											
											
											<tr>
												<td><span class="text-semibold">What is your relationship to this company? * </span></td>
												<td>CEO/CFO</td>
											</tr>
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td>Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2  class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Historical financials audited</b></th>
						
						<th><b>Fiscal year end</b> </th>
						
						

						</tr>
						
					<tr>
					<td>Yes</td>
					
					<td>January</td>
					
					</tr>
					
					
					</table>
	<br/><br/><br/>
						
						<table class="table table-striped table-bordered">
						<tr>
						<th>FY </th>
						<th><b>2018 Actuals</b></th>
						<th><b>2019 Actuals</b></th>
						<th><b>2020 Projected</b></th>
						

						</tr>
						
					<tr>
					<td><b>Revenue</b></td>
					<td>$13</td>
					<td>$15</td>
					<td>$18</td>
					</tr>
					
					<tr>
					<td><b>EBITDA</b></td>
					<td>$13</td>
					<td>$25</td>
					<td>$38</td>
					</tr>
					</table>
					</div>
											
								</div>

											
								</div>
		
         
		  
		  <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				
				<a href="edit-sell-side-screen.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
				
				<a href="#"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Archive</button></a>
                       
						
	
                </div>
            </div>
        </div> 
		  
		  
		  
        </div>
		
		
        <div class="tab-pane active" id="2">
          
		  
		  <div class="content">
								<div class="row">
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-8">
								
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
							<br/>
								
								
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
								
								<div class="col-md-4">
								<h3 class="company-name">Business Details</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US East</li>
									<li><span class="text-semibold">Offices :</span> US, India, Latin America</li>
									<li><span class="text-semibold">Employees :</span> 200+</li>
									</ul>
								</div>
								
									<div class="col-md-6">
									<h3 class="company-name">Service Offerings</h3>
									<div>
									
									<li>Product Devlopment, testing and Maintenance</li>
									<li>Security & infrastructure Services</li>
									<li>Prepackaged software implementation and Support</li>
									<li>Digital Assets Management</li>
									<li>Microsoft, Oracle, and SAP Maintenance</li>
									<li>Software devlopment  & Maintenance</li>
									
									</ul></div>
									
									
										<h3 class="company-name">Customers</h3>
										<div>
									<ul>
									<li>100+ mid market and enterprise customers </li>
									<li>Marquee accounts, including fortune 500 companies </li>
									<li>Primary verticle
									<ul>
									<li>BFSI</li>
									<li>Education</li>
									<li>Health Care</li>
									</ul>
									
									</li>
									
									</ul>
									
									</div></div>
									
									<div class="col-md-6">
									
									
										<table class="table">
											<h3 class="company-name">Financials</h3>
											
											<tr>
											<td style="border:none;"></td>
												<td style="border:none; font-size:24px;"><b><u>2017</u></b></td>
												</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Revenue :</span></td>
												<td>$110m - $120m</td>
											</tr>
											<tr>
												<td><span class="text-semibold"> Revenue Growth : </span></td>
												<td>10%YoY </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Gross Margin :	</span></td>
												<td>30-40%  </td>
											</tr>
												<tr>
												<td><span class="text-semibold"> EBITDA Margin :</span></td>
												<td>5-10%</td>
											</tr>
												
										
										</table>
										
										
										
										
										
										<h3 class="company-name">Investment Consideration</h3>
										
										<div><ul>
										<li>Diversified Offerings</li>
										<li>No Concentration, Largest customer is <10% of revenue</li>
										<li>Founder owned and operated</li>
										<li>Capital efficent, No Outside Capital</li>
										<li>40% recurring revenue</li>
										<li><b>Scale:</b> Sizeable enough to win deals in the mid-market but there are many oppourtunities to scale further via M&amp;A</li>
										
										<li><b>Profitable growth:</b> The company can improve this by partnering with the right private equity frim</li> 
										</ul></div>
										
									</div>
										
											
								</div>
 <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				
				<a href="edit-sell-side-blind-profile.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
				
                      
                </div>
            </div>
        </div> 
							</div>
		  
		  
        </div>
		
		
        <div class="tab-pane" id="3">
          <div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														<th>Target’s Check Size</th>
														<th>Target’s Revenue</th>
														<th>Target’s EBITDA</th>
														<th>Download</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">10-20-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-5M</a></td>
															       
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$10-15M</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">08-17-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce.com (SFDC)</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Salesforce Media Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>07-01-2019</td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Nivita Construction</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">10-20-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Nivita Production</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
												</tbody>
											</table>
										</div>
        </div>
		
		<div class="tab-pane" id="4">
          <div class="row">
											<h1 style="text-align:center;margin-bottom: 20px;">Thank You!</h1>
										
										<p>Now that you have created a Sell Side Screen, you will be assigned a TBP account manager who will schedule a time will walk you though the next steps.</p>
										<p>Meanwhile, to assist the process you can complete the <a href="sell-side-blind-profile.php" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Click here to open form">following forms to start work on a Blind Profile</a>. Your Blind Profile will be published after review with TBP.</p>
										
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														<th>Target’s Check Size</th>
														<th>Target’s Revenue</th>
														<th>Target’s EBITDA</th>
														<th>Email</th>
														
														<th>Actions</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">10-20-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#myModal">Buy side screen</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-5M</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														
														<td> <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal12">Pursue</button>
						 
						</div></td>
														
						
						
													</tr>
													
													
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">10-21-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#myModal">Sell side engagment </a></td>
														
														<td><a data-toggle="modal" href="#myModal">$10-15M</a></td>
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
													<td> <div id="pageMessages"><button class="btn btn-success" onclick="createAlert('','your profile moved to archived.','','success',true,true,'pageMessages');">Pursue</button></div></td>
														
														
						
						
													</tr>
													
												</tbody>
											</table>
											
											
										</div>
								</div>
									
		

		
		<div class="tab-pane" id="5">
          <div>
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														<th>Target Check Size</th>
														<th>Target<sup>,</sup>s Revenue</th>
														<th>Target<sup>,</sup>s EBITDA</th>
														<th>Active It</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">10-20-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">08-17-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>07-01-2019</td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">10-20-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
 <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
												</tbody>
											</table>
										</div>
        </div>
		
      </div>
    </div>
  </div>
</div></div>
			
<!------->
  </div>
</div></div></div></div></div>


<?php require_once('footer.php'); ?>