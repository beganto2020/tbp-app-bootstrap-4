<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Primary Objective </span> </h4>
            </div>

              <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                   
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                  <div class="panel panel-flat">
                    	<div class="panel-heading">
							
                        <div class="panel-body">
						

									<div class="steps-basic  " action="#" >
							 
							<img src="../images/primary-1.jpg" class="img-responsive"/>
							
							<div class="content">
								

												<div class="row">
											
							<div class="col-md-12">
								<div class="form-group">
									<label class="display-block text-semibold">What is the primary objective of screen? <sup>*</sup>  <a href="#" data-toggle="tooltip" data-placement="top" title="Please Select the Primary Object Of Screen"><i class="fa fa-info-circle"></i></a></label>

<input type="radio" id="sell" name="tbp" class="styled" onclick="showData()">
  <label for="sell">Sell the business</label>	

  
  <input type="radio" id="fan" name="tbp" class="styled" onclick="showData()">
  <label for="fan">Obtain financing </label>


  <!-------------------------------------- if sell business is Selected -------------------------->
  								
<div class="Sell_busi" id="sell_Busi">

	<label class="display-block text-semibold">Are you looking for:</label>
		<label class="checkbox-inline">
			<input type="checkbox" >Strategic Partner
		</label>

		<label class="checkbox-inline">
			<input type="checkbox">
			Private Equity Partner
		</label>


</div>



  <!--------------------------------------If obtain financing Is Selected-------------------------->
<div class="Sell_busi" id="obtain_Fin">
	
	<label class="display-block text-semibold">Are you looking to raise:</label>
		<label class="checkbox-inline">
			<input type="checkbox" id="equity_check" onchange="showData()">
			Equity Financing
		</label>

		<label class="checkbox-inline">
			<input type="checkbox" id="debt_check"  onchange="showData()">
			Debt
		</label>


</div>
  <!--------------------------------------If Equity Financing is selected -------------------------->
<div class="Sell_busi" id="equity_Fin">
	<label class="display-block text-semibold">What is the approximate size of financing you are looking for:</label>
	
	<span class="display-inline text-semibold">$ Amount Range:</span>
		<label class="checkbox-inline">
			<input type="checkbox" >
			$1-5M
		</label>

		<label class="checkbox-inline">
			<input type="checkbox">
			$5-10M
		</label>
		<label class="checkbox-inline">
			<input type="checkbox">
			$10-20M
		</label>
		<label class="checkbox-inline">
			<input type="checkbox">
			$20-50M
		</label>
		<label class="checkbox-inline">
			<input type="checkbox">
			> $50M
        </label>
        <br/><br/>
		<span class="display-inline text-semibold">% Dilution:</span>
		<label class="checkbox-inline">
		<input type="radio" name="radio-inline-left1" >
		< 10%
		</label>

		<label class="checkbox-inline">
		<input  type="radio" name="radio-inline-left1" >
		10-19%
		</label>

		<label class="checkbox-inline">
		<input  type="radio" name="radio-inline-left1" >
		20-29%
		</label>

		<label class="checkbox-inline">
		<input  type="radio" name="radio-inline-left1" >
		30-39%
		</label>

		<label class="checkbox-inline">
		<input type="radio" name="radio-inline-left1" >
		40-49%
		</label>

		<label class="checkbox-inline">
		<input type="radio" name="radio-inline-left1" >
		>50%
		</label>

</div>

  <!--------------------------------------If Debt Financing is selected -------------------------->
<div class="Sell_busi" id="debt">
	<label class="display-block text-semibold">Type of debt:</label>
	
	
		<label class="checkbox-inline">
			<input type="checkbox" >
				A/R Financing
		</label>

		<label class="checkbox-inline">
			<input type="checkbox">
				Junior debt
		</label>
		<label class="checkbox-inline">
			<input type="checkbox">
				Mezzanine Financing
		</label>
		<label class="checkbox-inline">
			<input type="checkbox">
				Senior debt 
		</label>
	


</div>


</div>
	</div>
						  </div>
	</div>

							
							
						</div>
							
	                
                        		<!---------==========================================================Create Wizard End -==========================================-->
                       
			
			 <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        
						
						<a class="btn btn-success float-right mr-1" href="create_sell_side_screen-details.php" role="button">Next</a>
                </div>
            </div>
        </div>  


					   </div>
						
			 		   
					
                    </div>
					




		            <!-- Basic modal -->
					
</div>
                    
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   