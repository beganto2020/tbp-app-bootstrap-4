<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Home</span></h4>
            </div>

             <div class="heading-elements col-md-3">
							<div class="heading-btn-group">
								<div class="btn-group btn-group-velocity">
	                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Create A New Screen </button>
									<ul class="dropdown-menu">
										<li><a href="create_sell_side_screen.php">Sell-Side Screen</a></li>
											<li><a href="create_buy_side_screen.php">Buy-Side Screen</a></li>
									</ul>
	                            </div>
							</div>
						</div>
          </div>
		
		
            <div class="row">
                
                   
                    <div class="content">
		<div class="content user-dashboard">
				
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">Buy-Side Screens</h5>
								<h4 class="text-semibold"><a href="create_buy_side_screen.php"><i class="fa fa-plus-circle"></i></a></h4>

								<table class="table table-togglable table-hover">
									<thead>
										<tr>
											<th>Screen Title</th>
											<th>Status</th>
											<th>Purpose of Screen</th>
											<th>Notification</th>
										</tr>
									</thead>
									<tbody>
										<tr class="bg-default">
											<td><a href="view-buy-side-screen-main-screen.php">Velocity wants Software company</a></td>
											<td><span class="badge badge-default">DRAFT</span></td>
											<td>Raise Capital</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
										<tr>
											<td><a href="edit-buy-side-screen.html">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
<tr class="bg-default">
											<td><a href="edit-buy-side-screen.html">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>

<tr>
											<td><a href="edit-buy-side-screen.html">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
									</tbody>
								</table>
								<p class="text-right"><a href="buy-side-listings.php">View More</a></p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">Sell-Side Screens</h5>
								<h4 class="text-semibold"><a href="create_sell_side_screen.php"><i class="fa fa-plus-circle"></i></a></h4>
								<table class="table table-togglable table-hover">
									<thead>
										<tr>
											<th>Screen Title</th>
											<th>Status</th>
											<th>Purpose of Screen</th>
											<th>Notification</th>
										</tr>
									</thead>
									<tbody>
										<tr class="bg-default">
											<td><a href="view-sell-side-screen-main-screen.php">Salesforce.com (SFDC) Q4 FY19 </a></td>
											<td><span class="badge badge-default">DRAFT</span></td>
											<td>Raise Capital</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
										<tr>
											<td><a href="view-sell-side-screen-main-screen.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>

<tr class="bg-default">
											<td><a href="view-sell-side-screen-main-screen.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>

<tr>
											<td><a href="view-sell-side-screen-main-screen.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>

									</tbody>
								</table>
								<p class="text-right"><a href="sell-side-listings.php">View More</a></p>
							</div>
						</div>
					</div>
				
					

				</div>
                    </div>
                </div>
            </div>
           
       
       
<?php require_once('footer.php'); ?>

   