

        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 10,
                responsive: true,
                
                buttons: [
                    {
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

   


function showData()
{
    if($('#sell').is(':checked'))
    {
        $('#obtain_Fin').hide();
        $('#sell_Busi').show();
        $('#equity_Fin').hide();
        $('#debt').hide();
    }
    else if($('#fan').is(':checked'))
    {
        $('#obtain_Fin').show();
        $('#sell_Busi').hide();
        $('#equity_Fin').hide();
        $('#equity_Fin1').hide();
        $('#debt').hide();  
        if($('#equity_check').is(':checked'))
        {
            $('#equity_Fin').show();
        }
        
        if($('#debt_check').is(':checked'))
        {
            $('#debt').show();           
        }
    
    }
    else
    {
        $('#obtain_Fin').hide();
        $('#sell_Busi').hide();
        $('#equity_Fin').hide();
        $('#debt').hide();
    }
}
    



$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip()
 
 
  
  $("#othCt").hide();
  
  $('#ctype').change(function(){
    if($(this).val() == '8'){
    $("#othCt").show();
    } 
    else {
      $("#othCt").hide();
    }
  });
  
    $('input[type="radio"].firstR').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".boxs").not(targetBox).hide();
        $(targetBox).show();
    $(targetBox).css("display", "flex");
    });
});



function createAlert(title, summary, details, severity, dismissible, autoDismiss, appendToId) {
  var iconMap = {
    info: "fa fa-info-circle",
    success: "fa fa-thumbs-up",
    warning: "fa fa-exclamation-triangle",
    danger: "fa ffa fa-exclamation-circle"
  };

  var iconAdded = false;

  var alertClasses = ["alert", "animated", "flipInX"];
  alertClasses.push("alert-" + severity.toLowerCase());

  if (dismissible) {
    alertClasses.push("alert-dismissible");
  }

  var msgIcon = $("<i />", {
    "class": iconMap[severity] // you need to quote "class" since it's a reserved keyword
  });

  var msg = $("<div />", {
    "class": alertClasses.join(" ") // you need to quote "class" since it's a reserved keyword
  });

  if (title) {
    var msgTitle = $("<h4 />", {
      html: title
    }).appendTo(msg);
    
    if(!iconAdded){
      msgTitle.prepend(msgIcon);
      iconAdded = true;
    }
  }

  if (summary) {
    var msgSummary = $("<strong />", {
      html: summary
    }).appendTo(msg);
    
    if(!iconAdded){
      msgSummary.prepend(msgIcon);
      iconAdded = true;
    }
  }

  if (details) {
    var msgDetails = $("<p />", {
      html: details
    }).appendTo(msg);
    
    if(!iconAdded){
      msgDetails.prepend(msgIcon);
      iconAdded = true;
    }
  }
  

  if (dismissible) {
    var msgClose = $("<span />", {
      "class": "close", // you need to quote "class" since it's a reserved keyword
      "data-dismiss": "alert",
      html: "<i class='fa fa-times-circle'></i>"
    }).appendTo(msg);
  }
  
  $('#' + appendToId).prepend(msg);
  
  if(autoDismiss){
    setTimeout(function(){
      msg.addClass("flipOutX");
      setTimeout(function(){
        msg.remove();
      },1000);
    }, 5000);
  }
}




$(document).ready(function(){
    $('input:checkbox').click(function() {
        $('input:checkbox').not(this).prop('checked', false);
    });
});

 $(document).ready(function(){
    $("#oc").hide();
    $("#inv").hide();
    $("#len").hide();
    $("#otr").hide();
  });

  

  function valueChanged()
  {
    if($('#ocCk').is(":checked"))   
      $("#oc").show();
    else
      $("#oc").hide();
      
    if($('#invCk').is(":checked"))   
      $("#inv").show();
    else
      $("#inv").hide();
      
    if($('#lenCk').is(":checked"))   
      $("#len").show();
    else
      $("#len").hide();
      
    if($('#otrCk').is(":checked"))   
      $("#otr").show();
    else
      $("#otr").hide();
  }



$(document).ready(function(){

    $('[data-toggle="popover"]').popover({

        placement : 'top',

        trigger : 'hover'

    });

});

$('#makeEditable').SetEditable({ $addButton: $('#but_add')});
$('#makeEditable2').SetEditable({ $addButton: $('#but_add')});