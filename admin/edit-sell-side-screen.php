<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Edit Sell side Screen  </span></h4>
            </div>

             <div class="heading-elements">
              <div class="heading-btn-group">
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
		<div class="content">
								<div class="row">
								
								
								
								<div class="col-md-12">
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
								<p class="subtitle" contenteditable="true">Over 150,000 companies, both big and small, are growing their business with Salesforce</p>
								<p><b>Headquater : </b> <span  contenteditable="true">Business Address. SALESFORCE TOWER 415 MISSION STREET 3RD FL. SAN FRANCISCO CA 94105.</span></p>
								
								
								<h3 class="company-name">Company Description</h3>
								<p contenteditable="true">Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
									<div class="col-md-12">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold">Ideal timeline for selecting a termsheet and starting diligence?</span></td>
												<td contenteditable="true">As soon as Possible </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Company undergoing a turnaround/restructuring? </span></td>
												<td contenteditable="true">Yes </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Management team expect to stay on after the deal?	</span></td>
												<td contenteditable="true">Management stay  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold">Targets Vertical area of focus : </span></td>
												<td contenteditable="true">Software</td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td contenteditable="true">Software > IT > Cloud</td>
											</tr>
											
											
												
											
											
											
											<tr>
												<td><span class="text-semibold">What is your relationship to this company? * </span></td>
												<td contenteditable="true">CEO/CFO</td>
											</tr>
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td contenteditable="true">Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td contenteditable="true"> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2  class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Historical financials audited</b></th>
						
						<th><b>Fiscal year end</b> </th>
						
						

						</tr>
						
					<tr>
					<td contenteditable="true">Yes</td>
					
					<td contenteditable="true">January</td>
					
					</tr>
					
					
					</table>
	<br/><br/><br/>
						
						<table class="table table-striped table-bordered">
						<tr>
						<th>FY </th>
						<th><b>2018 Actuals</b></th>
						<th><b>2019 Actuals</b></th>
						<th><b>2020 Projected</b></th>
						

						</tr>
						
					<tr>
					<td><b>Revenue</b></td>
					<td contenteditable="true">$13</td>
					<td contenteditable="true">$15</td>
					<td contenteditable="true">$18</td>
					</tr>
					
					<tr>
					<td><b>EBITDA</b></td>
					<td contenteditable="true">$13</td>
					<td contenteditable="true">$25</td>
					<td contenteditable="true">$38</td>
					</tr>
					</table>
					</div>
											
								</div>
</div>


<div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a href="edit-sell-side-blind-profile.php" class="btn btn-success a-btn-slide-text">
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
        <span><strong>Update</strong></span>            
    </a>
						
						
	
	
                </div>
            </div>
        </div> 
							</div>
                        

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   