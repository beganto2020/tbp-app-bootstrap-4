<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Edit Campaign Details </span></h4>
            </div>

           <div class="heading-elements col-md-3">
               <div class="heading-btn-group">
                                 <div class="form-group thanks-button text-right">
                        
						
						<a style="margin-left:20px;" class="btn  btn-default float-right mr-1" href="create-campaign.php" role="button">Create New Campaign</a>
						
                </div>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="row">
								
								<div class="col-md-12">
								<p class="company-name">Project : Velocity</p>
								
								
								</div>
								
					<div class="col-md-8">
							
							<table class="table table-striped table-bordered">
						
					<tr>
					<td style="text-align:center;"><b>Type</b></td>
					<td style="text-align:center;" contenteditable="true">Spotlight</td>
					</tr>
						
					<tr>
					<td style="text-align:center;"><b>Realted To</b> </td>
					<td style="text-align:center;" contenteditable="true">Email marketing</td>
					</tr>
						
					<tr>
					<td style="text-align:center;"><b>Attachments</b> </td>
					<td style="text-align:center;" contenteditable="true">Stock.pdf, current market status.pdf, Employees.doc</td>
					
					</tr>
					<tr>
					<td style="text-align:center;"><b>Content</b> </td>
					<td style="text-align:center;" contenteditable="true">Autoresize anchor autolink charmap code codesample directionality fullpage help hr image imagetools insertdatetime link lists media nonbreaking pagebreak preview print searchreplace table template textpattern toc visualblocks visualchars</td>
					
					</tr>
					
					</table></div>
					
					
					
					
					
								
         
		  
		  <div class="row" style="margin-top:30px;">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                       <a href="view-campaign.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple">Update</button></a>
					   
					   <a href="edit-campaign.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple">Cancel</button></a>
					   
                </div>
            </div></div></div>
                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   