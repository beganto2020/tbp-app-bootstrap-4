<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Campaign Listing </span></h4>
            </div>

           <div class="heading-elements col-md-3">
               <div class="heading-btn-group">
                                <a href="create-campaign.php" class="dt-button buttons-selected btn btn-default" >New  </a>
                                <a href="#" class="dt-button buttons-selected btn btn-default">Import</a>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table dataTables-example" >
                     <thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Title</th>
														
														<th>Type</th>
														<th>Related To </th>
														<th>Status</th>
														<th>Participants</th>
														<th>Bounces</th>
														<th>Opened</th>
														<th>Responded</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													
													
														<td><a href="view-campaign.php">Company Promotions</a></td>
														
														<td><a href="view-campaign.php">Spotlight</a></td>
														
														<td><a href="view-campaign.php">Email Marketing</a></td>
														
														<td><a href="view-campaign.php">Published</a></td>
															       
														<td><a href="view-campaign.php">88 (78)</a></td>
														
														<td><a href="view-campaign.php"> xyz@gmail.com</a></td>
														
														<td><a href="view-campaign.php"> Abc@gmail.com</a></td>
														
														<td><a href="view-campaign.php"> Grover@gmail.com</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#">Create Similar</a></li>
												   
												   <li><a class="dropdown-item" href="#">Manage Participants</a></li>
												   
												       


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
									<tr class="bg-default">
													<td><input type="checkbox"></td>
													
													
														<td><a href="view-campaign.php">Company Promotions</a></td>
														
														<td><a href="view-campaign.php">Spotlight</a></td>
														
														<td><a href="view-campaign.php">Email Marketing</a></td>
														
														<td><a href="view-campaign.php">Published</a></td>
															       
														<td><a href="view-campaign.php">88 (78)</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> xyz@gmail.com</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> Abc@gmail.com</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> Grover@gmail.com</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#">Create Similar</a></li>
												   
												   <li><a class="dropdown-item" href="#">Manage Participants</a></li>
												   
												       


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													
													
														<td><a href="view-campaign.php">Company Promotions</a></td>
														
														<td><a href="view-campaign.php">Spotlight</a></td>
														
														<td><a href="view-campaign.php">Email Marketing</a></td>
														
														<td><a href="view-campaign.php">Published</a></td>
															       
														<td><a href="view-campaign.php">88 (78)</a></td>
														
														<td><a href="view-campaign.php">xyz@gmail.com</a></td>
														
														<td><a href="view-campaign.php">Abc@gmail.com</a></td>
														
														<td><a href="view-campaign.php">Grover@gmail.com</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#">Create Similar</a></li>
												   
												   <li><a class="dropdown-item" href="#">Manage Participants</a></li>
												   

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													
													
														<td><a href="view-campaign.php">Company Promotions</a></td>
														
														<td><a href="view-campaign.php">Spotlight</a></td>
														
														<td><a href="view-campaign.php">Email Marketing</a></td>
														
														<td><a href="view-campaign.php">Published</a></td>
															       
														<td><a href="view-campaign.php">88 (78)</a></td>
														
														<td><a href="view-campaign.php"> xyz@gmail.com</a></td>
														
														<td><a href="view-campaign.php">Abc@gmail.com</a></td>
														
														<td><a href="view-campaign.php"> Grover@gmail.com</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#">Create Similar</a></li>
												   
												   <li><a class="dropdown-item" href="#">Manage Participants</a></li>
												   
												       


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													
													
													
													
													
													
												</tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   