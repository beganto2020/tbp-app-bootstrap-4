<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-8">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Deal Listing</span> - Beganto</h4>
            </div>

           <div class="heading-elements col-md-4">
              <div class="heading-btn-group">
                                <a href="deal-create.php" class="dt-button buttons-selected btn btn-default">Add New Deal  </a>
                                <a href="#"  data-toggle="modal" data-target="#dealUpload" class="dt-button buttons-selected btn btn-default">Upload Deals</a>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table dataTables-example" >
                    <thead>
                          <tr>
                            <th>Date</th>
                            <th>Buyers/Investors</th>
                            <th>Target/Issuers</th>
                            <th>Type</th>
                           
                            <th>Trans Value</th>
                            <th>EV/REV</th>
                            <th>EV/EBITDA</th>
                             <th>Classification</th>
                             <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><a href="deal-edit.php">02/23/2020</a></td>
                            <td><span class="label bg-warning-400"><a href="#" id="text-field2" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Company Name">Trifecta Capital</a></span><br><span class="label bg-success-400" style="margin-top: 2px;"><a href="#" id="text-field2a" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Company Name">Serpentine Technologies Limited</a></span> <a href="#" data-toggle="modal" data-target="#exampleModal1" data-popup="tooltip" title="Do Mapping"><i class="fa fa-spinner"></i></a></td>
                            <td><span class="label bg-success-400"><a href="#" id="text-field3" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Company Name">Passman SAS</a></span> <a href="#" data-toggle="modal" data-target="#exampleModal1" data-popup="tooltip" title="Do Mapping"><i class="fa fa-spinner"></i></a></td>
                            <td>Private Placement</td>
                         
                            <td>11.6</td>
                            <td>2.3</td>
                            <td>4</td>
                               <td>
                                <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                                  <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                                  <li>Software</li>
                                  <li>CRM</li>
                                  <li>Events</li>
                                  <li>Utilities</li>
                                  <li class="active">Workday</li>
                                </ul>
                                <br>
                                <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                                  <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                                  <li>Software</li>
                                  <li>IT</li>
                                  <li>SAP</li>
                                  <li>Utilities</li>
                                  <li class="active">Workday</li>
                                </ul>
                                <a href="#" data-toggle="modal" data-target="#exampleModal3" data-popup="tooltip" title="Alter Classification"><i class="icon-tree6"></i></a>
                            </td>
                            <td>Active</td>
                              <td class="text-center">
                                                  <ul class="icons-list">
                                                      <li class="dropdown">
                                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                          </a>

                                                           <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                          <li><a href="deal-edit.php"> Edit </a></li>
                                                           <li><a href="#"> Archive</a></li>
                                                            <li><a href=""> Activate</a></li>
                                                           <li><a href="#"> View Spotlight</a></li>
                                                            <li><a href=""> Remove Spotlight</a></li>
                                                           <li><a href="#"> Associate a Spotlight</a></li>
                                                       
                                                    </ul>
                                            </li>
                                        </ul>
                                    </td>
                          </tr>
                          <tr>
                             <td><a href="deal-edit.php">02/23/2020</a></td>
                            <td><span class="label bg-success-400"><a href="#" id="text-field4" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Company Name">Serpentine Technologies Limited</a></span><br/><span class="label bg-warning-400"><a href="#" id="text-field2" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Company Name">Trifecta Capital</a></span> <a href="#" data-toggle="modal" data-target="#exampleModal1" data-popup="tooltip" title="Do Mapping"><i class="fa fa-spinner"></i></a></td>
                            <td><span class="label bg-success-400"><a href="#" id="text-field5" data-type="text" data-inputclass="form-control" data-pk="1" data-title="Company Name">Whispr AI IVS</a></span> <a href="#" data-toggle="modal" data-target="#exampleModal1" data-popup="tooltip" title="Do Mapping"><i class="fa fa-spinner"></i></a></td>
                            <td>Merger/Acquisition</td>
                    
                            <td>11.6</td>
                            <td>2.3</td>
                            <td>4</td>
                                   <td>
                                <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                                  <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                                  <li>Software</li>
                                  <li>CRM</li>
                                  <li>Events</li>
                                  <li>Utilities</li>
                                  <li class="active">Workday</li>
                                </ul>
                                <br>
                                <ul class="breadcrumb breadcrumb-arrows" style="display: inline; margin-right: 10px;">
                                  <li><i class="fa fa-arrow-circle-right" style="color: #009688;"></i></li>
                                  <li>Software</li>
                                  <li>IT</li>
                                  <li>SAP</li>
                                  <li>Utilities</li>
                                  <li class="active">Workday</li>
                                </ul>
                                <a href="#" data-toggle="modal" data-target="#exampleModal3" data-popup="tooltip" title="Alter Classification"><i class="icon-tree6"></i></a>
                            </td>
                            <td>Archived</td>
                               <td class="text-center">
                                                  <ul class="icons-list">
                                                      <li class="dropdown">
                                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                                <i class="icon-menu9"></i>
                                                          </a>

                                                           <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                          <li><a href="deal-edit.php"> Edit </a></li>
                                                           <li><a href="#"> Archive</a></li>
                                                            <li><a href=""> Activate</a></li>
                                                           <li><a href="#"> View Spotlight</a></li>
                                                            <li><a href=""> Remove Spotlight</a></li>
                                                           <li><a href="#"> Associate a Spotlight</a></li>
                                                       
                                                    </ul>
                                            </li>
                                        </ul>
                                    </td>
                          </tr>
                        </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   