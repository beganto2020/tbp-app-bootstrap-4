<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Create Users </span> - Beganto</h4>
            </div>

           <div class="heading-elements col-md-3">
             
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                         <div class="row"  >
                     <h5 style="background: #F3F2F2; padding-left: 10px;">Account  Information</h5>
                    <div class="col-md-4">
                       <div class="form-group ">
                        <label class="">Salutation<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Salutation" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">None</option>
                        <option value="opt2">Mr.</option>
                        <option value="opt3">Ms.</option>
                        <option value="opt4">Mrs</option>
                    
                                                
                    </select>
                      </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                     
                      <div class="form-group">
                        <label>First Name<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your First Name" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>

                   
                      

                         <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Account Name<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your Account Name " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Phone<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your Phone No " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                       <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Mobile No<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your Mobile No" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                      

                      <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Email<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your Email " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Secondary Email<a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your Secondary Email  data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                       <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Lead Source<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="What is your Company Lead source ?" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>




                 <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                        <div class="form-group">
                        <label><sup>*</sup></label>
                        
                      <label class="checkbox-inline">
                <input type="checkbox" >Email Optout
                
              </label>
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                          <div class="form-group">
                        <label><sup>*</sup></label>
                        
                      <label class="checkbox-inline">
                <input type="checkbox" >Primary M&A Contact
                
              </label>
                      </div>
                    </div>
                       <div class="col-md-4">
                       <label>&nbsp;</label>
                           <div class="form-group">
                        <label><sup>*</sup></label>
                        
                      <label class="checkbox-inline">
                <input type="checkbox" >DoNotSendSpotlights
                
              </label>
                      </div>
                    </div>
                  </div>
                  
                        <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Assistant<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide your Assistant Name " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Assistant's Phone<a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide Assistant's Phone" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                       <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Assistant's Email<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide Assistant's Email Name" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>


                        <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group">
                        <label>LinkedIn handle <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Share Your LinkedIn handle" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Facebook handle<a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Share Your Facebook handle" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                       <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Twitter handle<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Share Your Twitter  handle" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>

    
                                 <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Description <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your First Name" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Instagram handle <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Share Your Instagram handle" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                       <div class="col-md-4">
                       <label>&nbsp;</label>
                         <div class="form-group">
                        <label>Photographs<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Share Your Photographs" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="file" class="form-control">
                      </div>
                    </div>
                  </div>



                       <div class="row" style="margin-top: 30px;" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;">Address Information</h5>
                    <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>Address<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Share Your Complete Address" data-original-title=""><i class="fa fa-info-circle"></i></a></label><br>
                       <label>Street</label>
                        
                        <textarea class="form-control border"   rows="4" cols="10"></textarea>
                      </div>
                    </div>
                   
                     <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>City<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div>

                       <div class="form-group">
                        <label>Zip/Postal Code<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div>
                    </div>
                       <div class="col-md-4">
                           <label>&nbsp;</label>
                         <div class="form-group">
                        <label>State/Province</label>
                        
                          <input type="text" class="form-control">
                      </div>
                       <div class="form-group">
                        <label>Country<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div>
                    
                    </div>
                    
                  </div>


                         
                   

         <div class="row mt-5 mb-5" >
                    
                   <div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				
				<a href="contacts.php"><button type="button" class="btn btn-primary"> Save</button></a>
				
				<a href="#"><button type="button" style="btn btn-primary" class="btn btn-default"> Cancel</button></a>
				
				
                       
						
	
                </div>
            </div> 

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   