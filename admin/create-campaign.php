<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Create New Campaign</span></h4>
            </div>

           <div class="heading-elements col-md-3">
              
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
					<div class="row" >
                    
                    
					<div class="col-md-6">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Title <sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Subject  for email campaign " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
					
					<div class="col-md-3">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class=""> Type <sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide Your Phone" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                           <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Spotlight</option>
                        <option value="opt2">Industry Report</option>
						<option value="opt3">Marketing</option>
						<option value="opt4">Engagement Outreach</option>
						<option value="opt5">Screen Outreach</option>
                       
                        
                                                
                    </select>
            </div>
                      </div>
                    </div>
					
					
					
                    
                    <div class="col-md-3">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label> Realated To <sup>*</sup> <a href="#"><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
					
					<div class="col-md-offest-1 col-md-4">
                       
			
    <div class="form-group">
					  
                        <label>Attachments<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Drag and Drop " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
						<div id="drop-zone">
  <p>Drop files here...</p>
  <div id="clickHere">or click here.. <i class="fa fa-upload"></i>
    <input type="file" name="file" id="file" multiple />
  </div>
  <div id='filename'></div>
</div>
						
                      </div>
                     
         
</div>
					
                    <div class="col-md-6">
                        
						<div class="color">
			
            <div class="form-group">
              <label>Describe the condition in detail</label>

	
                <textarea name="area2" style="width: 100%;">
       Some Initial Content was in this textarea
</textarea>
            </div>

        </div>	
                      
                    </div>
                  
                   
                      
                    
                    
            
                    </div>
                        

                    </div>
                </div>
            </div>
            </div>
        </div>
       
     
<script src="../js/nicEdit-latest.js" type="text/javascript"></script>
<script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
<?php require_once('footer.php'); ?>

   