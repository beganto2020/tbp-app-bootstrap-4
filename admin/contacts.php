<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Users Listing </span>- Beganto </h4>
            </div>

           <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                                <a href="contact-create.php" class="dt-button buttons-selected btn btn-default legitRipple">New  </a>
                                <a href="#" class="dt-button buttons-selected btn btn-default legitRipple">Import</a>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table dataTables-example" >
                     <thead>
                                <tr><th width="10%"><input type="checkbox"></th>
                                    <th>Name</th>
                                    <th>Account Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Title</th>
                                   
                                    <th class="text-center">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">New Soft</a></td>
                                    <td>Enright Rade</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@newsoft.com</td>
                                    <td>CEO</td>
                                 
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                 <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">smartSoft</a></td>
                                    <td>medan Enrich</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@smartsoft.com</td>
                                    <td>investor</td>
                                  
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">Workex</a></td>
                                    <td>Enright Rock</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@workex.com</td>
                                    <td>MD</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">TBP</a></td>
                                    <td>Adom Gill</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@tbp.com</td>
                                    <td>Expert</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">Vacu</a></td>
                                    <td>Scott word</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@vacusoft.com</td>
                                    <td>CEO</td>
                                  
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">wordwork</a></td>
                                    <td>Elder shep</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@wordwork.com</td>
                                    <td>Owner</td>
                                  
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">workplace</a></td>
                                    <td>Edger John</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@newsoft.com</td>
                                    <td>Founder</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                             <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">Localtreee</a></td>
                                    <td>Enrich Bellen</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>Expert@newsoft.com</td>
                                    <td>expert</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">Donework</a></td>
                                    <td>Rick Well</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@Donework.com</td>
                                    <td>Founder</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">New Soft</a></td>
                                    <td>Enright Throug</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@newsoft.com</td>
                                    <td>CEO</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">New Soft</a></td>
                                    <td>Johm Murry</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@newsoft.com</td>
                                    <td>CEO</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">New Soft</a></td>
                                    <td>Sigma John</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@newsoft.com</td>
                                    <td>CEO</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                               <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">New Soft</a></td>
                                    <td>Robert Joham</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@newsoft.com</td>
                                    <td>CEO</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                  <td><input type="checkbox"></td>
                                    <td><a href="contact-edit.php">New Soft</a></td>
                                    <td>John Worker</td>
                                    <td>+1 (510) 456-789</td>
                                    <td>admin@newsoft.com</td>
                                    <td>CEO</td>
                                   
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                               
                                              <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">New Engagement Target</a></li> 
                                                        <li><a href="#" data-toggle="modal" data-target="#modal_engagement">New Engagement </a></li>
                                                         <li><a href="#"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Notes</a></li>
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_attachment"> New Attachement</a></li>
                                                          <li><a href="#" > Send Mail</a></li>

                                                </ul>

                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                              
                            </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   