<!----View sell side screen--->


<!-- Modal -->
  <div class="modal fade matches" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header  green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways</h4>
        </div>
        <div id="exTab2">	
<ul class="nav nav-tabs">
			<li class="active">
        <a  href="#sell-tab1" data-toggle="tab">Blind Profile</a>
			</li>
			<li><a href="#sell-tab2" data-toggle="tab">Activity</a>
			</li>
			<li><a href="#sell-tab3" data-toggle="tab">Comments</a>
			<li><a href="#sell-tab4" data-toggle="tab">Documents</a>

			</li>
		</ul>

			<div class="tab-content ">
			  <div class="tab-pane active" id="sell-tab1">
          <div class="content" style="margin-top:20px;">
								<div class="row">
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-12">
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
							<br/>
								
								
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
								
								<div class="col-md-6">
								<h3 class="company-name">Business Details</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US East</li>
									<li><span class="text-semibold">Offices :</span> US, India, Latin America</li>
									<li><span class="text-semibold">Employees :</span> 200+</li>
									</ul>
								
									<h3 class="company-name">Service Offerings</h3>
									<div>
									
									<li>Product Devlopment, testing and Maintenance</li>
									<li>Security & infrastructure Services</li>
									<li>Prepackaged software implementation and Support</li>
									<li>Digital Assets Management</li>
									<li>Microsoft, Oracle, and SAP Maintenance</li>
									<li>Software devlopment  & Maintenance</li>
									
									</ul></div>
									
									
										<h3 class="company-name">Customers</h3>
										<div>
									<ul>
									<li>100+ mid market and enterprise customers </li>
									<li>Marquee accounts, including fortune 500 companies </li>
									<li>Primary verticle
									<ul>
									<li>BFSI</li>
									<li>Education</li>
									<li>Health Care</li>
									</ul>
									
									</li>
									
									</ul>
									
									</div></div>
									
									<div class="col-md-6">
									
									
										<table class="table">
											<h3 class="company-name">Financials</h3>
											
											<tr>
											<td style="border:none;"></td>
												<td style="border:none; font-size:24px;"><b><u>2017</u></b></td>
												</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Revenue :</span></td>
												<td>$110m - $120m</td>
											</tr>
											<tr>
												<td><span class="text-semibold"> Revenue Growth : </span></td>
												<td>10%YoY </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Gross Margin :	</span></td>
												<td>30-40%  </td>
											</tr>
												<tr>
												<td><span class="text-semibold"> EBITDA Margin :</span></td>
												<td>5-10%</td>
											</tr>
												
										
										</table>
										
										
										
										
										
										<h3 class="company-name">Investment Consideration</h3>
										
										<div><ul>
										<li>Diversified Offerings</li>
										<li>No Concentration, Largest customer is <10% of revenue</li>
										<li>Founder owned and operated</li>
										<li>Capital efficent, No Outside Capital</li>
										<li>40% recurring revenue</li>
										<li><b>Scale:</b> Sizeable enough to win deals in the mid-market but there are many oppourtunities to scale further via M&amp;A</li>
										
										<li><b>Profitable growth:</b> The company can improve this by partnering with the right private equity frim</li> 
										</ul></div>
										
									</div>
										
											
								</div>
							</div>
		  
		  
		  
		  </div>



				<div class="tab-pane" id="sell-tab2">
          <table class="table table-togglable">
												<thead>
													<tr>
												
														<th>Date</th>
														<th>Name</th>
														<th>Email Id</th>
														<th>Status</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr>
													
													<td>11-02-2020</td>
													<td>jhone</td>
													<td>jhone@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													

<tr>
													
													<td>05-04-2020</td>
													<td>Taden Paul</td>
													<td>paul@example.com</td>
													<td><span class="badge badge-default">Draft</span></td>
													
														
													</tr>
<tr>
													
													<td>08-17-2020</td>
													<td>Harry</td>
													<td>Harry@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>03-03-2019</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													<tr>
													
													<td>03-03-2019</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-18-2020</td>
													<td>Jhone trend</td>
													<td>trend@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													</table>

				</div>
        <div class="tab-pane" id="sell-tab3">
          <div class="panel-body">
              <textarea class="form-control" placeholder="Enter Your here Comments..." rows="3"></textarea><br>
              <a href="#" class="btn btn-success btn-sm pull-right">Comment</a>
              <div class="clearfix"></div>
              <hr>
              <ul class="media-list">
                <li class="media">
                
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Rexona Kumi</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, <a href="#"># consectetur adipiscing</a> .</p>
                  </div>
                </li>
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ John Doe</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor <a href="#"># ipsum dolor</a> adipiscing elit.</p>
                  </div>
                </li>
                <li class="media">
                
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Madonae Jonisyi</strong>
                    <p>Lorem ipsum dolor <a href="#"># sit amet</a> sit amet, consectetur adipiscing elit.</p>
                  </div>
                </li>
              </ul><span class="text-danger">237K users active</span>
            
          </div>
				</div>
				
			<div class="tab-pane" id="sell-tab4">
<table class="table">
              <thead>
                <tr >
                  <th style="font-weight: bolder;">Date</th>
                  <th style="font-weight: bolder;">Title</th>
                  
                  <th style="font-weight: bolder;">Download</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>05-04-2020</td>
                  <td>Salesforce.com turnover documents of 2017</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				<tr>
                  <td>08-17-2020</td>
                  <td>Salesforce.com updatted list of company management</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
                <tr>
                  <td>04-26-2020</td>
                  <td>List of company stock holders</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				
                  <tr>
                  <td>02-11-2020</td>
                  <td>Documents of market exchange reports</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
              </tbody>
            </table>


</div>			
				
				
			</div>
        
      </div>
      <div class="modal-footer">
	  <button type="button" style="background-color:#000; color:#fff;" class="btn btn-default"><i class="fa fa-ban" aria-hidden="true"></i> Decline</button>
        <button type="button" style="background-color:green; color:#fff;" class="btn btn-default"><i class="fa fa-pencil"  aria-hidden="true"></i> Signed NDA</button>
      </div>
    </div>
	
	
  </div> </div>

</div></div></div>
					<!-- /basic modal -->



 <div class="modal fade matches" id="myModal12" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header  green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Sign Up with TBP Finder Fee agreement</h4>
        </div>
      <div class="modal-body">
        
      <form class="well form-horizontal" action=" " method="post"  id="contact_form3">
<fieldset>

<!-- Form Name -->


<!-- Text input-->

<div class="form-group">
   
  <div class="col-md-12 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-user"></i></span>
  <input  name="first_name" placeholder="First Name" class="form-control"  type="text">
    </div>
  </div>
</div>



  <div class="form-group"> 
  
    <div class="col-md-12 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-list"></i></span>
    <select style="display:block !important;" name="department" class="form-control selectpicker">
      <option value="">Select your Business</option>
      <option>Agriculture  </option>
      <option>Manufacturing</option>
      <option>Fintech & BFSI  </option>
      <option>Engineering & Construction  </option>
      <option>Education  </option>
      <option>Telecomm</option>
      <option>Software Services &amp; IT department</option>
      
    </select>
  </div>
</div>
</div>
  


<!-- Text input-->

<div class="form-group">
  
    <div class="col-md-12 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
  <input name="user_password" placeholder="Password" class="form-control"  type="password">
    </div>
  </div>
</div>

<!-- Text input-->

<div class="form-group">
   
    <div class="col-md-12 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
  <input name="confirm_password" placeholder="Confirm Password" class="form-control"  type="password">
    </div>
  </div>
</div>

<!-- Text input-->
       <div class="form-group">
   
    <div class="col-md-12 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
    </div>
  </div>
</div>


<!-- Text input-->
       
<div class="form-group">
  
    <div class="col-md-12 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
  <input name="contact_no" placeholder="(639)" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- Select Basic -->

<!-- Success message -->
<div class="alert alert-success" role="alert" id="success_message">Success <i class="fa fa-thumbs-up"></i> Success!.</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-12"><br>
    &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<button type="submit" class="btn btn-success" >&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbspSUBMIT <span class="fa fa-send"></span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</button>
  </div>
</div>

</fieldset>
</form>
</div>
	  
	  
      </div>
     
    </div></div>
	
	<!----View sell side screen--->	
	
	
	<!--- buy side screen----->
	
	<!-- Modal -->
  <div class="modal fade matches" id="buymyModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header  green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Velocity Media Partners limited Requires well capitalized firm</h4>
        </div>
        <div id="exTab2">	
<ul class="nav nav-tabs">
			<li class="active">
        <a  href="#buy-tab1" data-toggle="tab">Blind Profile</a>
			</li>
			<li><a href="#buy-tab2" data-toggle="tab">Activity</a>
			</li>
			<li><a href="#buy-tab3" data-toggle="tab">Comments</a>
			<li><a href="#buy-tab4" data-toggle="tab">Documents</a>

			</li>
		</ul>

			<div class="tab-content ">
			  <div class="tab-pane active" id="buy-tab1">
          <div class="content">
								<div class="row">
								<br/>
								<div class="col-md-12"><p style="float:right;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></p></div>
								<div class="col-md-12">
								
								
								<h3> Project Brilliant is a 2000+ well captilized global digital transformation service firm.</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
								
								
								<div class="col-md-12">
								<h2 class="company-name">Company Financials</h2>
								<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>
								</div>
								
								</div>
								<div class="row">
								
								
									<div class="col-md-6">
									<h3>Target Offerings</h3>
									<ul>
									
									<li>Cloud Services
									<ul>
									<li>PaaS infrastructure
									<ul>
									<li>Microsoft Stack</li></ul></li>
									
									<li>SaaS cloud ERP infrastructure
									<ul>
									<li>SAP Hana</li>
									<li>Success factor</li>
									<li>Workday</li>
									<li>Salesforce</li></ul></li>
									
									<li>SaaS Cloud verticle Applications
									<ul>
									<li>BFSI</li>
									<li>CPG</li>
									<li>Retail</li></ul></li></ul></li>
									
									
									</ul>
									
									
									
										
										<h3>Targets Verticals</h3>
										
										<ul>
										<li>Technology</li>
										<li>BFSI</li>
										<li>Retail</li>
										<li>CPG</li>
										<li>Utilities</li>
										
										</ul>
										
										
									
									
									
										
									</div>
									
									<div class="col-md-6">
									
										
										<h3>Targets Customers</h3>
									<ul>
									<li>US Based </li>
									<li>Global 2000 companies </li>
									<li>Referenceable Client Base</li>
									
									</ul>
										
									
									<h3>Target Business</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US </li>
									
									</ul>
										
										<h3>Investment considerations</h3>
										
										<ul>
										<li>Owner/Founder run companies</li>
										<li>Preferable no instituional capital</li>
										<li>Managment team to stay for at least 2 years post transaction</li>
										<li>Company should have well trained Empolyees</li>
										<li>Company should had great reputation in market</li>
										<li>Company should be in growth turnover</li>
										
										
										</ul>
										
									</div>
									
									
										
											
								</div>
				</div></div>



				<div class="tab-pane" id="buy-tab2">
          <table class="table table-togglable">
												<thead>
													<tr>
												
														<th>Date</th>
														<th>Name</th>
														<th>Email Id</th>
														<th>Status</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr>
													
													<td>11-02-2020</td>
													<td>jhone</td>
													<td>jhone@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													

<tr>
													
													<td>09-21-2020</td>
													<td>Taden Paul</td>
													<td>paul@example.com</td>
													<td><span class="badge badge-default">Draft</span></td>
													
														
													</tr>
<tr>
													
													<td>10-21-2020</td>
													<td>Harry</td>
													<td>Harry@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-17-2020</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													<tr>
													
													<td>08-17-2020</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-17-2020</td>
													<td>Jhone trend</td>
													<td>trend@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													</table>

				</div>
        <div class="tab-pane" id="buy-tab3">
          <div class="panel-body">
              <textarea class="form-control" placeholder="Enter Your here Comments..." rows="3"></textarea><br>
              <a href="#" class="btn btn-success btn-sm pull-right">Comment</a>
              <div class="clearfix"></div>
              <hr>
              <ul class="media-list">
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Rexona Kumi</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, <a href="#"># consectetur adipiscing</a> .</p>
                  </div>
                </li>
                <li class="media">
                 
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ John Doe</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor <a href="#"># ipsum dolor</a> adipiscing elit.</p>
                  </div>
                </li>
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Madonae Jonisyi</strong>
                    <p>Lorem ipsum dolor <a href="#"># sit amet</a> sit amet, consectetur adipiscing elit.</p>
                  </div>
                </li>
              </ul><span class="text-danger">237K users active</span>
            
          </div>
				</div>
				
			<div class="tab-pane" id="buy-tab4">
<table class="table">
              <thead>
                <tr >
                  <th style="font-weight: bolder;">Date</th>
                  <th style="font-weight: bolder;">Title</th>
                  
                  <th style="font-weight: bolder;">Download</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>08-17-2020</td>
                  <td>Required turnover documents of 2017</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				<tr>
                  <td>10-20-2020</td>
                  <td>shared terms & condition document</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
                <tr>
                  <td>09-21-2020</td>
                  <td>List of company stock holders</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				
                  <tr>
                  <td>10-20-2020</td>
                  <td>Required stock market exchange reports</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
              </tbody>
            </table>


</div>			
				
				
			</div>
        
      </div>
      <div class="modal-footer">
	  <button type="button" style="background-color:#000; color:#fff;" class="btn btn-default"><i class="fa fa-ban" aria-hidden="true"></i> Decline</button>
        <button type="button" style="background-color:green; color:#fff;" class="btn btn-default"><i class="fa fa-pencil"  aria-hidden="true"></i> Signed NDA</button>
      </div>
    </div>
	
	
  </div> </div>


					
 
</div>

<!---buy side screen--->

<!---admin side---->
<!-- 
============================================================================================  New MOdel for All Pages ==================================================================================================== -->


<div class="modal fade" id="myModaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h3 class="modal-title" id="myModalLabel">Warning!</h3>

            </div>
            <div class="modal-body">
                 <h4> Are you sure you want to DELETE?</h4>

            </div>
            <!--/modal-body-collapse -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="btnDelteYes" href="#">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
            <!--/modal-footer-collapse -->
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

                <!-- Large modal -->
          <div id="modal_contact" class="modal fade">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">New Contact</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form >

                            <!--=======================================================Row Design  ====================================------------>
                  <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Salutation<sup>*</sup></label>
                        <div class="form-group">
                        <select name="select" class="form-control input-md">
                        <option value="opt1">None</option>
                        <option value="opt2">Mr.</option>
                        <option value="opt3">Ms.</option>
                        <option value="opt4">Mrs</option>
                    
                                                
                    </select>
                      </div>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Phone <sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>First Name<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Last Name</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                      <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Account Name<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Mobile No</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Account Name<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Mobile No</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Email<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Secondary Email</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                    <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Lead Source<sup>*</sup></label>
                        
                        <div class="form-group">
                        <select name="select" class="form-control input-md">
                        <option value="opt1">Web</option>
                        <option value="opt2">Google</option>
                     
                    
                                                
                    </select>
                      </div>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Linkedin Profile</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label><sup>*</sup></label>
                        
                      <label class="checkbox-inline">
                <input type="checkbox" >Email Optout
                
              </label>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                       <div class="form-group">
                        <label><sup>*</sup></label>
                        
                      <label class="checkbox-inline">
                <input type="checkbox" >Primary M&A Contac
                
              </label>
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>



                     <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Assistant <sup>*</sup></label>
                        
                       <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                       <div class="form-group">
                        <label>Asst. Phone<sup>*</sup></label>
                        
                     <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>



                      <div class="row ">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Assistant Email<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label><sup>*</sup></label>
                        
                      <label class="checkbox-inline">
                <input type="checkbox" >DoNotSendSpotlights
                
              </label>
                      </div>
                    </div>
                  </div>

                    <!--=======================================================Row Design  ====================================------------>


                        <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label> Description<sup>*</sup></label>
                        
                        <textarea class="form-control border"   rows="10" cols="10"></textarea>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <label>Address <sup>*</sup></label>
                      <div class="form-group">
                        <label>Street</label>
                        <textarea class="form-control border" rows="3" cols="10"></textarea>
                      </div>
                      <div class="row">
                        <div class="col-md-5">
                      <div class="form-group">
                        <label> City </label>
                        <input type="text" class="form-control">
                      </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-5">
                           <div class="form-group">
                        <label> State/Province </label>
                        <input type="text" class="form-control">
                      </div>
                        </div>
                        
                      </div>
                         <div class="row">
                        <div class="col-md-6">
                      <div class="form-group">
                        <label> Zip/ Postal Code </label>
                        <input type="text" class="form-control">
                      </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                        <label> Country </label>
                        <input type="text" class="form-control">
                      </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>  


                    </div>

                </div>

                <div class="modal-footer">
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /large modal -->

<div id="modal_team" class="modal fade">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" style="text-align:center;">Add New Contact</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form >

                      
                   <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Name<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    
                    
                    
                 
                      <div class="form-group">
                        <label>Title<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    
                    
                      <div class="form-group">
                        <label>Contact No</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                  
                      <div class="form-group">
                        <label>Email<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    
                      
                        
                    </div>
					</form>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                  


                    </div>

                

                <div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  
                </div></div>
              </div>
            </div>
			
			
			
			
			<!---- add team--->
			
			<div id="modal_new-team-member" class="modal fade">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" style="text-align:center;">New Team</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form >

                      
                   <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>First Name<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    
                    
                    
                 
                      <div class="form-group">
                        <label>Last Name<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    
                        
                  
                      <div class="form-group">
                        <label>Email<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
					
					  <div class="form-group">
                        <label>Role<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
					  
					  
                    </div>
                    
                      
                        
                    </div>
					</form>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                  


                    </div>

                

                <div class="modal-footer">
				<button type="button" class="btn btn-Success">Invite</button>
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  
                </div></div>
              </div>
            </div>
			
			<!----add team--->
         

<!-- notes & Attachment --->

<div id="modal_notes" class="modal fade">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" style="text-align:center;">New Notes</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form >

                      
                   <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label>Subject<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    
                    
                    
                 
                      <div class="form-group">
                        <label>Name<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    
                    
                      <div class="form-group">
                        <label>Task</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                  
                     
					  
					   <div class="form-group">
                        <label>Content<sup>*</sup></label>
                        
                        <input type="textarea"  style="width:100%; height:50px;" class="form-control">
                      </div>
					  
					  <div class="form-group">
                        <label>Flie upload<sup>*</sup></label>
                        
                        <input type="file" class="form-control">
                      </div>
                    </div>
                    
                      
                        
                    </div>
					</form>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                  


                    </div>

               

                <div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  
                </div> </div>
              </div>
            </div>


<!----Notes for update status ---->
<div id="modal_update-status" class="modal fade">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" style="text-align:center;">Add  Note For Update Status</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form >

                      
                  
					<div class="row" >
                    <div class="col-md-12">
                      <div class="form-group ">
                        <label class="">Add Notes<sup>*</sup></label>
                         <textarea class="form-control border"   rows="6" cols="10"></textarea>
                      </div>
                    </div>
                    
                    
                  </div>


              
</form>
</div>
                </div>

                <div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
            </div>
          </div>
		  

<!----Notes for update status ---->


<!----
==========**********************************************************************************************  MODEL FOR TASK ************************************************************========================== -->

<!---Engagement open activities ---->
<div id="modal_openactivity" class="modal fade">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" style="text-align:center;">New Open Activity</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form >


                            <!--=======================================================Row Design  ====================================------------>
                              

                            <!--=======================================================Row Design  ====================================------------>
                  <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Subject<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Name </label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Task <sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Due Date</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                      <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Assigned to</label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Status <sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  
					<div class="row" >
                    <div class="col-md-12">
                      <div class="form-group ">
                        <label class="">Comment<sup>*</sup></label>
                         <textarea class="form-control border"   rows="6" cols="10"></textarea>
                      </div>
                    </div>
                    
                    
                  </div>


              
</form>
</div>
                </div>

                <div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
            </div>
          </div>
		  
		  
		  <!--- close open activities ----->
<!---engagement activity---->

<div id="modal_activity" class="modal fade">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" style="text-align:center;">New Activity</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form >


                            <!--=======================================================Row Design  ====================================------------>
                              

                            <!--=======================================================Row Design  ====================================------------>
                  <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Subject<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Due Date</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Contact <sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Assign To</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                      <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Related to</label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Status <sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  
              
</form>
</div>
                </div>

                <div class="modal-footer">
				<button type="button" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
            </div>
          </div>
		  
		  
		  <!---- Enagegment Activity ---->





                <!-- Large modal -->
          <div id="modal_task" class="modal fade">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">New Task</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form >


                  <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Assigned To<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Subject </label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Related to <sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Name</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                      <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Due Date</label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Status <sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  

  <div class="row" >
                    <div class="col-md-12">
                      <div class="form-group ">
                        <label class="">Comment<sup>*</sup></label>
                         <textarea class="form-control border"   rows="6" cols="10"></textarea>
                      </div>
                    </div>
                    
                    
                  </div>

              
</form>
</div>
                </div>

                <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary">Save changes</button>
				  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /large modal -->
<!-- 
==========**********************************************************************************************  MODEL FOR TARGET ENGAGEMENT************************************************************========================== -->



                <!-- Large modal -->
          <div id="modal_eng_target" class="modal fade">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" style="text-align:center;">Enagegment Target Edit</h5>
                </div>

                <div class="modal-body">
                  
                   <div class="panel-body">
                          <form >

                            <!--=======================================================Row Design  ====================================------------>
                  <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Engagement Target Name<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Engagement <sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Target Account<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Tier</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                      <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Target Contact 1<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Target Contact 2</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Stage<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Pass_Reason</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Updates<sup>*</sup></label>
                        
                        <textarea class="form-control border"   rows="2" cols="8"></textarea>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                     

</form>

                    </div>
                  
                  
                </div>

                <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary">Save changes</button>
				  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

           
<div id="modal_Campaign" class="modal fade">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" style="text-align:center;">New Campaign</h5>
                </div>

                <div class="modal-body">
                 <div class="panel-body">
                          <form>


                              <div class="row" >
                    <div class="col-md-12">
                      <div class="form-group ">
                        <label class="">Subject<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
					  
					  
					   <div class="form-group ">
                        <label class="">Last Date of run<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
					  
					  <div class="form-group ">
                        <label class="">Participants<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
					  
					  <div class="form-group ">
                        <label class="">Bounced<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
					  
					   <div class="form-group ">
                        <label class="">Opened<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
					  
					   <div class="form-group ">
                        <label class="">Responded<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
					  
                    </div>
                    
                    
                  </div>

                             
</form>
</div>
                </div>

                <div class="modal-footer">
				 <button type="button" class="btn btn-primary">Save changes</button>
                  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                 
                </div>
              </div>
            </div>
          </div>


<!-- 
==========**********************************************************************************************  MODEL FOR Engagements************************************************************========================== -->

    <!-- Large modal -->
          <div id="modal_engagement" class="modal fade">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">Edit Engagement</h5>
                </div>

                <div class="modal-body">
                  
                  <div class="panel-body">
                          <form >

                            <!--=======================================================Row Design  ====================================------------>
                  <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Engagement  Name<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Campaign-1<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Client Account<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Campaign-2</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                      <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Engagement Status<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Compaign-3</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Engagement Type<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Deal Team</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Progress<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Eng Start Date</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

              <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Progress<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Work Plane Date</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>



                  <!--=======================================================Row Design  ====================================------------>

              <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Current End Date</label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Actual Close Date</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>

                   <!--=======================================================Row Design  ====================================------------>

              <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Cash At Close</label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Commitment Fee</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>

<!--=======================================================Row Design  ====================================------------>

              <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Next Milestone</label>
                        
                        <textarea class="form-control border"   rows="8" cols="10"></textarea>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Opportunity</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>

</form>
                    </div>
                    
                </div>

                <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary">Save changes</button>
				  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /large modal -->

<!-- 
==========**********************************************************************************************  MODEL FOR ATTACHMENT************************************************************========================== -->

    <!-- Large modal -->
          <div id="modal_attachment" class="modal fade">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">New Attachment</h5>
                </div>

                <div class="modal-body">
                  
                  
                  
                    <div class="row" >
                    <div class="col-md-12 ">
                    <!-- Multiple file upload -->
          <div class="panel panel-flat">
            <div class="panel-heading">
              
              <div class="heading-elements">
                
                      </div>
            </div>

            <div class="panel-body">
              <p class="content-group">
             
              <form action="#" class="dropzone" id="dropzone_multiple"></form>
            </div>
          </div>
          <!-- /multiple file upload -->
                    </div>
                 
                   
                  </div>
                  
                  
                  
                </div>

                <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary">Save changes</button>
				  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /large modal -->

<!-- 
============================================================================================  create Organization MOdel ==================================================================================================== -->


 <!-- Large modal -->
          <div id="modal_organization" class="modal fade">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">Create Organization</h5>
                </div>

                <div class="modal-body">
                  
                <!--   
                    <div class="row" >
                <div class="col-md-4">
                  
                </div>
                <div class="col-md-6 text-right">&nbsp;</div>
              </div> -->
                        </div>
                     <div class="panel-body">
                          <form >
                  <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Company Name <sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Phone <sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Website<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Phone 1 </label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>

                      <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Company Description<sup>*</sup></label>
                        
                        <textarea class="form-control border"   rows="10" cols="10"></textarea>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Billing Address </label>
                        <textarea class="form-control border" rows="3" cols="10"></textarea>
                      </div>
                      <div class="row">
                        <div class="col-md-5">
                      <div class="form-group">
                        <label>Billing City </label>
                        <input type="text" class="form-control">
                      </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-5">
                           <div class="form-group">
                        <label>Billing State/Province </label>
                        <input type="text" class="form-control">
                      </div>
                        </div>
                        
                      </div>
                         <div class="row">
                        <div class="col-md-6">
                      <div class="form-group">
                        <label>Billing Zip/ Postal Code </label>
                        <input type="text" class="form-control">
                      </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                        <label>Billing Country </label>
                        <input type="text" class="form-control">
                      </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>

                      <div class="row ">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Revenue<sup>*</sup></label>
                        <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Revenue</option>
                        <option value="opt2">>10</option>
                        <option value="opt3">10-50</option>
                        <option value="opt4">50-100</option>
                        <option value="opt5">>100</option>
                                                
                    </select>
            </div>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Account Source </label>
                            <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Web</option>
                        <option value="opt2">Google</option>
                        
                                                
                    </select>
              </div>
                      </div>
                    </div>
                  </div>

                    <div class="row">
                    <div class="col-md-6">
                     
                    </div>
                    <div class="col-md-6">
                    
                    </div>
                  </div>



                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
              <label class="display-block text-semibold">Entity Type</label>
              <label class="checkbox-inline">
                <input type="checkbox" id="ocCk" onchange="valueChanged()">
                Operating Company
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="invCk" onchange="valueChanged()">
                Investor
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="lenCk" onchange="valueChanged()">
                Lender
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="otrCk" onchange="valueChanged()">
                Other
              </label>
            </div>
                    </div>

                     <div class="col-md-5">
                    <label class="display-block text-semibold">Account Owner<sup>*</sup></label>
                    <select multiple="multiple" class="select-border-color border-warning grey-400 " style="border-bottom: gray!important;">
                      <optgroup label="Ownership">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      
                    
          </select>
                    </div>


                       <!--- ======================================================= Oerating  Company ===========================================------------->
        <div class="row" id="oc">
                    <div class="col-md-12">
            <fieldset class="customFieldset" style="margin-top:30px;">
              <!-- <legend style="">Operating Company</legend>   -->  
              <h6 style="color: #a976f5;"> Operating Company</h6>         
            
                <div class="row " >
                <div class="col-md-6">
                   <div class="form-group mt-3">
                    <label class="display-block text-semibold">Company Type</label>
                    <label class="radio-inline">
                      <input type="radio" name="radio-inline-left" class="styled" checked="checked">
                      Large Co
                    </label>

                    <label class="radio-inline">
                      <input type="radio" name="radio-inline-left" class="styled">
                      Prospect
                    </label>
                  </div>
                </div>
                
                </div>
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                  <label>Publicly Traded? <sup>*</sup></label>
                  <input name="" type="checkbox" value="">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  
                  </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                  <label>Parent Company / Investors / PrivateEquity <sup>*</sup></label>
                    <select multiple="multiple" class="select-border-color border-warning">
                      <optgroup label="Parent Company">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      <optgroup label="Investors">
                        <option value="IL" >PrivateEquity</option>
                        <option value="IA">Investors</option>
                        <option value="KS">Parent</option>
                      
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        <option value="MA">Massachusetts</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label>Subsidiary Companies / Affiliate Companies<sup>*</sup></label>           
                  <select multiple="multiple" class="select-border-color border-warning">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Subsidiary Companies</option>
                        <option value="CO">Affiliate Companies</option>
                        <option value="ID">TBP</option>
                      
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >Subsidiary Companies</option>
                        <option value="IA">Affiliate Companies</option>
                        <option value="KS">TBP</option>
                        <option value="KY">Kentucky</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        
                      </optgroup>
                    </select>
                  </div>
                </div>
                </div>
            </fieldset>
                    </div>
                  </div>

                         <div class="row" id="inv">
                    <div class="col-md-12">
            <fieldset class="customFieldset" style="margin-top:30px;" >
              <!-- <legend>Investor</legend> -->
              <h6 style="color: #a976f5;">Investor</h6>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="multi-select-full">
                      <select class="multiselect" multiple="multiple">
                        <option value="1">PE/Buyout</option>
                        <option value="2">Venture Capital</option>
                        <option value="3">Fund of Fund</option>
                        <option value="4">Institutional LP</option>
                        <option value="5">Individual Accredited</option>
                        <option value="6">Individual UHNW</option>
                        <option value="7">Family Office</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-6">&nbsp;</div>
              </div>

            

                <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Parent Company / Investors / PrivateEquity </label>
                    <select multiple="multiple" class="select-border-color border-warning">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >PrivateEquity</option>
                        <option value="IA">Investors</option>
                        <option value="KS">Parent</option>
                      
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        <option value="MA">Massachusetts</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  <label>Subsidiary Companies / Affiliate Companies<sup>*</sup></label>           
                  <select multiple="multiple" class="select-border-color border-warning">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Subsidiary Companies</option>
                        <option value="CO">Affiliate Companies</option>
                        <option value="ID">TBP</option>
                      
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >Subsidiary Companies</option>
                        <option value="IA">Affiliate Companies</option>
                        <option value="KS">TBP</option>
                        <option value="KY">Kentucky</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        
                      </optgroup>
                    </select>
                  </div>
                </div>
                </div>
            </fieldset>
                    </div>
                  </div>

                        <div class="row" id="len">
                    <div class="col-md-12">
            <fieldset class="customFieldset" style="margin-top:30px;">
              <!-- <legend>Lender</legend> -->
              <h6 style="color: #a976f5;">Lender</h6>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="multi-select-full">
                      <select class="multiselect" multiple="multiple">
                        <option value="1">Working capital</option>
                        <option value="2">Senior Debt</option>
                        <option value="3">Mz Debt</option>
                        <option value="4">Venture Debt</option>
                        <option value="5" onclick="showOther()">Other</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-6"><input type="text" class="form-control" placeholder="Enter If Other Type" id="otherShow" style="display: none;"></div>
              </div>
            </fieldset>
            <script type="text/javascript">
              function showOther() {
                 document.getElementById('otherShow').style.display = "block";
              }
            </script>
                    </div>
                  </div>

                     <div class="row" id="otr">
                    <div class="col-md-12">
            <fieldset class="customFieldset" style="margin-top:30px;">
              <!-- <legend>Other</legend> -->
              <h6 style="color: #a976f5;">Other</h6>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="multi-select-full">
                      <select class="multiselect" multiple="multiple">
                        <option value="1">Accountants</option>
                        <option value="2">Lawyers</option>
                        <option value="3">Investment Banker/Broker</option>
                        <option value="4">Media</option>
                        <option value="5" onclick="showOther1()">Other</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-6"><input type="text" class="form-control" placeholder="Enter If Other Type"  id="otherShow1" style="display: none;" ></div>
              </div>
            </fieldset>

                    </div>
                  </div>

            <script type="text/javascript">
              function showOther1() {
                 document.getElementById('otherShow1').style.display = "block";
              }
            </script>
                     


                  </div>


                  <!--- ======================================================= Business Catogary ===========================================------------->
                     <div class="row mt-5" style="margin-top: 40px; ">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label>Business Category <sup>*</sup></label>
                    


                        <div class="radio-box border" >
                        
                        <div class="form-group text-muted" style="border:1px solid #ddd; padding: 10px;" >
                    <label class="display-block text-semibold">Software</label>
                    <label class="checkbox-inline">
                      <input type="checkbox" checked="checked">
                      IT
                    </label>

                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Cloud Infrastructure
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      CRM

                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Data Analytics

                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      HR/HCM


                    </label>
                      <label class="checkbox-inline">
                      <input type="checkbox">
                      Supply Chain



                    </label>
                    <a href="" class="display-block" style="padding: 20px;" data-toggle="modal" data-target="#treeModal">More</a>
              </div>

                      </div>
                    </div>

                    </div>
                        <div class="col-md-6">
                      <div class="form-group">
                        <label>&nbsp;</label>
                       

                        <div class="radio-box border">
                        
                        <div class="form-group text-muted" style="border:1px solid #ddd; padding: 20px;" >
                    <label class="display-block text-semibold">IT Services</label>
                    <label class="checkbox-inline">
                      <input type="checkbox" checked="checked">
                      IT
                    </label>

                    <label class="checkbox-inline">
                      <input type="checkbox">
                      CRM
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      HR/HCM
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Supply Chain
                    </label>

                    <a href="" class="display-block" style="padding: 20px;" data-toggle="modal" data-target="#treeModal1">More</a>
                  </div>

                      </div>
                    </div>

                    </div>


                  </div>

                  <div class="row" style="margin-top:28px; margin-bottom: 40px;">
                    <div class="col-md-6">
                      <div class="radio-box border">
                        
                        <div class="form-group text-muted" style="border:1px solid #ddd; padding: 20px;" >
                    <label class="display-block text-semibold">Others</label>
                  

                     <input type="text" class="form-control">
                    
                    
                  </div>

                      </div>
                    
                    </div>
                      <div class="col-md-6">
                      
                    </div>

                  </div>






<!-- 
                  <div class="row mt-5 mb-5" >
                    
                    <div class="col-lg-4 col-md-offset-5 col-lg-offset-5 mt-5 mb-5" >
                    <div class="heading-btn-group  " style=" position: fixed; bottom: 120px; left:70%; width: 100%;">
                                <a href="#" class="btn bg-pink-400 btn-raised active legitRipple">Save</a>
                                <a href="#" class="btn bg-pink-400 btn-raised active legitRipple">Cancel</a>
                                
                      </div>
            </div>        


                  </div> -->

                    </div>















<!-- Modal -->
<div class="modal fade" id="treeModal" tabindex="-1" role="dialog" aria-labelledby="treeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="treeModalLabel">Select Industries</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="tree">
      <ul>
      <li class="jstree-open">Software
        <ul>
          <li>IT
            <ul>
              <li>IT (Database-related)</li>
              <li>IT (Network)</li>
            </ul>
          </li>
          <li>Cloud Infrastructure</li>
          <li>CRM
            <ul>
              <li>CRM (Marketing)</li>
              <li>IT (Events)</li>
            </ul>
          </li>
          <li>Data Analytics</li>
          <li>HR/HCM</li>
          <li>Supply Chain</li>
        </ul>
      </li>
      </ul>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="treeModal1" tabindex="-1" role="dialog" aria-labelledby="treeModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="treeModalLabel1">Select Industries</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="tree1">
      <ul>
      <li class="jstree-open">IT Services
        <ul>
          <li>IT</li>
          <li>CRM</li>
          <li>HR/HCM</li>
          <li>Supply Chain</li>
          <li>Cloud</li>
          <li>Data Analytics</li>
        </ul>
      </li>
      </ul>
    </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary">Save changes</button>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- 
***************************************************************************************************************************************** -->
           
                  
                  
                  
                  
                  
                  
                  
                  
                   <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary">Save changes</button>
				  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
                  
                  
                  
                  
                </div>

               
              </div>
            </div>
          </div>
          <!-- /large modal -->

<!-- 
============================================================================================  New MOdel for All Pages ==================================================================================================== -->

<!-- 
============================================================================================  create Engagement MOdel ==================================================================================================== -->

    <!-- Large modal -->
          <div id="modal_eng_wizard" class="modal fade">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">Create Engagement</h5>
                </div>

                <div class="modal-body">
                  
                   <!-- Saving state -->
                <div class="panel panel-white">
            <div class="panel-heading">
              <!-- <h6 class="panel-title">Create Engagement</h6> -->
              <div class="heading-elements">
                
                      </div>
            </div>

                    <form class="steps-state-saving" action="#" >
              <h6>Create Engagement</h6>
              <fieldset class="fiwizard">
              
                  <div class="panel-body col-md-10 col-md-offset-1">
                          <form >

                            <!--=======================================================Row Design  ====================================------------>
                  <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Engagement  Name<sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Campaign-1<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Client Account<sup>*</sup></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Campaign-2</label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

                      <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Engagement Status<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Compaign-3</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                   <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Engagement Type<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Deal Team</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Progress<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Eng Start Date</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>

              <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Progress<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Work Plane Date</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>



                  <!--=======================================================Row Design  ====================================------------>

              <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Current End Date</label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Actual Close Date</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>

                   <!--=======================================================Row Design  ====================================------------>

              <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Cash At Close</label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Commitment Fee</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>

<!--=======================================================Row Design  ====================================------------>

              <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Next Milestone</label>
                        
                        <textarea class="form-control border"   rows="6" cols="10"></textarea>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Opportunity</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>

             



</div>

                
 
            
              </fieldset>

              
              

              <h6>Create Blind Profile</h6>
              <fieldset>
            
                            <div class="panel-body col-md-10 col-md-offset-1">
                          <form >

                            <!--=======================================================Row Design  ====================================------------>
                   <div class="row" >
                    <div class="col-md-5">
                      <div class="form-group ">
                        <label class="">Title <sup>*</sup></label>
                         <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">

                      <div class="form-group">
                        <label>Company Description <sup>*</sup></label>
                        <textarea class="form-control border"   rows="8" cols="10"></textarea>
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                    <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Target Offerings *<sup>*</sup></label>
                         <textarea class="form-control border"   rows="8" cols="10"></textarea>
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Target's Customers *</label>
                         <textarea class="form-control border"   rows="8" cols="10"></textarea>
                      </div>
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
            <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Target Business <sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Financials *</label>
                         <input type="text" class="form-control">
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Target's Verticals *<sup>*</sup></label>
                        
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                      <div class="form-group">
                        <label>Investment Considerations *</label>
                          <textarea class="form-control border"   rows="6" cols="10"></textarea>
                      </div>
                      
                        
                    </div>
                  </div>
                  <!--=======================================================Row Design  ====================================------------>
                 
                  <!--=======================================================Row Design  ====================================------------>

             


            

<!--=======================================================Row Design  ====================================------------>

             

             



</div>
                  
                  <!--=======================================================Row Design  ====================================------------>

              </fieldset>



 
            
            </form>
                </div>
                <!-- /saving state -->
                  
                 
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                </div>

                <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary">Save changes</button>
				  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /large modal -->

<!-- 
============================================================================================  New MOdel for Create LookUp ==================================================================================================== -->



    <!-- Large modal -->
          <div id="modal_create_lookup" class="modal fade">
            <div class="modal-dialog modal-default">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title text-center">Edit Lookup</h5>
                </div>

                <div class="modal-body">
                  
                  
                               

                    <div class="row">
                      <div class="col-md-6">
                         <h4>Entity Type</h4>   
                      </div>
                      <div class="col-md-6">
                       <h6>Investor</h6>
                      </div>
                    </div>
                  <!----============================================ROW one ===========================-------->
                    <div class="row" style="margin-top: 30px;">
                 
               
                    

                    <div class="row">
                      <div class="col-md-6">
                           <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Code</option>
                        <option value="opt2">01</option>
                        <option value="opt3">02</option>
                        <option value="opt4">03</option>
                    
                                                
                    </select>
                      </div>
                        
                      </div>
                      <div class="col-md-6">
                           <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Display Name</option>
                        <option value="opt2">Mindshare</option>
                        <option value="opt3">Worktop</option>
                        <option value="opt4">Vacuspft</option>
                    
                                                
                    </select>
                      </div>
                      </div>
                    </div>
                  
                   
                  </div>
                        
                  <!----============================================ROW tw0 ===========================-------->
                    
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                </div>

                <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary">Save changes</button>
				  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /large modal -->


  <!----============================================ROW lookup Value ===========================-------->


    <!-- Large modal -->
          <div id="modal_view_lookup" class="modal fade">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title">View Lookup</h5>
                </div>

                <div class="modal-body">
                  
                  
                  
                  <!----============================================ROW one ===========================-------->
                    <div class="row" >
                    <table class="table">
                      
                        <tr class="text-teal">
                          <th>Lookup Name</th>
                           <th>Lookup Value</th>
                        </tr>
                        <tr>
                          <td>Lookup 1</td>
                          <td>Value 1</td>
                        </tr>
                        <tr>
                          <td>Lookup 2</td>
                          <td>Value 2</td>
                        </tr>
                        <tr>
                          <td>Lookup 3</td>
                          <td>Value 3</td>
                        </tr>
                        <tr>
                          <td>Lookup 4</td>
                          <td>Value 4</td>
                        </tr>
                        <tr>
                          <td>Lookup 5</td>
                          <td>Value 5</td>
                        </tr>
                    </table>
                   
                  </div>
                        
                  <!----============================================ROW tw0 ===========================-------->
                    
                  
                  
                  
                  
                  
                  
                  
                  
                  
                  
                </div>

                <div class="modal-footer">
                  
                  <button type="button" class="btn btn-primary">Save changes</button>
				  <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          <!-- /large modal -->



<!-- 
===================================================================Business Catogary more tree model =============================================================================== -->


<!-- Modal -->
<div class="modal fade" id="treeModal" tabindex="-1" role="dialog" aria-labelledby="treeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="treeModalLabel">Select Industries</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="tree">
      <ul>
      <li class="jstree-open">Software
        <ul>
          <li>IT
            <ul>
              <li>IT (Database-related)</li>
              <li>IT (Network)</li>
            </ul>
          </li>
          <li>Cloud Infrastructure</li>
          <li>CRM
            <ul>
              <li>CRM (Marketing)</li>
              <li>IT (Events)</li>
            </ul>
          </li>
          <li>Data Analytics</li>
          <li>HR/HCM</li>
          <li>Supply Chain</li>
        </ul>
      </li>
      </ul>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="treeModal1" tabindex="-1" role="dialog" aria-labelledby="treeModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="treeModalLabel1">Select Industries</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="tree1">
      <ul>
      <li class="jstree-open">IT Services
        <ul>
          <li>IT</li>
          <li>CRM</li>
          <li>HR/HCM</li>
          <li>Supply Chain</li>
          <li>Cloud</li>
          <li>Data Analytics</li>
        </ul>
      </li>
      </ul>
    </div>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-primary">Save changes</button>
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<!---deals--->
<div class="modal fade" id="dealUpload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel3" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel3">Upload Deals</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
  
                    
					 
					 <form action="" method="post" enctype="multipart/form-data" id="js-upload-form">
            <div class="form-inline">
              <div class="form-group">
                <input type="file" name="files[]" id="js-upload-files" multiple>
              </div>
              <button type="submit" class="btn btn-sm btn-primary" id="js-upload-submit">Upload  files</button>
            </div>
          </form>
			
			
			
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-success" data-dismiss="modal">Upload</button>
<button type="button" class="btn btn-secondary" style="color:#000;" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Link Spotlights</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table datatable-basic12">
      <thead>
        <tr>
          <th>S. No.</th>
          <th>Deal Name</th>
          <th>Remove</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td><a href="editSpotlight.html">Spotlight 1</a></td>
          <td>
            <div class="checkbox">
              <label>
                <input type="checkbox" class="styled">
              </label>
            </div>
          </td>
        </tr>
        <tr>
          <td>2</td>
          <td><a href="editSpotlight.html">Spotlight 2</a></td>
          <td>
            <div class="checkbox">
              <label>
                <input type="checkbox" class="styled">
              </label>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
    <div class="clearfix">&nbsp;</div>
    <div class="row">
      <div class="col-md-12">
        <button type="button" class="btn btn-primary btn-raised" id="adMorSpot">Link New Spotlights</button>
      </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="hiDe4">
      <div class="row">
        <div class="col-md-12">
          <div class="multi-select-full">
            <select class="multiselect-filtering" multiple="multiple">
                            <option value="1" selected>Spotlight 1</option>
                            <option value="2" selected>Spotlight 2</option>
                            <option value="3">Spotlight 3</option>
                            <option value="4">Spotlight 4</option>
                        </select>
          </div>
        </div>
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel1">Map Company</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label>Select Company From The List</label>
          <select class="select-search">
            <option value="1">Trifecta Capital</option>
            <option value="2">Innoviti Payment Solutions Pvt. Ltd.</option>
            <option value="3">Keyhole TIG Limited</option>
            <option value="4">Serpentine Technologies Limited</option>
          </select>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12"><span class="label label-flat border-info text-info-600">Company not found in the database! Add New</span></div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">  
          <label>Add New Company</label>
          <input type="text" class="form-control">
        </div>
      </div>
    </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>


