<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold"> Engagement </span>- Beganto </h4>
            </div>

           <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                                <a href="engagement-create.php" class="dt-button buttons-selected btn btn-default legitRipple">Create a New Engagement  </a>
                                
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div id="exTab2">
  <div class="panel panel-default"> 
    <div class="panel-heading">
      <div class="panel-title">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#engagment-tab1" data-toggle="tab">Engagement Details</a>
          </li>
          <li><a href="#engagment-tab2" data-toggle="tab">Screens Tab</a>
          </li>
          <li><a href="#engagment-tab3" data-toggle="tab">Blind Profile </a>
          </li>
		  
		  <li><a href="#engagment-tab4" data-toggle="tab">Master Prospects</a>
          </li>
		  
		  <li><a href="#engagment-tab5" data-toggle="tab">Active Prospects</a>
          </li>
		  
		  <li><a href="#engagment-tab6" data-toggle="tab">Archive Prospects</a>
          </li>
		  
		  
        </ul>
      </div>
    </div>
    
    <div class="">
      <div class="tab-content ">
        <div class="tab-pane active" id="engagment-tab1">
		<div class="panel-body">
                        	<div class="row">
                        		<div class="col-lg-12" >

                
					<!-- /info alert -->

                        			<div class="row" id="hideData" style="margin:20px 0px;">
                        				<div class="col-md-4">
                        					<label class="text-teal">Engagement Details</label>
                        					<table class="table table-striped">
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Engagement Name</strong></td>
                        							<td> Salesforce.com (SFDC) Q4 FY19</td>
                        								<td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        							
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Client Name</strong></td>
                        							<td>salesforce Organization</td>
                        							<td class="text-teal"><div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Deal Team</strong></td>
                        							<td> salesforce Team</td>
                        							<td class="text-teal"><div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        						<tr class="showOnhoverEditIcon">
                        							<td> <strong>Start Date</strong></td>
                        							<td>11-02-2018</td>
                        							<td class="text-teal"><div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        					</table>

                        				</div>
                        				<div class="col-md-4">
                        					<label style="float: right;"> &nbsp;</label>
                        						<table class="table table-striped ">
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Current End Date</strong></td>
                                      <td>11-02-2019</td>
                                      <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Status</strong></td>
                        							<td>Archived</td>
                        							<td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Progress</strong></td>
                                      <td>10-20%</td>
                                      <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        						<tr class="showOnhoverEditIcon">
                        							 <td><strong>Actual Close Date</strong></td>
                                        <td>02-11-2020</td>
                                          <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
												
												
                        					</table>
                        				</div>
                        					<div class="col-md-4">
                        					<label style="float: right;"> &nbsp;</label>

                                    <table class="table" >
                                      
                                       <tr class="showOnhoverEditIcon" >
                                        <td width="20%"><strong>Next Milestone</strong></td>
                                        <td width="80%" >  Salesforce stock market is so high. Its Locates in Usa Created Software , Web Design , Web Develeopment   , Software testing , CRM , ROI , Digital Marketing </td>
                                          <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                      </tr>
                                     


                                    </table>
                                    
                                        

                        				</div>

                        			</div>








                        		</div>
                        		<div class="col-lg-12 " style="display: none; box-shadow: 0 2px  4px rgba(0,0,0,.16); border-radius: 0.25rem; padding: 20px;"  id="editData">
                        			
                       <!--=======================================================Row Design  ====================================------------>
                   

<div class="row" >
                     <!-- <h5 style="background: #F3F2F2; padding-left: 10px;">Account  Information</h5> -->
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class="">Engagement Name <sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide Your Company name" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label>Client<sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="select a Organization" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <select class="form-control">
										  <option>SalesForce</option>
										  <option>Velocity</option>
										  <option>Nivita</option>
										  <option>TZP Media Partners</option>
									</select>
                      </div>
                    </div>
                    <div class="col-md-4">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Engagement Type<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Engagement Type " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <select class="form-control">
										  <option>Finder Only</option>
										  <option>Full Service</option>
										  <option>M&A/recap</option>
										  <option>Capital raising syndicate</option>
										  <option>Exclusive</option>
										  <option>Non-Exclusive</option>
									</select>
                      </div>
                    </div>
                  </div>

                   
                       <!--=======================================================Row Design  ====================================------------>

                         <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class="">Deal Team <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Enter Deal Team" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <div class="multiselect">
							<div class="selectBox" onclick="showCheckboxes()">
								<select class="form-control">
									<option>Select Contacts</option>
									</select>
								<div class="overSelect"></div>
									</div>
								<div id="checkboxes">
      <label for="one">
        <input type="checkbox" id="one" />SalesForce Team</label>
      <label for="two">
        <input type="checkbox" id="two" />Velocity Team</label>
      <label for="three">
        <input type="checkbox" id="three" />Nivita Team</label>
		
		<label for="three">
		<input type="checkbox" id="four" />TZP Media Partners Team</label>
    </div>
  </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label>Start Date<sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your Official Company Website" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
						<br/>
                                  <input class="form-control" style="width:50%;" type="date" id="start" name="trip-start"
       value="2018-07-22"
       min="1991-01-01" max="2058-12-31">
                      </div>
                    </div>
                     <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label>End Date<sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your Official Company Website" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
						<br/>
                                  <input class="form-control" style="width:50%;" type="date" id="start" name="trip-start"
       value="2018-07-22"
       min="1991-01-01" max="2058-12-31">
                      </div>
                    </div>
                  </div>
                      <!--=======================================================Row Design  ====================================------------>

                  <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                     <div class="col-md-4">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Status<sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Status " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <select class="form-control">
										  <option>Active</option>
										  <option>Archived</option>
										  <option>Draft</option>
										  
									</select>
                      </div>
                    </div>
                   
                     <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>Progress</label>
                          
                        <select class="form-control">
										  <option>0-10%</option>
										  <option>10-20%</option>
										  <option>20-30%</option>
										   <option>30-40%</option>
										    <option>20-30%</option>
											 <option>40-50%</option>
											  <option>50-60%</option>
											  <option>60-70%</option> 
											  <option>70-80%</option>
											<option>80-90%</option>
											<option>90-100%</option>  											  
										  
									</select>
                          
                      </div>
                    </div>
                       <div class="col-md-4">
                       
                    <label class="display-block text-semibold">Actual Close Date</label>
                <br/>
                                  <input class="form-control" style="width:50%;" type="date" id="start" name="trip-start"
       value="2018-07-22"
       min="1991-01-01" max="2058-12-31">
                    </div>
                    
                  </div>


                       <!--=======================================================Row Design  ====================================------------>

                         <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class="">Next Milestone<sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide Your Company name" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <textarea class="form-control"></textarea>
                      </div>
                    </div>
                 
                  </div>








                




                  <div class="row mt-5 mb-5" >
                  	
					<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				
				<a href="engagements.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"> Save</button></a>
				
				<a href="engagements.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"> Cancel</button></a>
				
				
                       
						
	
                </div>
            </div>
					
					
                  				


                  </div>


          </div>

                  <!---    ================================================right side========================== -->
                        		

                        	</div>



                        <!---	======================================================contact  row 1======================================== -->

                        	<div style="margin-top:40px;">
                        		<div class="col-md-6">
                        				<div class="panel panel-white">
									<div class="panel-heading" style="background-color: #eee; padding: 10px 10px;" >
										<h6 class="panel-title">
											<a data-toggle="collapse" href="#collapsible-controls-group1" class="text-bold text-teal"><i class="icon-magazine"></i>Team Member </a>
										</h6>

										 <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_team" ><i class=" icon-plus3"></i> New Team Member</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
									</div>
									<div id="collapsible-controls-group1" class="panel-collapse collapse ">
										<div class="panel-body">
											
										<table class="table" id="DyanmicTable">
                      <thead>
                            <tr style="background: #fff;  color: orange; font-weight: bolder!important;">
                              
                            
                              <th>Name</th>
                              <th>Title</th>
                              <th>Email</th>
                              <th>Phone</th>
                                <th>Action</th>
                            </tr>  
                        </thead>  
                        <tbody>
                          <tr>
                            
                         
                           <td>Kees Wurth</td>
                            <td>CEO / Owner</td>
                             <td>mail@mail.com</td>
                             <td>123247439</td>
                              
                              </tr>
                               <tr>
                              
                            <td>Kees Wurth</td>
                            <td>CEO / Owner</td>
                             <td>mail@mail.com</td>
                             <td>123247439</td>
                                
                              </tr>
							  
							  <tr>
                              
                            <td>Kees Wurth</td>
                            <td>CEO / Owner</td>
                             <td>mail@mail.com</td>
                             <td>123247439</td>
                                
                              </tr>

                        </tbody>      
                    </table>
										
										</div>
									</div>
									
								
								</div>
                        		</div>
                        		
                        	</div>
  <!--=================================Row Edit ===========================-->

                                  
                          <!--- ======================================================contact  row 3======================================== -->
                           <!--=================================Row Edit ===========================-->

                                  <div>
                            <div class="col-md-6">
                            <div class="panel panel-white">
                  <div class="panel-heading" style="background-color: #eee; padding: 10px 10px;">
                    <h6 class="panel-title">
                      <a class="collapsed text-bold text-orange" data-toggle="collapse" href="#tabop"> <i style="color:#FF9800; font-size:16px;" class="fa fa-tasks"></i> Open Activities </a>
                    </h6>

                    <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_openactivity" ><i class=" icon-plus3"></i> New Task</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
                  </div>
                  <div id="tabop" class="panel-collapse collapse">
                    <div class="panel-body">
                       <table class="table" id="DyanmicTable1">
                        <thead>
                          <tr>
                            
                            <th>Subject</th>
                              <th>Name</th>
                                <th>Task</th>
                                  <th>Due Date</th>
                                    <th>Assigned To</th>
									<th>Actions</th>
                                     
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            
                            <td>Email: Introducton</td>
                            <td>David</td>
                            <td>check</td>
                            <td>10/12/2019</td>
                            <td>Sunil Grover</td>
                            
                          </tr>
						  
						  <tr>
                            
                            <td>Email: Introducton</td>
                            <td>David</td>
                            <td>check</td>
                            <td>10/12/2019</td>
                            <td>Sunil Grover</td>
                            
                          </tr>
						  
						  <tr>
                            
                            <td>Email: Introducton</td>
                            <td>David</td>
                            <td>check</td>
                            <td>10/12/2019</td>
                            <td>Sunil Grover</td>
                            
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                            </div>
                            <div class="col-md-6">
                          <div class="panel panel-white">
                  <div class="panel-heading" style="background-color: #eee; padding: 10px 10px;">
                    <h6 class="panel-title">
                      <a class="collapsed text-bold text-green" data-toggle="collapse" href="#tabactiv">  <i style="color:#8BC34A; font-size:16px;"  class="fa fa-tasks"></i> Activities</a>
                    </h6>

                    <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_activity" ><i class=" icon-plus3"></i> New Task</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
                  </div>
                  <div id="tabactiv" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table" id="DyanmicTable2">
                        <thead>
                          <tr>
                          
                            <th>Subject</th>
                              <th>Name</th>
                                <th>Task</th>
                                  <th>Due Date</th>
                                    <th>Assigned To</th>
                                        <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            
                            <td>Email: Introducton</td>
                            <td>David</td>
                            <td>check</td>
                            <td>10/12/2019</td>
                            <td>Sunil Grover</td>
                             
                          </tr>
						  
						  <tr>
                            
                            <td>Email: Introducton</td>
                            <td>David</td>
                            <td>check</td>
                            <td>10/12/2019</td>
                            <td>Sunil Grover</td>
                             
                          </tr>
						  
						  
						  <tr>
                            
                            <td>Email: Introducton</td>
                            <td>David</td>
                            <td>check</td>
                            <td>10/12/2019</td>
                            <td>Sunil Grover</td>
                             
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                            </div>
                          </div>
                          <!--- ======================================================contact  row 3======================================== -->
  <!---	======================================================contact  row 2======================================== -->
  						<div>
                        		<div class="col-md-6">
                        			<div class="panel panel-white">
									<div class="panel-heading crd-head" style="background-color: #eee; padding: 10px 10px;" >
										<h6 class="panel-title">
											<a class="collapsed text-bold text-success" data-toggle="collapse" href="#tab3"><i class="icon-attachment"></i> Notes and Attachments</a>
										</h6>

										 <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_notes" ><i class=" icon-plus3"></i> New Notes</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
									</div>
									<div id="tab3" class="panel-collapse collapse ">
										<div class="panel-body">
									<table class="table" id="DyanmicTable3">
									<thead>
									<tr class="first-row">
                          
                            <th>Subject</th>
                              <th>Name</th>
                                <th>Task</th>
								<th>Type</th>
                                  <th>Action</th>
                             
                          </tr>
                        </thead>
						<tbody>
                          <tr>
                            
                            <td>Email: Introducton</td>
                            <td>David</td>
                            
                            <td>Email to client</td>
							<td>Notes</td>
                            
                             
                          </tr>
						  
						  <tr>
                            
                            <td>Email: Introducton</td>
                            <td>Mendis</td>
                            
                            <td>client requirement Pdf</td>
							<td>Attachment</td>
                            
                             
                          </tr>
						  
						  <tr>
                            
                            <td>Email: Introducton</td>
                            <td>Jhon</td>
                            
                            <td>Manage report of organization</td>
							<td>Notes</td>
                            
                             
                          </tr>
                         
                          
                        
                            
                           
                          </tbody>          
                      </table>


                 


										</div>
									</div>
								</div>
                        		</div>
								
					<div class="col-md-12">
                        			<div class="panel panel-white">
									<div class="panel-heading " style="background-color: #eee; padding: 10px 10px;" >
										<h6 class="panel-title ">
											<a class="collapsed text-bold text-info" data-toggle="collapse" href="#collapsible-controls-group2" ><i class="icon-wrench"></i>Campaign </a>
										</h6>

										<div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_Campaign" ><i class=" icon-plus3"></i> New Campaign</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
									</div>
									<div id="collapsible-controls-group2" class="panel-collapse collapse">
										<div class="panel-body">
                      <table class="table" id="DyanmicTable4">
									<thead>
									<tr class="first-row">
                          
                            <th>Subject</th>
                              <th>Date of last run</th>
                                <th>Participants</th>
								<th>Bounced</th>
								<th>Opened</th>
								<th>Responded</th>
                                  <th>Action</th>
                             
                          </tr>
                        </thead>
						<tbody>
                          <tr>
                            
                            <td>SalesForce: Introducton</td>
                            <td>12-03-2017</td>
                            <td>Jhonty Roy</td>
                            <td>No</td>
							<td>Yes</td>
							<td>Yes</td>
                            
                             
                          </tr>
						  
						  <tr>
                            
                            <td>SalesForce: Introducton</td>
                            <td>15-03-2017</td>
                            <td>Albert watson</td>
                            <td>yes</td>
							<td>No</td>
							<td>No</td>
                            
                             
                          </tr>
						  
						  
						  <tr>
                            
                            <td>SalesForce: Introducton</td>
                            <td>18-03-2017</td>
                            <td>Mendis </td>
                            <td>No</td>
							<td>Yes</td>
							<td>Yes</td>
                            
                             
                          </tr>
						  
                           
                          </tbody>          
                      </table>
										</div>
									</div>
								</div>
                        		</div>			
								
                        		
                        	</div>



</div>
								
		</div>
		
		
        <div class="tab-pane" id="engagment-tab2">
          <div class="content">
								

												<div class="row">
											
							<div class="col-md-12">
								


<table class="table tab-pane table-togglable table-hover">
												<thead>
													<tr>
													<th></th>
														<th>Title</th>
														
														<th>Objective</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														<th>Classification</th>
														<th>New Matches</th>
														<th>Active Matches</th>
														<th>Status</th>
														
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
											<td><input type="radio" name="colorRadio" value="red"></td>	
										<td>Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways</td>
										
										<td>Provide Capital</td>
														
										<td>$1-5M</td>
															       
										<td>$6m</td>
														
										<td>Industrials + Construction, Technology</td>
														
										<td>7</td>
														
										<td>12</td>
										
										<td>Active</td>
														
														
														
													</tr>
													
														<tr class="bg-default">
											<td><input type="radio" name="colorRadio" value="blue"> </td>		
										<td>Velocity wants ERP and CRM Software company in information technology</td>
										
										<td>Provide Capital</td>
														
										<td>$1-5M</td>
															       
										<td>$6m</td>
														
										<td>Industrials + Construction, Technology</td>
														
										<td>7</td>
														
										<td>12</td>
										
										<td>Active</td>
														
														
														
													</tr>
													
													
												</tbody>
											</table>

  


  <!-------------------------------------- if sell business is Selected -------------------------->
  								
<div class="red box">

	<div class="panel-flat">
                    	<div class="panel-heading">
							
                        
						<div>
 


  <div class="panel-default"> 
    <div class="panel-heading">
      <div class="panel-title">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#sell-side-main1" data-toggle="tab">Screen Details</a>
          </li>
          <li><a href="#sell-side-main2" data-toggle="tab">Blind Profile</a>
          </li>
          <li><a href="#sell-side-main3" data-toggle="tab">Active Matches</a>
          </li>
		  
		  <li><a href="#sell-side-main4" data-toggle="tab">New Matches</a>
          </li>
		  
		  <li><a href="#sell-side-main5" data-toggle="tab">Archived Matches</a>
          </li>
		  
		  
        </ul>
      </div>
    </div>
    
    <div class="panel-body">
      <div class="tab-content ">
        <div class="tab-pane active" id="sell-side-main1">
		<div class="row">
									
								
								
								
								<div class="col-md-8">
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
								<p class="subtitle">Over 150,000 companies, both big and small, are growing their business with Salesforce</p>
								<p><b>Headquater : </b> Business Address. SALESFORCE TOWER 415 MISSION STREET 3RD FL. SAN FRANCISCO CA 94105.</p>
								
								
								<h3 class="company-name">Company Description</h3>
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
									<div class="col-md-12">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold">Ideal timeline for selecting a termsheet and starting diligence?</span></td>
												<td>As soon as Possible </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Company undergoing a turnaround/restructuring? </span></td>
												<td>Yes </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Management team expect to stay on after the deal?	</span></td>
												<td>Management stay  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold">Targets Vertical area of focus : </span></td>
												<td>Software</td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td>Software > IT > Cloud</td>
											</tr>
											
											
												
											
											
											
											<tr>
												<td><span class="text-semibold">What is your relationship to this company? * </span></td>
												<td>CEO/CFO</td>
											</tr>
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td>Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2  class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Historical financials audited</b></th>
						
						<th><b>Fiscal year end</b> </th>
						
						

						</tr>
						
					<tr>
					<td>Yes</td>
					
					<td>January</td>
					
					</tr>
					
					
					</table>
	<br/><br/><br/>
						
						<table class="table table-striped table-bordered">
						<tr>
						<th>FY </th>
						<th><b>2018 Actuals</b></th>
						<th><b>2019 Actuals</b></th>
						<th><b>2020 Projected</b></th>
						

						</tr>
						
					<tr>
					<td><b>Revenue</b></td>
					<td>$13</td>
					<td>$15</td>
					<td>$18</td>
					</tr>
					
					<tr>
					<td><b>EBITDA</b></td>
					<td>$13</td>
					<td>$25</td>
					<td>$38</td>
					</tr>
					</table>
					</div>
											
								</div>

											
								</div>
		
         
		  
		  <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a href="edit-sell-side-screen.php" class="btn btn-success a-btn-slide-text">
        <i style="color:#fff; padding-right:15px; font-size:15px; text-align:center;" class="fa fa-edit" aria-hidden="true"></i>
        <span><strong>Edit</strong></span>            
    </a>
						
						  <span class="button-checkbox">
        <button type="button" class="btn" data-color="success">Archive</button>
        <input type="checkbox" class="hidden" />
    </span>
	
	
                </div>
            </div>
        </div> 
		  
		  
		  
        </div>
		
		
        <div class="tab-pane" id="sell-side-main2">
          
		  
		  <div class="content">
								<div class="row">
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-8">
								
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
							<br/>
								
								
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
								
								<div class="col-md-4">
								<h3 class="company-name">Business Details</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US East</li>
									<li><span class="text-semibold">Offices :</span> US, India, Latin America</li>
									<li><span class="text-semibold">Employees :</span> 200+</li>
									</ul>
								</div>
								
									<div class="col-md-6">
									<h3 class="company-name">Service Offerings</h3>
									<div>
									
									<li>Product Devlopment, testing and Maintenance</li>
									<li>Security & infrastructure Services</li>
									<li>Prepackaged software implementation and Support</li>
									<li>Digital Assets Management</li>
									<li>Microsoft, Oracle, and SAP Maintenance</li>
									<li>Software devlopment  & Maintenance</li>
									
									</ul></div>
									
									
										<h3 class="company-name">Customers</h3>
										<div>
									<ul>
									<li>100+ mid market and enterprise customers </li>
									<li>Marquee accounts, including fortune 500 companies </li>
									<li>Primary verticle
									<ul>
									<li>BFSI</li>
									<li>Education</li>
									<li>Health Care</li>
									</ul>
									
									</li>
									
									</ul>
									
									</div></div>
									
									<div class="col-md-6">
									
									
										<table class="table">
											<h3 class="company-name">Financials</h3>
											
											<tr>
											<td style="border:none;"></td>
												<td style="border:none; font-size:24px;"><b><u>2017</u></b></td>
												</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Revenue :</span></td>
												<td>$110m - $120m</td>
											</tr>
											<tr>
												<td><span class="text-semibold"> Revenue Growth : </span></td>
												<td>10%YoY </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Gross Margin :	</span></td>
												<td>30-40%  </td>
											</tr>
												<tr>
												<td><span class="text-semibold"> EBITDA Margin :</span></td>
												<td>5-10%</td>
											</tr>
												
										
										</table>
										
										
										
										
										
										<h3 class="company-name">Investment Consideration</h3>
										
										<div><ul>
										<li>Diversified Offerings</li>
										<li>No Concentration, Largest customer is <10% of revenue</li>
										<li>Founder owned and operated</li>
										<li>Capital efficent, No Outside Capital</li>
										<li>40% recurring revenue</li>
										<li><b>Scale:</b> Sizeable enough to win deals in the mid-market but there are many oppourtunities to scale further via M&amp;A</li>
										
										<li><b>Profitable growth:</b> The company can improve this by partnering with the right private equity frim</li> 
										</ul></div>
										
									</div>
										
											
								</div>
 <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a href="edit-sell-side-blind-profile.php" class="btn btn-success a-btn-slide-text">
        <i style="color:#fff; padding-right:15px; font-size:15px; text-align:center;" class="fa fa-edit" aria-hidden="true"></i>
        <span><strong>Edit</strong></span>            
    </a>
						
						
	
	
                </div>
            </div>
        </div> 
							</div>
		  
		  
        </div>
		
		
        <div class="tab-pane" id="sell-side-main3">
          <div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														
														<th>Headline</th>
														<th>Company Name</th>
														<th>Contact Name</th>
														<th>Target’s Check Size</th>
														<th>Target’s Revenue</th>
														<th>Target’s EBITDA</th>
														
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Salesforce</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Jhone watson</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-5M</a></td>
															       
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Jhone watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$10-15M</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce.com (SFDC)</a></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Jhone watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Salesforce Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Jhone watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Nivita Construction</a></td>
														<td><a data-toggle="modal" href="#myModal">Nivita</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Sanju watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Nivita Production</a></td>
														<td><a data-toggle="modal" href="#myModal">Nivita</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Sanju watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														<td><a data-toggle="modal" href="#myModal">TZP Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
												</tbody>
											</table>
										</div>
        </div>
		
		<div class="tab-pane" id="sell-side-main4">
          <div class="row">
											<h1 style="text-align:center;margin-bottom: 20px;">Thank You!</h1>
										
										<p>Now that you have created a Sell Side Screen, you will be assigned a TBP account manager who will schedule a time will walk you though the next steps.</p>
										<p>Meanwhile, to assist the process you can complete the <a href="sell-side-blind-profile.php" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Click here to open form">following forms to start work on a Blind Profile</a>. Your Blind Profile will be published after review with TBP.</p>
										<div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														<th>Target’s Check Size</th>
														<th>Target’s Revenue</th>
														<th>Target’s EBITDA</th>
														<th>Email</th>
														
														<th>Actions</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#myModal">Buy side screen</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-5M</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														
														<td> <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal12">Pursue</button>
						 
						</div></td>
														
						
						
													</tr>
													
													
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#myModal">Sell side engagment </a></td>
														
														<td><a data-toggle="modal" href="#myModal">$10-15M</a></td>
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														<td> <div id="pageMessages"><button class="btn btn-success" onclick="createAlert('','your profile moved to archived.','','success',true,true,'pageMessages');">Pursue</button></div>
						  
						</td>
														
														
						
						
													</tr>
													
												</tbody>
											</table>
											
											
										</div>
								</div>
									
		
        </div>
		
		<div class="tab-pane" id="sell-side-main5">
          <div>
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														<th>Company Name</th>
														<th>Contact Name</th>
														<th>Target Check Size</th>
														<th>Target<sup>,</sup>s Revenue</th>
														<th>Target<sup>,</sup>s EBITDA</th>
														<th>Active It</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														<td><a data-toggle="modal" href="#myModal">TZP Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														<td><a data-toggle="modal" href="#myModal">TZP Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
												</tbody>
											</table
										</div>
        </div>
		
      </div>
   
  </div>
</div>

<!-- Modal -->
  <div class="modal fade matches" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header  green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Salesforce.com (SFDC) Q4 FY19 Earnings Call: Key Takeaways</h4>
        </div>
        <div id="exTab2">	
<ul class="nav nav-tabs">
			<li class="active">
        <a  href="#sell-side-tab1" data-toggle="tab">Blind Profile</a>
			</li>
			<li><a href="#sell-side-tab2" data-toggle="tab">Activity</a>
			</li>
			<li><a href="#sell-side-tab3" data-toggle="tab">Comments</a>
			<li><a href="#sell-side-tab4" data-toggle="tab">Documents</a>

			</li>
		</ul>

			<div class="tab-content ">
			  <div class="tab-pane active" id="sell-side-tab1">
          <div class="content" style="margin-top:20px;">
								<div class="row">
								<div class="col-md-12"><p style="float:right;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></p></div>
								<div class="col-md-12">
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
							<br/>
								
								
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
								
								<div class="col-md-6">
								<h3 class="company-name">Business Details</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US East</li>
									<li><span class="text-semibold">Offices :</span> US, India, Latin America</li>
									<li><span class="text-semibold">Employees :</span> 200+</li>
									</ul>
								
									<h3 class="company-name">Service Offerings</h3>
									<div>
									
									<li>Product Devlopment, testing and Maintenance</li>
									<li>Security & infrastructure Services</li>
									<li>Prepackaged software implementation and Support</li>
									<li>Digital Assets Management</li>
									<li>Microsoft, Oracle, and SAP Maintenance</li>
									<li>Software devlopment  & Maintenance</li>
									
									</ul></div>
									
									
										<h3 class="company-name">Customers</h3>
										<div>
									<ul>
									<li>100+ mid market and enterprise customers </li>
									<li>Marquee accounts, including fortune 500 companies </li>
									<li>Primary verticle
									<ul>
									<li>BFSI</li>
									<li>Education</li>
									<li>Health Care</li>
									</ul>
									
									</li>
									
									</ul>
									
									</div></div>
									
									<div class="col-md-6">
									
									
										<table class="table">
											<h3 class="company-name">Financials</h3>
											
											<tr>
											<td style="border:none;"></td>
												<td style="border:none; font-size:24px;"><b><u>2017</u></b></td>
												</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Revenue :</span></td>
												<td>$110m - $120m</td>
											</tr>
											<tr>
												<td><span class="text-semibold"> Revenue Growth : </span></td>
												<td>10%YoY </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Gross Margin :	</span></td>
												<td>30-40%  </td>
											</tr>
												<tr>
												<td><span class="text-semibold"> EBITDA Margin :</span></td>
												<td>5-10%</td>
											</tr>
												
										
										</table>
										
										
										
										
										
										<h3 class="company-name">Investment Consideration</h3>
										
										<div><ul>
										<li>Diversified Offerings</li>
										<li>No Concentration, Largest customer is <10% of revenue</li>
										<li>Founder owned and operated</li>
										<li>Capital efficent, No Outside Capital</li>
										<li>40% recurring revenue</li>
										<li><b>Scale:</b> Sizeable enough to win deals in the mid-market but there are many oppourtunities to scale further via M&amp;A</li>
										
										<li><b>Profitable growth:</b> The company can improve this by partnering with the right private equity frim</li> 
										</ul></div>
										
									</div>
										
											
								</div>
							</div>
		  
		  
		  
		  </div>



				<div class="tab-pane" id="sell-side-tab2">
          <table class="table table-togglable">
												<thead>
													<tr>
												
														<th>Date</th>
														<th>Name</th>
														<th>Email Id</th>
														<th>Status</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr>
													
													<td>02-11-2020</td>
													<td>jhone</td>
													<td>jhone@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													

<tr>
													
													<td>09-21-2020</td>
													<td>Taden Paul</td>
													<td>paul@example.com</td>
													<td><span class="badge badge-default">Draft</span></td>
													
														
													</tr>
<tr>
													
													<td>10-20-2020</td>
													<td>Harry</td>
													<td>Harry@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-17-2020</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													<tr>
													
													<td>08-17-2020</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-27-2020</td>
													<td>Jhone trend</td>
													<td>trend@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													</table>

				</div>
        <div class="tab-pane" id="sell-side-tab3">
          <div class="panel-body">
              <textarea class="form-control" placeholder="Enter Your here Comments..." rows="3"></textarea><br>
              <a href="#" class="btn btn-success btn-sm pull-right">Comment</a>
              <div class="clearfix"></div>
              <hr>
              <ul class="media-list">
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Rexona Kumi</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, <a href="#"># consectetur adipiscing</a> .</p>
                  </div>
                </li>
                <li class="media">
                 
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ John Doe</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor <a href="#"># ipsum dolor</a> adipiscing elit.</p>
                  </div>
                </li>
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Madonae Jonisyi</strong>
                    <p>Lorem ipsum dolor <a href="#"># sit amet</a> sit amet, consectetur adipiscing elit.</p>
                  </div>
                </li>
              </ul><span class="text-danger">237K users active</span>
            
          </div>
				</div>
				
			<div class="tab-pane" id="sell-side-tab4">
<table class="table">
              <thead>
                <tr >
                  <th style="font-weight: bolder;">Date</th>
                  <th style="font-weight: bolder;">Title</th>
                  
                  <th style="font-weight: bolder;">Download</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>05-09-2018</td>
                  <td>Salesforce.com turnover documents of 2017</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				<tr>
                  <td>10-10-2018</td>
                  <td>Salesforce.com updatted list of company management</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
                <tr>
                  <td>07-01-2019</td>
                  <td>List of company stock holders</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				
                  <tr>
                  <td>03-13-2019</td>
                  <td>Documents of market exchange reports</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
              </tbody>
            </table>


</div>			
				
				
			</div>
        
      </div>
      <div class="modal-footer">
	  <button type="button" style="background-color:#000; color:#fff;" class="btn btn-default"><i class="fa fa-ban" aria-hidden="true"></i> Decline</button>
        <button type="button" style="background-color:green; color:#fff;" class="btn btn-default"><i class="fa fa-pencil"  aria-hidden="true"></i> Signed NDA</button>
      </div>
    </div>
	
	
  </div> </div>

</div></div>


</div></div></div>



  <!--------------------------------------If obtain financing Is Selected-------------------------->
<div class="box blue">
	
	<div class="panel-flat">
                    	<div class="panel-heading">
							
                        
						<div>
 


  <div class="panel-default"> 
    <div class="panel-heading">
      <div class="panel-title">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#buy-side-eng1" data-toggle="tab">Screen Details</a>
          </li>
          <li><a href="#buy-side-eng2" data-toggle="tab">Blind Profile</a>
          </li>
          <li><a href="#buy-side-eng3" data-toggle="tab">Active Matches</a>
          </li>
		  
		  <li><a href="#buy-side-eng4" data-toggle="tab">New Matches</a>
          </li>
		  
		  <li><a href="#buy-side-eng5" data-toggle="tab">Archived Matches</a>
          </li>
		  
		  
        </ul>
      </div>
    </div>
    
    <div class="">
      <div class="tab-content ">
        <div class="tab-pane active" id="buy-side-eng1">
		<div class="content">
								<div class="row">
								
								
								
								<div class="col-md-9">
								<p class="company-name">Velocity Media Partners</p>
								<p>Velocity Want to buy big capital companies with great turnover. $5M+ ARR SaaS business in construction industry</p>
								
								
								
								<h3 class="company-name">Company Description</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
									<div class="col-md-12">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold"> ideal size of the company you are targeting</span></td>
												<td>500+ </td>
											</tr>
											<tr>
												<td><span class="text-semibold">kind of debt do you provide </span></td>
												<td>Senior debt </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Are you interested in looking at turnaround/restructuring opportunities?</span></td>
												<td>Yes  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Industry classification : </span></td>
												<td>Agriculture > Cisco  </td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td>Software > IT > Cloud</td>
											</tr>
											
											
											
										
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td>Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2 class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>

						
						
					</div>
											
								</div>
</div>

							</div>
		
         
		  
		  
        </div>
		
		
        <div class="tab-pane" id="buy-side-eng2">
          
		  
		  <div class="content" style="margin-bottom: 20px;">
								<div class="row" style="margin-bottom: 20px;">
								<div class="col-md-12"><p style="float:right;"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></p></div>
								<div class="col-md-8">
								<p class="company-name">Velocity Media Partners</p>
								<br/>
								<h3> Project Brilliant is a 2000+ well captilized global digital transformation service firm.</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
								<div class="col-md-4">
								<h2 class="company-name">Company Financials</h2>
								<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>
								</div>
								
								</div>
								<div class="row">
								
								
									<div class="col-md-4">
									<h3>Target Offerings</h3>
									<ul>
									
									<li>Cloud Services</li>
									
									<li>PaaS infrastructure</li>
									
									<li>Microsoft Stack</li>
									
									<li>SaaS cloud ERP infrastructure</li>
									
									<li>SAP Hana</li>
									<li>Success factor</li>
									<li>Workday</li>
									<li>Salesforce</li>
									
									<li>SaaS Cloud verticle Applications</li>
									
									<li>BFSI</li>
									<li>CPG</li>
									<li>Retail</li></ul>
									
									
									</div>
									
									
									<div class="col-md-4">
									
										
										<h3>Targets Verticals</h3>
										
										<ul>
										<li>Technology</li>
										<li>BFSI</li>
										<li>Retail</li>
										<li>CPG</li>
										<li>Utilities</li>
										
										</ul>
										
										<h3>Targets Customers</h3>
									<ul>
									<li>US Based </li>
									<li>Global 2000 companies </li>
									<li>Referenceable Client Base</li>
									
									</ul>
									
									
									
										
									</div>
									
									<div class="col-md-4">
									
										
										
										
									
									<h3>Target Business</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US </li>
									
									</ul>
										
										<h3>Investment considerations</h3>
										
										<ul>
										<li>Owner/Founder run companies</li>
										<li>Preferable no instituional capital</li>
										<li>Managment team to stay for at least 2 years post transaction</li>
										<li>Company should have well trained Empolyees</li>
										<li>Company should had great reputation in market</li>
										<li>Company should be in growth turnover</li>
										
										
										</ul>
										
									</div>
									
									
									<div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a href="edit-buy-side-blind-profile.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
						
						
	
                </div>
            </div>
        </div>	
											
								</div>
 
							</div>
		  
		  
		  
        </div>
		
		
        <div class="tab-pane" id="buy-side-eng3">
          <div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														<th>Download</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
												</tbody>
											</table>
										</div>
        </div>
		
		<div class="tab-pane" id="buy-side-eng4">
          <div class="row">
											<h1 style="text-align:center;margin-bottom: 20px;">Thank You!</h1>
										<p>Now that you have created a Buy Side Screen, you will be assigned a TBP account manager who will schedule a time will walk you though the next steps.</p>
										<p>Meanwhile, to assist the process you can complete the <a href="create-buy-side-blind.php" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Click here to open form">following forms to start work on a Blind Profile</a>. Your Blind Profile will be published after review with TBP.</p>
										<div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														<th>Email</th>
														
														<th>Actions</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td>
														
														
														
														
														<a data-toggle="modal" href="#myModal">Sell side screen</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														
														<td> <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal12">Pursue</button>
						 
						</div></td>
														
						
						
													</tr>
													
													
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#myModal">buy side engagment </a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														<td> <div id="pageMessages"><button class="btn btn-success" onclick="createAlert('','your profile moved to archived.','','success',true,true,'pageMessages');">Pursue</button></div>
						  
						</td>
														
														
						
						
													</tr>
													
												</tbody>
											</table>
											
											
										</div>
								</div>
									
		
        </div>
		
		<div class="tab-pane" id="buy-side-eng5">
          <div>
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Target<sup>,</sup>s Revenue</th>
														<th>Target<sup>,</sup>s EBITDA</th>
														<th>Active It</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
												</tbody>
											</table
										</div>
        </div>
		
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
  <div class="modal fade matches" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header  green">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Velocity Media Partners limited Requires well capitalized firm</h4>
        </div>
        <div id="exTab2">	
<ul class="nav nav-tabs">
			<li class="active">
        <a  href="#buy-side-eng-modeltab1" data-toggle="tab">Blind Profile</a>
			</li>
			<li><a href="#buy-side-eng-modeltab2" data-toggle="tab">Activity</a>
			</li>
			<li><a href="#buy-side-eng-modeltab3" data-toggle="tab">Comments</a>
			<li><a href="#buy-side-eng-modeltab4" data-toggle="tab">Documents</a>

			</li>
		</ul>

			<div class="tab-content ">
			  <div class="tab-pane active" id="buy-side-eng-modeltab1">
          <div class="content">
								<div class="row">
								<br/>
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-12">
								
								
								<h3> Project Brilliant is a 2000+ well captilized global digital transformation service firm.</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
								
								
								<div class="col-md-12">
								<h2 class="company-name">Company Financials</h2>
								<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>
								</div>
								
								</div>
								<div class="row">
								
								
									<div class="col-md-6">
									<h3>Target Offerings</h3>
									<ul>
									
									<li>Cloud Services
									<ul>
									<li>PaaS infrastructure
									<ul>
									<li>Microsoft Stack</li></ul></li>
									
									<li>SaaS cloud ERP infrastructure
									<ul>
									<li>SAP Hana</li>
									<li>Success factor</li>
									<li>Workday</li>
									<li>Salesforce</li></ul></li>
									
									<li>SaaS Cloud verticle Applications
									<ul>
									<li>BFSI</li>
									<li>CPG</li>
									<li>Retail</li></ul></li></ul></li>
									
									
									</ul>
									
									
									
										
										<h3>Targets Verticals</h3>
										
										<ul>
										<li>Technology</li>
										<li>BFSI</li>
										<li>Retail</li>
										<li>CPG</li>
										<li>Utilities</li>
										
										</ul>
										
										
									
									
									
										
									</div>
									
									<div class="col-md-6">
									
										
										<h3>Targets Customers</h3>
									<ul>
									<li>US Based </li>
									<li>Global 2000 companies </li>
									<li>Referenceable Client Base</li>
									
									</ul>
										
									
									<h3>Target Business</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US </li>
									
									</ul>
										
										<h3>Investment considerations</h3>
										
										<ul>
										<li>Owner/Founder run companies</li>
										<li>Preferable no instituional capital</li>
										<li>Managment team to stay for at least 2 years post transaction</li>
										<li>Company should have well trained Empolyees</li>
										<li>Company should had great reputation in market</li>
										<li>Company should be in growth turnover</li>
										
										
										</ul>
										
									</div>
									
									
										
											
								</div>
				</div></div>



				<div class="tab-pane" id="buy-side-eng-modeltab2">
          <table class="table table-togglable">
												<thead>
													<tr>
												
														<th>Date</th>
														<th>Name</th>
														<th>Email Id</th>
														<th>Status</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr>
													
													<td>02-11-2020</td>
													<td>jhone</td>
													<td>jhone@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													

<tr>
													
													<td>09-21-2020</td>
													<td>Taden Paul</td>
													<td>paul@example.com</td>
													<td><span class="badge badge-default">Draft</span></td>
													
														
													</tr>
<tr>
													
													<td>10-20-2020</td>
													<td>Harry</td>
													<td>Harry@example.com</td>
													<td><span class="badge badge-default">Active</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-17-2020</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													<tr>
													
													<td>08-17-2020</td>
													<td>Seema</td>
													<td>seema@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													
													<tr>
													
													<td>08-27-2020</td>
													<td>Jhone trend</td>
													<td>trend@example.com</td>
													<td><span class="badge badge-default">Archived</span></td>
													
														
													</tr>
													</table>

				</div>
        <div class="tab-pane" id="buy-side-eng-modeltab3">
          <div class="panel-body">
              <textarea class="form-control" placeholder="Enter Your here Comments..." rows="3"></textarea><br>
              <a href="#" class="btn btn-success btn-sm pull-right">Comment</a>
              <div class="clearfix"></div>
              <hr>
              <ul class="media-list">
                <li class="media">
                 
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Rexona Kumi</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, <a href="#"># consectetur adipiscing</a> .</p>
                  </div>
                </li>
                <li class="media">
                  
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ John Doe</strong>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor <a href="#"># ipsum dolor</a> adipiscing elit.</p>
                  </div>
                </li>
                <li class="media">
                 
                  <div class="media-body">
                    <span class="text-muted pull-right"><small class="text-muted">30 min ago</small></span> <strong class="text-success">@ Madonae Jonisyi</strong>
                    <p>Lorem ipsum dolor <a href="#"># sit amet</a> sit amet, consectetur adipiscing elit.</p>
                  </div>
                </li>
              </ul><span class="text-danger">237K users active</span>
            
          </div>
				</div>
				
			<div class="tab-pane" id="buy-side-eng-modeltab4">
<table class="table">
              <thead>
                <tr >
                  <th style="font-weight: bolder;">Date</th>
                  <th style="font-weight: bolder;">Title</th>
                  
                  <th style="font-weight: bolder;">Download</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>05-09-2018</td>
                  <td>Required turnover documents of 2017</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				<tr>
                  <td>10-10-2018</td>
                  <td>shared terms & condition document</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
                <tr>
                  <td>07-01-2019</td>
                  <td>List of company stock holders</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
				
				
                  <tr>
                  <td>03-13-2019</td>
                  <td>Required stock market exchange reports</td>
                  
                  
                  <td><a href="#"><i class="fa fa-download" data-popup="tooltip" title="Download Documents"></i></a></td>
                </tr>
                
              </tbody>
            </table>


</div>			
				
				
			</div>
        
      </div>
      <div class="modal-footer">
	  <button type="button" style="background-color:#000; color:#fff;" class="btn btn-default"><i class="fa fa-ban" aria-hidden="true"></i> Decline</button>
        <button type="button" style="background-color:green; color:#fff;" class="btn btn-default"><i class="fa fa-pencil"  aria-hidden="true"></i> Signed NDA</button>
      </div>
    </div>
	
	
  </div> </div>

</div></div>
         
		  
        </div></div></div></div></div></div>
		
		
        <div class="tab-pane" id="engagment-tab3">
          <div class="content" style="margin-bottom: 20px;">
								<div class="row" style="margin-bottom: 20px;">
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-8">
								<p class="company-name">Velocity Media Partners</p>
								<br/>
								<h3> Project Brilliant is a 2000+ well captilized global digital transformation service firm.</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
								<div class="col-md-4">
								<h2 class="company-name">Company Financials</h2>
								<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>
								</div>
								
								</div>
								<div class="row">
								
								
									<div class="col-md-4">
									<h3>Target Offerings</h3>
									<ul>
									
									<li>Cloud Services</li>
									
									<li>PaaS infrastructure</li>
									
									<li>Microsoft Stack</li>
									
									<li>SaaS cloud ERP infrastructure</li>
									
									<li>SAP Hana</li>
									<li>Success factor</li>
									<li>Workday</li>
									<li>Salesforce</li>
									
									<li>SaaS Cloud verticle Applications</li>
									
									<li>BFSI</li>
									<li>CPG</li>
									<li>Retail</li></ul>
									
									
									</div>
									
									
									<div class="col-md-4">
									
										
										<h3>Targets Verticals</h3>
										
										<ul>
										<li>Technology</li>
										<li>BFSI</li>
										<li>Retail</li>
										<li>CPG</li>
										<li>Utilities</li>
										
										</ul>
										
										<h3>Targets Customers</h3>
									<ul>
									<li>US Based </li>
									<li>Global 2000 companies </li>
									<li>Referenceable Client Base</li>
									
									</ul>
									
									
									
										
									</div>
									
									<div class="col-md-4">
									
										
										
										
									
									<h3>Target Business</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US </li>
									
									</ul>
										
										<h3>Investment considerations</h3>
										
										<ul>
										<li>Owner/Founder run companies</li>
										<li>Preferable no instituional capital</li>
										<li>Managment team to stay for at least 2 years post transaction</li>
										<li>Company should have well trained Empolyees</li>
										<li>Company should had great reputation in market</li>
										<li>Company should be in growth turnover</li>
										
										
										</ul>
										
									</div>
									
									<div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				
				<a href="edit-sell-side-blind-profile.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
				
                      
                </div>
            </div>
        </div> 
										
											
								</div>

							</div>
								</div>
		
		<div class="tab-pane" id="engagment-tab4">
          
          <div class="">
									
											<table class="table dataTables-example">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>03-11-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														<td>Comvest Partners</td>
														
														
														<td>$6m</td>
														
														<td> $1.3M</td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td>04-15-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														<td>TZP Group</td>
														
														
														<td>$3m</td>
														
														<td>$1.5M</td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-25-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														<td>Velocity Media Partners</td>
														
														
														<td>$13m</td>
														
														<td>$3.5M</td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													
													
													
													
													
													
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-29-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														<td>TZP Group</td>
														
														
														<td>$3m</td>
														
														<td>$1.5M</td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
												</tbody>
											</table>
										</div>
      
								</div>
									
		
        
		
		<div class="tab-pane" id="engagment-tab5">
		
		
          <table class="table dataTables-example">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
												</tbody>
											</table>
        </div>
		
		<div class="tab-pane" id="engagment-tab6">
		
		
          <table class="table dataTables-example">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Target<sup>,</sup>s Revenue</th>
														<th>Target<sup>,</sup>s EBITDA</th>
														
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>03-11-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														<td>Comvest Partners</td>
														
														
														<td>$6m</td>
														
														<td> $1.3M</td>
														
														
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td>04-15-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														<td>TZP Group</td>
														
														
														<td>$3m</td>
														
														<td>$1.5M</td>
														
														
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-25-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														<td>Velocity Media Partners</td>
														
														
														<td>$13m</td>
														
														<td>$3.5M</td>
														
													
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-26-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td>Velocity Media Partners</td>
														
														
														<td>$13m</td>
														
														<td>$3.5M</td>
														
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td>Velocity Media Partners</td>
														
														
														<td>$13m</td>
														
														<td>$3.5M</td>
														
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td>Velocity Media Partners</td>
														
														
														<td>$13m</td>
														
														<td>$3.5M</td>
														
														
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-29-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														<td>TZP Group</td>
														
														
														<td>$3m</td>
														
														<td>$1.5M</td>
														
														
													</tr>
													
												</tbody>
											</table>
        </div>
		
      </div>
    </div>
  </div>
</div>

        </div>
       
  

</div></div></div>



<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function(){
    $('input[type="radio"]').click(function(){
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".box").not(targetBox).hide();
        $(targetBox).show();
    });
});
</script>
  
<?php require_once('footer.php'); ?>

   