<?php require_once('header.php'); ?>


        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>
            <div class="wrapper wrapper-content">
        
        <!-- Page header -->
				<div class="page-header">
					<div class="page-header-content row">
						<div class="page-title col-md-9">
							<h3 style="padding-left: 25px;"><span class="text-semibold">Home</span></h3>
						</div>

						<div class="heading-elements col-md-3">
							<div class="heading-btn-group">
								<div class="btn-group">
	                                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown">Create A New Screen</button>
									<ul class="dropdown-menu">
										<li><a href="create_sell_side_screen.php">Sell-Side Screen</a></li>
											<li><a href="create_buy_side_screen.php">Buy-Side Screen</a></li>
									</ul>
	                            </div>
							</div>
						</div>
					</div>
				</div>
				<!-- /page header -->
				
				
				<!-- Content area -->
				<div class="content">
				<div class="row admin-dashboard">
						<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">Active Users</h5>
								<table class="table table-togglable table-hover">
									<table class="table">
										<thead>
											<tr>
												<th>Full Name</th>
												<th>Email</th>
												<th>Last Activity</th>
												<th>Last Activity Time</th>
												<th>Login</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>James Smith</td>
												<td>james@gmail.com</td>
												<td>Archived</td>
												
												<td>03:24am</td>
												<td>07:24am</td>
												
											</tr>
											<tr>
												<td>James Smith</td>
												<td>james@gmail.com</td>
												<td>Archived</td>
												
												<td>03:24am</td>
												<td>07:24am</td>
												
											</tr>
											<tr>
												<td>James Smith</td>
												<td>james@gmail.com</td>
												<td>Archived</td>
												
												<td>03:24am</td>
												<td>07:24am</td>
												
											</tr>
											
											<tr>
												<td>James Smith</td>
												<td>james@gmail.com</td>
												<td>Archived</td>
												
												<td>03:24am</td>
												<td>07:24am</td>
												
											</tr>
											
											<tr>
												<td>James Smith</td>
												<td>james@gmail.com</td>
												<td>Archived</td>
												
												<td>03:24am</td>
												<td>07:24am</td>
												
											</tr>
											
											
										</tbody>
									</table>
									
								</div>
							</div>
						
						<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">Unverified Users</h5>
								<table class="table table-togglable table-hover">
									<table class="table">
										<thead>
											<tr>
												<th>Full Name</th>
												<th>Email</th>
												<th>Profile Activated at</th>
												<th>Last Activity</th>
												<th>Last Activity Time</th>
												
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>James Smith</td>
												<td>james@gmail.com</td>
												<td>05:25am</td>
												
												<td>03:24am</td>
												<td>07:24am</td>
												
											</tr>
											<tr>
												<td>James Smith</td>
												<td>james@gmail.com</td>
												<td>05:25am</td>
												
												<td>03:24am</td>
												<td>07:24am</td>
												
											</tr>
											<tr>
												<td>James Smith</td>
												<td>james@gmail.com</td>
												<td>05:25am</td>
												
												<td>03:24am</td>
												<td>07:24am</td>
												
											</tr>
											
										</tbody>
									</table>
									
								</div>
							</div>
						</div>
					
					
				<div class="row admin-dashboard">	
				<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">Buy-Side Screens</h5>
								<h4 class="text-semibold"><a href="create_buy_side_screen.php"><i class="fa fa-plus-circle"></i></a></h4>
								<table class="table table-togglable table-hover">
									<thead>
										<tr>
											<th>Screen Title</th>
											<th>Status</th>
											<th>Purpose of Screen</th>
											<th>Notification</th>
										</tr>
									</thead>
									<tbody>
										<tr class="bg-default">
											<td><a href="view-buy-side-screen-main-screen.php">Velocity wants ERP and CRM Software company</a></td>
											<td><span class="badge badge-default">DRAFT</span></td>
											<td>Raise Capital</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
										<tr>
											<td><a href="edit-buy-side-screen.html">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
<tr class="bg-default">
											<td><a href="edit-buy-side-screen.html">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>

<tr>
											<td><a href="edit-buy-side-screen.html">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
									</tbody>
								</table>
								
								<p class="text-right"><a href="buy-side-screens.php">View More</a></p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">Sell-Side Screens</h5>
								<h4 class="text-semibold"><a href="create_sell_side_screen.php"><i class="fa fa-plus-circle"></i></a></h4>
								<table class="table table-togglable table-hover">
									<thead>
										<tr>
											<th>Screen Title</th>
											<th>Status</th>
											<th>Purpose of Screen</th>
											<th>Notification</th>
										</tr>
									</thead>
									<tbody>
										<tr class="bg-default">
											<td><a href="view-sell-side-screen-main-screen.php">Salesforce.com (SFDC) Q4 FY19 Earnings Call</a></td>
											<td><span class="badge badge-default">DRAFT</span></td>
											<td>Raise Capital</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
										<tr>
											<td><a href="view-sell-side-screen-main-screen.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>

<tr class="bg-default">
											<td><a href="view-sell-side-screen-main-screen.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>

<tr>
											<td><a href="view-sell-side-screen-main-screen.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>

									</tbody>
								</table>
								<p class="text-right"><a href="sell-side-screens.php">View More</a></p>
							</div>
						</div></div>
					
				
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">Engagement</h5>
								
								<table class="table table-togglable table-hover">
									<thead>
										<tr>
											<th>Engagement Name</th>
											<th>Status</th>
											<th>Purpose of engagement</th>
											<th>Notification</th>

										</tr>
									</thead>
									<tbody>
										<tr class="bg-default">
											<td><a href="../admin/engagements.php">Salesforce.com (SFDC) Q4 FY19</a></td>
											<td><span class="badge badge-default">DRAFT</span></td>
											<td>Raise Capital</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
										<tr>
											<td><a href="../admin/engagements.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">ARCHIVED</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
										
										<tr>
											<td><a href="../admin/engagements.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">Active</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
											
<tr>
											<td><a href="../admin/engagements.php">Untitled Project</a></td>
											<td><span class="badge bg-darkb">Active</span></td>
											<td>Sell a Business</td>
											<td><a href=""><span class="text-danger"><i class="fa fa-bell"></i></span></a></td>
										</tr>
										
										
									</tbody>
								</table>
								<p class="text-right"><a href="engagements.php">View More</a></p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">Recent Notifications</h5>
						
								<table class="table table-togglable table-hover">
									<thead>
										<tr>
											<th>Timestamp</th>
											<th>Title</th>
											<th>Details</th>
											<th>Target Employee</td>
											<th>Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><a href="view-engagement.html">02-13-2020 11:30 am</a></td>
											<td><span class="">TBP add New Project</span></td>
											<td>TBP Project Detail</td>
											<td>Sunil Grover</td>
											
											<td class="notifi-icon"><span class="text-danger"><i class="fa fa-check"></i> Unread</span></td>
											
											
											<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#">Mark Read</a>
            <li><a class="dropdown-item" href="#"> Mark Unread</a>
          
			<li><a class="dropdown-item" href="#">Delete</a></li>
			     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
										</tr>
										<tr>
										
											<td><a href="view-engagement.html">02-23-2020 12:30 am</a></td>
											<td><span class="">TBP add New Project</span></td>
											<td>TBP Project Detail</td>
											
											<td>Jhonty Watson</td>
											<td><span class="text-success"> <i class="fa fa-check-double"></i> Read</span></td>
											<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#">Mark Read</a>
            <li><a class="dropdown-item" href="#"> Mark Unread</a>
          
			<li><a class="dropdown-item" href="#">Delete</a></li>
			     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>

										</tr>
<tr>
										
											<td><a href="view-engagement.html">02-23-2020 12:30 am</a></td>
											<td><span class="">TBP add New Project</span></td>
											<td>TBP Project Detail</td>
											
											<td>Jhonty Watson</td>
											<td><span class="text-success"> <i class="fa fa-check-double"></i> Read</span></td>
											<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#">Mark Read</a>
            <li><a class="dropdown-item" href="#"> Mark Unread</a>
          
			<li><a class="dropdown-item" href="#">Delete</a></li>
			     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>

										</tr>
										
								
									</tbody>
								</table>
								<p class="text-right"><a href="notification.php">View More</a></p>
							</div>
						</div></div>
						
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-body border-top-primary">
								<h5 class="text-semibold">My Notifications</h5>
						
								<table class="table table-togglable table-hover">
									<thead>
										<tr>
										
											<th>Timestamp</th>
											<th>Title</th>
											<th>Details</th>
											<th>Target Employee</td>
											<th>Status</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><a href="view-engagement.html">02-13-2020 11:30 am</a></td>
											<td><span class="">TBP add New Project</span></td>
											<td>TBP Project Detail</td>
											<td>Sunil Grover</td>
											
											<td class="notifi-icon"><span class="text-danger"><i class="fa fa-check"></i> Unread</span></td>
											
											
											<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#">Mark Read</a>
            <li><a class="dropdown-item" href="#"> Mark Unread</a>
          
			<li><a class="dropdown-item" href="#">Delete</a></li>
			     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
										</tr>
										<tr>
										
											<td><a href="view-engagement.html">02-23-2020 12:30 am</a></td>
											<td><span class="">TBP add New Project</span></td>
											<td>TBP Project Detail</td>
											
											<td>Jhonty Watson</td>
											<td><span class="text-success"> <i class="fa fa-check-double"></i> Read</span></td>
											<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#">Mark Read</a>
            <li><a class="dropdown-item" href="#"> Mark Unread</a>
          
			<li><a class="dropdown-item" href="#">Delete</a></li>
			     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>

										</tr>
										
								
									</tbody>
								</table>
								<p class="text-right"><a href="notification.php">View More</a></p>
							</div>
						</div>

</div>
					
				
		

    <?php require_once('footer.php'); ?>