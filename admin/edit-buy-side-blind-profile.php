<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Edit buy side Blind Profile</span></h4>
            </div>

             <div class="heading-elements">
              <div class="heading-btn-group">
                               
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
		<div class="steps-basic  " action="#" >
							
							
							
							
							<div class="content" style="margin-bottom: 20px;">
								<div class="row" style="margin-bottom: 20px;">
								<div class="col-md-8">
								<p class="company-name" contenteditable="true">Velocity Media Partners</p>
								<br/>
								<h3 contenteditable="true"> Project Brilliant is a 2000+ well captilized global digital transformation service firm.</h3>
								<p contenteditable="true">Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
								<div class="col-md-4">
								<h2 class="company-name">Company Financials</h2>
								<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td contenteditable="true">10-25M</td>
					
					<td contenteditable="true">$1-5M</td>
					
					</tr>
					
					
					</table>
								</div>
								
								</div>
								<div class="row">
								
								
									<div class="col-md-4">
									<h3>Target Offerings</h3>
									<div contenteditable="true">
									<ul>
									
									<li>Cloud Services</li>
									
									<li>PaaS infrastructure</li>
									
									<li>Microsoft Stack</li>
									
									<li>SaaS cloud ERP infrastructure</li>
									
									<li>SAP Hana</li>
									<li>Success factor</li>
									<li>Workday</li>
									<li>Salesforce</li>
									
									<li>SaaS Cloud verticle Applications</li>
									
									<li>BFSI</li>
									<li>CPG</li>
									<li>Retail</li></ul>
									
									</div>
									</div>
									
									
									<div class="col-md-4">
									
										
										<h3>Targets Verticals</h3>
										<div contenteditable="true">
										<ul>
										<li>Technology</li>
										<li>BFSI</li>
										<li>Retail</li>
										<li>CPG</li>
										<li>Utilities</li>
										
										</ul>
										</div>
										
										<h3>Targets Customers</h3>
										<div contenteditable="true">
									<ul>
									<li>US Based </li>
									<li>Global 2000 companies </li>
									<li>Referenceable Client Base</li>
									
									</ul></div>
									
									
									
										
									</div>
									
									<div class="col-md-4">
									
										
										
										
									
									<h3>Target Business</h3>
									<ul>
									<li><b>HQ. :</b> <span contenteditable="true">USA, UK</span> </li>
									
									</ul>
										
										<h3>Investment considerations</h3>
										<div  contenteditable="true">
										<ul>
										<li>Owner/Founder run companies</li>
										<li>Preferable no instituional capital</li>
										<li>Managment team to stay for at least 2 years post transaction</li>
										<li>Company should have well trained Empolyees</li>
										<li>Company should had great reputation in market</li>
										<li>Company should be in growth turnover</li>
										
										
										</ul>
										
									</div></div>
									
									
										
											
								</div>
<div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a href="edit-sell-side-blind-profile.php" class="btn btn-success a-btn-slide-text">
        <i class="fa fa-floppy-o" aria-hidden="true"></i>
        <span><strong>Update</strong></span>            
    </a>
						
						
	
	
                </div>
            </div>
        </div> 
							</div>


						
							
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   