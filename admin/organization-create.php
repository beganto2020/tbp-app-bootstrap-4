<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Organizations</span> </h4>
            </div>

           <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                                <a href="organization-create.php" class="dt-button buttons-selected btn btn-default legitRipple">New  </a>
                                <a href="#" class="dt-button buttons-selected btn btn-default legitRipple">Import</a>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;">Account  Information</h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class="">Company Name <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Provide Your Company name"><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label>Website  <a href="#"data-toggle="tooltip" data-placement="top" title="" data-original-title="Select Your Official Company Website"><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Phone <sup>*</sup><a href="#"data-toggle="tooltip" data-placement="top" title="" data-original-title="Provide Contact No"><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>

                   
                       <!--=======================================================Row Design  ====================================------------>

                         <div class="row" >
                     <h5></h5>
                 
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label>Revenue<sup>*</sup> <a href="#"data-toggle="tooltip" data-placement="top" title="" data-original-title="What is your Company Revenue ?"><i class="fa fa-info-circle"></i></a></label>
                                   <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Revenue</option>
                        <option value="opt2">>10</option>
                        <option value="opt3">10-50</option>
                        <option value="opt4">50-100</option>
                        <option value="opt5">>100</option>
                                                
                    </select>
            </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Account Source <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Provide Account Source"><i class="fa fa-info-circle"></i></a></label>
                        <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Web</option>
                        <option value="opt2">Google</option>
                        
                                                
                    </select>
              </div>
                      </div>
                    </div>
                 
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>Company Description<sup>*</sup></label>
                        
                        <textarea class="form-control border"   rows="1" cols="10"></textarea>
                      </div>
                    </div>
					 </div>
                      <!--=======================================================Row Design  ====================================------------>

                  <div class="row" >
                   
                     <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>No of Employee</label>
                          <label style="display: block;"></label>
                          <label style="display: block;"></label>
                            
                        
                          <input type="text" class="form-control">
                      </div>
                    </div>
                       <div class="col-md-4">
                        <label>&nbsp;</label>
                    <label class="display-block text-semibold">Account Owner<sup>*</sup></label>
                     <select id="done"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Ownership">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      </select>
                    </div>
                    
                  </div>



<!--=======================================================Row Design  ====================================------------>


                       <div class="row" style="margin-top: 30px;" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;">Address Information</h5>
                    <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        
                       <label>Street</label>
                        
                        <textarea class="form-control border"   rows="1" cols="10"></textarea>
                      </div>
                    </div>
                   
                     <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>City<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div> </div>
						
						
                       <div class="col-md-4">
                           <label>&nbsp;</label>
                         <div class="form-group">
                        <label>State/Province<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div></div>
					  
					  <div class="col-md-4">
					  <label>&nbsp;</label>
                       <div class="form-group">
                        <label>Country<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div>
                    
                    </div>
					
					<div class="col-md-4" >
						<label>&nbsp;</label>
                       <div class="form-group">
                        <label>Zip/Postal Code<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div>
					  
                    </div>
                    
                  </div>


 
              <div class="row">
                <h5>Additional Information</h5>

                    <div class="col-md-6">
                       
                      <div class="form-group">
              <label class="display-block text-semibold">Entity Type <sup>*</sup></label>
              <label class="checkbox-inline">
                <input type="checkbox" id="ocCk" onchange="valueChanged()">
                Operating Company
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="invCk" onchange="valueChanged()">
                Investor
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="lenCk" onchange="valueChanged()">
                Lender
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="otrCk" onchange="valueChanged()">
                Other
              </label>
            </div>
                    </div>

                    

                       <!--- ======================================================= Oerating  Company ===========================================------------->
        <div class="row" id="oc">
                    <div class="col-md-12">
            <fieldset class="customFieldset">
              
              <h6 style="color: #a976f5;"> Operating Company<sup>*</sup></h6>         
            
                <div class="row " >
                <div class="col-md-3">
                  <div class="row">
                  <!--   -->
                  <div class="col-md-12">
                    <div class="form-group mt-3">
                    <label class="display-block text-semibold">Company Type</label>
                    <label class="radio-inline">
                      <input type="radio" name="radio-inline-left" class="styled" checked="checked">
                      Large Co
                    </label>

                    <label class="radio-inline">
                      <input type="radio" name="radio-inline-left" class="styled">
                      Prospect
                    </label>
                  </div>
                    
                  </div>
                  
                  </div>
                </div>
				
				<div class="col-md-2">
                    <label>&nbsp;</label>
                     <div class="form-group">
                  <input name="" type="checkbox" value=""><label>Publicly Traded? <sup>*</sup></label>
                  
                  </div>
                  </div>

                   <div class="col-md-3">

                         <div class="form-group">
                  <label>Parent Company / Investors / PrivateEquity <sup>*</sup></label>
                      <select id="done"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Parent Company">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      <optgroup label="Investors">
                        <option value="IL" >PrivateEquity</option>
                        <option value="IA">Investors</option>
                        <option value="KS">Parent</option>
                      
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        <option value="MA">Massachusetts</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                  </div>
                  
                </div>

                   <div class="col-md-3">

                      <div class="form-group">
                  <label>Subsidiary Companies / Affiliate Companies<sup>*</sup></label>           
                    <select id="done"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Subsidiary Companies</option>
                        <option value="CO">Affiliate Companies</option>
                        <option value="ID">TBP</option>
                      
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >Subsidiary Companies</option>
                        <option value="IA">Affiliate Companies</option>
                        <option value="KS">TBP</option>
                        <option value="KY">Kentucky</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        
                      </optgroup>
                    </select>
                  </div>
                
                </div>


                
                </div>
            
               
            </fieldset>
                    </div>
                  </div>

                         <div class="row" id="inv">
                    <div class="col-md-12">
            <fieldset class="customFieldset" style="margin-top:30px;" >
              <!-- <legend>Investor</legend> -->
              <h6 style="color: #a976f5;">Investor</h6>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="multi-select-full">
                        <select id="done"  class="selectpicker form-control" multiple data-done-button="true">
                        <option value="1">PE/Buyout</option>
                        <option value="2">Venture Capital</option>
                        <option value="3">Fund of Fund</option>
                        <option value="4">Institutional LP</option>
                        <option value="5">Individual Accredited</option>
                        <option value="6">Individual UHNW</option>
                        <option value="7">Family Office</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-4">  
                  <div class="form-group">
                    <label>Parent Company / Investors / PrivateEquity </label>
                     <select id="done"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >PrivateEquity</option>
                        <option value="IA">Investors</option>
                        <option value="KS">Parent</option>
                      
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        <option value="MA">Massachusetts</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                      <div class="form-group">
                  <label>Subsidiary Companies / Affiliate Companies<sup>*</sup></label>           
                    <select id="done"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Subsidiary Companies</option>
                        <option value="CO">Affiliate Companies</option>
                        <option value="ID">TBP</option>
                      
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >Subsidiary Companies</option>
                        <option value="IA">Affiliate Companies</option>
                        <option value="KS">TBP</option>
                        <option value="KY">Kentucky</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        
                      </optgroup>
                    </select>
                  </div>
              </div>
              </div>

            

            </fieldset>
                    </div>
                  </div>

                        <div class="row" id="len">
                    <div class="col-md-12">
            <fieldset class="customFieldset" style="margin-top:30px;">
              <!-- <legend>Lender</legend> -->
              <h6 style="color: #a976f5;">Lender</h6>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="">
                      <select class="form-control" name="color2" onchange='CheckColors2(this.value);'>
                        <option value="1">Working capital</option>
                        <option value="2">Senior Debt</option>
                        <option value="3">Mz Debt</option>
                        <option value="4">Venture Debt</option>
                        <option value="5">Other</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-6"><input  type="text" name="color2" placeholder="Enter If Other Type" id="color2" class="form-control" style="display:none;"></div>
              </div>
            </fieldset>
             <script type="text/javascript">
function CheckColors2(val){
 var element=document.getElementById('color2');
 if(val=='5')
   element.style.display='block';
 else  
   element.style.display='none';
}

</script> 
                    </div>
                  </div>

                     <div class="row" id="otr">
                    <div class="col-md-12">
            <fieldset class="customFieldset" style="margin-top:30px;">
              <!-- <legend>Other</legend> -->
              <h6 style="color: #a976f5;">Other</h6>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="">
                      <select class="form-control" name="color" onchange='CheckColors(this.value);'>
                        <option value="1">Accountants</option>
                        <option value="2">Lawyers</option>
                        <option value="3">Investment Banker/Broker</option>
                        <option value="4">Media</option>
                        <option value="5">Other</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-6"><input  type="text" name="color" placeholder="Enter If Other Type" id="color" class="form-control" style="display:none;"></div>
              </div>
            </fieldset>

                    </div>
                  </div>

           <script type="text/javascript">
function CheckColors(val){
 var element=document.getElementById('color');
 if(val=='5')
   element.style.display='block';
 else  
   element.style.display='none';
}

</script> 
                     


                  </div>
                       <!--=======================================================Row Design  ====================================------------>







  <div class="row" >
                     <h5> </h5>
                    <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>Business Category <sup>*</sup></label>
                    


                        <div class="radio-box border" >
                        
                        <div class="form-group" style="border:1px solid #ddd; padding: 10px;" >
                    <label class="display-block text-semibold">Software</label>
                    <label class="checkbox-inline">
                      <input type="checkbox" checked="checked">
                      IT
                    </label>

                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Cloud Infrastructure
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      CRM

                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Data Analytics

                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      HR/HCM


                    </label>
                      <label class="checkbox-inline">
                      <input type="checkbox">
                      Supply Chain

                    </label><br/><br/><br/>
                    <a href="" class="display-block" data-toggle="modal" data-target="#treeModal">More</a>
              </div>

                      </div>
                    </div>
                    </div>
                   
                     <div class="col-md-4" >
                      <label>&nbsp;</label>
                   <div class="form-group">
                        <label>&nbsp;</label>
                       

                        <div class="radio-box border">
                        
                        <div class="form-group" style="border:1px solid #ddd; padding: 10px;" >
                    <label class="display-block text-semibold">IT Services</label>
                    <label class="checkbox-inline">
                      <input type="checkbox" checked="checked">
                      IT
                    </label>

                    <label class="checkbox-inline">
                      <input type="checkbox">
                      CRM
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      HR/HCM
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Supply Chain
                    </label>
<br/><br/><br/>
                    <a href="" class="display-block" data-toggle="modal" data-target="#treeModal1">More</a>
                  </div>

                      </div>
                    </div>
                    </div>
					
					
                       <div class="col-md-4">
                            <label>&nbsp;</label>
                   <div class="form-group">
                       
                       <label>&nbsp;</label>

                        <div class="radio-box border">
                        
                        <div class="form-group" style="border:1px solid #ddd; padding:3px 20px;" >
                    <label class="display-block text-semibold">Others</label>
                

                   <input type="text" name="" class="form-control">
                  
 <a href="" class="display-block" style="padding: 0px;" data-toggle="modal" data-target="#treeModal1">&nbsp;</a>
                  </div>

                      </div>
                    </div>


                    </div>  </div>
					
					<div class="row">
					<div class="col-md-6">
												<div class="form-group">
												<label  class="display-block text-semibold">Target Verticals </label>
												<label>In which industries do you have strong customer success and presence</label>
												<fieldset class="custom-fieldset">
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														Agriculture
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Manufacturing
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Fintech & BFSI
													</label>
														
													<label class="checkbox-inline">
														<input type="checkbox">
														Engineering & Construction
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Education
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Telecomm
													</label>
													
												</fieldset>
											</div>
											</div>
											
											
											<div class="col-md-6">
											<label  class="display-block text-semibold">Hardware Ecosystem </label>
												<label>&nbsp;</label>
												<label>&nbsp;</label>
												
													
													<fieldset class="custom-fieldset">
													
													<label class="checkbox-inline">
														<input type="checkbox" checked="checked">
														IBM
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														VMWare
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Dell
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														EMC
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Cisco
													</label>

													<label class="checkbox-inline">
														<input type="checkbox">
														Juniper
													</label>
													
												</fieldset>


											</div>
										</div>

                    
              


                
               <!--    </div> -->
                      


         <div class="row mt-5 mb-5" >
                    
                    <div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				
				<a href="organizations.php"><button type="button" class="btn btn-primary legitRipple"> Save</button></a>
				
				<a href="#"><button type="button" class="btn btn-primary legitRipple"> Cancel</button></a>
				
				
                       
						
	
                </div>
            </div>     



                    
            </div> </div>
			
			
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   