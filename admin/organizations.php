<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Organizations</span> </h4>
            </div>

           <div class="heading-elements col-md-3">
              <div class="heading-btn-group">
                                <a href="organization-create.php" class="dt-button buttons-selected btn btn-default legitRipple">New  </a>
                                <a href="#" class="dt-button buttons-selected btn btn-default legitRipple">Import</a>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="table-responsive">
                    <table class="table dataTables-example" >
                    <thead>
                                <tr><th width="1%;"><input type="checkbox"></td></th>
                                    <th>Name</th>
									<th>Type</th>
                                    <th>Location</th>
                                    <th>Website</th>
                                    <th>Classification</th>
                                    <th>Primary Contact Email</th>
                                    <th>Primary Contact's Phone</th>
                                    <th>Revenue</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company</a> </td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                   <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            

                                                   <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                               <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                  <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                 </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                         <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                 <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                         <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                               <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                         <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                                 
                                
                                
                               
                                 
                               <tr><td><input type="checkbox"></td>
                                    <td><a href="organization-edit.php">Salesforce Inc</a></td>
                                    <td><a href="organization-edit.php">Operating Company </a></td>
									<td><a href="organization-edit.php">United States /  California/ San Francisco</a></td>
                                    <td><a href="organization-edit.php">www.salesforce.com</a></td>
                                    <td><a href="organization-edit.php">CRM</a></td>
                                    <td><a href="organization-edit.php">John@gmail.com</a></td>
                                    <td><a href="organization-edit.php">+1 (510) 456-789</a></td>
                                    <td><a href="organization-edit.php">$3.5m</a></td>
                                    <td class="text-center">
                                         <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   
                                              
                                                     <li><a href="#"   data-toggle="modal" data-target="#modal_team"> New Contact </a></li>
                                                      <li><a href="#"  data-toggle="modal" data-target="#modal_task">New Task</a></li>
                                                       <li><a href="#"  data-toggle="modal" data-target="#modal_eng_target">Engagement Target</a></li> 
                                                        <li><a href="engagement-create.php">New Engagement </a></li>
                                                         <li><a href="create_sell_side_screen.php"> New  Screen</a></li>
                                                         
                                                         <li><a href="#" data-toggle="modal" data-target="#modal_notes"> New Note/Attachment</a></li>
														 
														 <li><a href="#"> Delete</a></li>
														 

                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                        </div>

                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   