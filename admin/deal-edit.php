<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">View Deal Details</span> </h4>
            </div>

           <div class="heading-elements col-md-3">
             <div class="heading-btn-group">
                               <a href="#" class="dt-button buttons-selected btn btn-default" onclick="showata()" >Edit </a>

                                <a href="#" class="dt-button buttons-selected btn btn-default">Delete</a>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="">
                <div class="col-lg-12 background-white">
                <div class="ibox ">
                   
                    <div class="ibox-content" style="border:none;">

                       

                               <div class="row" id="hideData">
                                <div class="col-md-4">
                                  <label class="text-teal">Deal Detail</label>
                                  <table class="table table-striped">
                                    <tr class="showOnhoverEditIcon">
                                      <td><strong>Date</strong></td>
                                      <td> 23/03/2019</td>
                                         <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                      
                                    <tr class="showOnhoverEditIcon">
                                      <td><strong>Buyers/Investors</strong></td>
                                      <td>TRIFECTA CAPITAL</td>
                                        <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                    </tr>
                                    <tr class="showOnhoverEditIcon">
                                      <td><strong> Target/Issuers</strong></td>
                                      <td>Passman SAS </td>
                                        <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                    </tr>
                                   
                                  </table>

                                </div>
                                <div class="col-md-4">
                                  <label style="float: right;"> &nbsp;</label>
                                    <table class="table table-striped ">
                                    <tr class="showOnhoverEditIcon">
                                      <td><strong>Type</strong></td>
                                      <td> Private Placement  </td>
                                        <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                    </tr>
                                    <tr class="showOnhoverEditIcon">
                                      <td><strong>Trans Value </strong></td>
                                      <td>11.6</td>
                                      <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                    </tr>
                                    <tr class="showOnhoverEditIcon">
                                      <td><strong>EV/REV</strong></td>
                                      <td>2.3</td>
                                      <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                    </tr>
                                   
                                  </table>
                                </div>
                                     <div class="col-md-4">
                                  <label style="float: right;"> &nbsp;</label>
                                    <table class="table table-striped ">
                                    <tr class="showOnhoverEditIcon">
                                      <td><strong>EV/EBITDA</strong></td>
                                      <td> 4 </td>
                                        <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                    </tr>
                                    <tr class="showOnhoverEditIcon">
                                      <td><strong>Classification</strong></td>
                                      <td>Software </td>
                                      <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                    </tr>
                                    
                                   
                                  </table>
                                 
                                </div>
                                  
                              </div>

                           
                            <div class="col-lg-12 " style="display: none;" id="editData">
                              
                    
              <div class="row" >
                     <h5>Account  Information</h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class="">Date <sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide Your Company name" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                         <input type="date" name="" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label> Buyers/Investors <sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Select Your Official Company Website" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Target/Issuers   <sup>*</sup><a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide Contact No " data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
                     

                        <div class="row" >
                     
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class=""> Type <sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Provide Your Phone" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                           <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Private Placement</option>
                        <option value="opt2"> Merger/Acquisition</option>
                       
                        
                                                
                    </select>
            </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label> Trans Value  <sup>*</sup> <a href="#" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="What is your Company Revenue ?" data-original-title=""><i class="fa fa-info-circle"></i></a></label>
                              <input type="text" name="" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label> EV/REV  </label>
                      <input type="text" class="form-control">
                      </div>
                    </div>
                  </div>
               
              
     <div class="row" >
                    

                         <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label> EV/EBITDA  <sup>*</sup></label>
                          
                          
                            
                        
                          <input type="text" class="form-control">
                      </div>
                    </div>

                    <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>Classification <sup>*</sup></label><br>


                        <input type="checkbox" name=""> Software
                         <input type="checkbox" name=""> CRM
                           <input type="checkbox" name=""> Events
                         <input type="checkbox" name=""> Utilities<br/>
                           <input type="checkbox" name=""> Workday
                        
                    <br/><br/>
                        <input type="checkbox" name=""> Software
                         <input type="checkbox" name=""> IT
                           <input type="checkbox" name=""> SAP
                         <input type="checkbox" name=""> Utilities<br/>
                           <input type="checkbox" name=""> Workday

                    
                      </div>
                    </div>
                   
                  
                       <div class="col-md-4">
                        <label>&nbsp;</label>
                         <div class="form-group">
                        <label>  Business Description  <sup>*</sup></label>
                          <label style="display: block;"></label>
                          <label style="display: block;"></label>
                            
                            <textarea class="form-control"></textarea>
                        
                         
                      </div>
                    </div>
                    
                  </div>


                  <div class="row">
                    
                    <div class="col-lg-12">
					
					 <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				
				<a href="#"><button type="button" class="btn btn-primary legitRipple"> Save</button></a>
				
				<a href="deal-edit.php"><button type="button" class="btn btn-primary legitRipple"> Cancel</button></a>
				
				
                       
						
	
                </div>
            </div>
        </div> 
					
					
                  
            </div>        


                  </div> 
                  </div>

                    </div>
                </div></div></div>
           
            
       
       <script type="text/javascript">
  
  function showata(){
    document.getElementById('editData').style.display = "block";
    document.getElementById('hideData').style.display = "none";
  }
</script>
<?php require_once('footer.php'); ?>

   