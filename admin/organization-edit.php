<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-9">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">Organization</span> - Beganto </h4>
            </div>

           <div class="heading-elements col-md-3">
            
							<div class="heading-btn-group">
                                <a href="#" class="dt-button buttons-selected btn btn-default"  onclick="showata()"  >Edit </a>
                                <a href="#" class="dt-button buttons-selected btn btn-default">Delete</a>
                                
                            </div>
						</div>
            </div>
        
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">
					<!----content-prev--->
					


                        			<div class="row" id="hideData">
                        				<div class="col-md-4">
                        					<label class="text-teal">Details</label>
                        					<table class="table table-striped">
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Account Name</strong></td>
                        							<td>Salesforce Inc</td>
                        								<td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        							</tr>
													
													<tr class="showOnhoverEditIcon">
                        							<td><strong>Type</strong></td>
                        							<td>Operating Company</td>
                        							<td class="text-teal"><div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
													
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Country/State/Province</strong></td>
                        							<td>United States /  California/ San Francisco</td>
                        							<td class="text-teal"><div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
												
												<tr class="showOnhoverEditIcon">
                        							<td><strong>Zip/Postal</strong></td>
                        							<td>3456794</td>
                        							<td class="text-teal"><div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
												
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Website</strong></td>
                        							<td>www.salesforce.com</td>
                        							<td class="text-teal"><div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
												
                        						<tr class="showOnhoverEditIcon">
                        							<td> <strong>	Classification</strong></td>
                        							<td> CRM</td>
                        							<td class="text-teal"><div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
												
												
                        					</table>

                        				</div>
										
                        				<div class="col-md-4">
                        					<label> &nbsp;</label>
                        						<table class="table table-striped ">
												<tr class="showOnhoverEditIcon">
                        							 <td><strong>Revenue</strong></td>
                                        <td>$3.5m</td>
                                          <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
												
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>No Of Employee</strong></td>
                                      <td> 8</td>
                                      <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Primary Contact's Phone</strong></td>
                        							<td>+1 (001) 456-789</td>
                        							<td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        						<tr class="showOnhoverEditIcon">
                        							<td><strong>Created By</strong></td>
                                      <td> Admin 28/01/2020</td>
                                      <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
                        						<tr class="showOnhoverEditIcon">
                        							 <td><strong>Last Modified By</strong></td>
                                        <td>Admin 28/01/2020</td>
                                          <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>
												
												
											<tr class="showOnhoverEditIcon">
                        							 <td><strong>Account Source</strong></td>
                                        <td>Google</td>
                                          <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                        						</tr>	
                        					</table>
                        				</div>
										
										
										
                        					<div class="col-md-4">
                        					<label> &nbsp;</label>

                                    <table class="table" >
                                      
                                       <tr class="showOnhoverEditIcon" >
                                        <td width="20%"><strong>Description</strong></td>
                                        <td width="80%" >Salesforce.com, inc. is an American cloud-based software company headquartered in San Francisco, California. It provides customer relationship management service and also sells a complementary suite of enterprise applications focused on customer service, marketing automation, analytics, and application development. It is a software Company Locates in Usa Created Software , Web Design , Web Develeopment   , Software testing , CRM , ROI , Digital Marketing </td>
                                          <td class="text-teal"> <div class="ic"><i class="fa fa-pencil " onclick="showata()"></i></div></td>
                                      </tr>
                                     


                                    </table>
                                    
                                        <a href="" class="btn btn-default" style="float: right; margin-top: 30px;">View More</a>

                        				</div>

                        			</div>





  <!---edit screen--->
		  
		  <div class="col-lg-12 edit-screen"  id="editData">
                        			
                      
                   <div class="row" >
				   
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class="">Company Name <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Please Select the Primary Object Of Screen"><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
					
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label>Website<sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Select Your Official Company Website"><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
					
                    <div class="col-md-4">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Phone <sup>*</sup><a href="#"data-toggle="tooltip" data-placement="top" title="" data-original-title="Provide Contact No"><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
					
                  </div>

          
                     

                         <div class="row" >
                     <h5 style="background: #F3F2F2; padding-left: 10px;"></h5>
                    <div class="col-md-4">
                      <label>&nbsp;</label>
                      <div class="form-group ">
                        <label class="">Phone No 1 <sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Provide Your Secondary Phone No"><i class="fa fa-info-circle"></i></a></label>
                        <input type="text" class="form-control">
                      </div>
                    </div>
                    <div class="col-md-4">
                       <label>&nbsp;</label>
                        <div class="form-group">
                        <label>Revenue<sup>*</sup> <a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="What is your Company Revenue?"><i class="fa fa-info-circle"></i></a></label>
                                   <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Revenue</option>
                        <option value="opt2">>10</option>
                        <option value="opt3">10-50</option>
                        <option value="opt4">50-100</option>
                        <option value="opt5">>100</option>
                                                
                    </select>
            </div>
                      </div>
                    </div>
                    <div class="col-md-4">
                         <label>&nbsp;</label>
                      <div class="form-group">
                        <label>Account Source <sup>*</sup><a href="#" data-toggle="tooltip" data-placement="top" title="" data-original-title="Provide Your Account Source"><i class="fa fa-info-circle"></i></a></label>
                        <div class="form-group">
                <select name="select" class="form-control input-md">
                        <option value="opt1">Web</option>
                        <option value="opt2">Google</option>
                        
                                                
                    </select>
              </div>
                      </div>
                    </div>
                  </div>

                   

                  <div class="row" >
                     <h5></h5>
                    <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>Company Description<sup>*</sup></label>
                        
                        <textarea class="form-control border"   rows="1" cols="10"></textarea>
                      </div>
                    </div>
                   
                     <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>No of Employee<sup>*</sup></label>
                         
                            
                        
                          <input type="text" class="form-control">
                      </div>
                    </div>
					
					
					
					
                       <div class="col-md-4">
					   
					   
      
					   
					   
                        <label>&nbsp;</label>
                    <label class="display-block text-semibold">Account Owner<sup>*</sup></label>
                    <select id="done"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Ownership">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      </select>
                    </div>
                    
                  </div>
<!--=======================================================Row Design  ====================================------------>


                       <div class="row">
                    
                    <div class="col-md-4" >
                      <label>Address<sup>*</sup></label>
                    <div class="form-group">
                       
                       <label>Street</label>
                        
                        <textarea class="form-control border"   rows="1" cols="10"></textarea>
                      </div>
                    </div>
                   
                     <div class="col-md-4" >
                      
                    <div class="form-group">
                        <label>City<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div></div>
					  
						<div class="col-md-4" >
                       <div class="form-group">
                        <label>Zip/Postal Code<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div>
                    </div></div>
					
					<div class="row">
                       <div class="col-md-4">
                           
                         <div class="form-group">
                        <label>State/Province</label>
                        
                          <input type="text" class="form-control">
                      </div>
					  </div>
					  
					   <div class="col-md-4">
                       <div class="form-group">
                        <label>Country<sup>*</sup></label>
                        
                          <input type="text" class="form-control">
                      </div>
                    
                    </div>
                    
                  </div>


                                     
              <div class="row">
              

                    <div class="col-md-6">
                       <label>&nbsp;</label>
                      <div class="form-group">
              <label class="display-block text-semibold">Entity Type</label>
              <label class="checkbox-inline">
                <input type="checkbox" id="ocCk" onchange="valueChanged()">
                Operating Company
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="invCk" onchange="valueChanged()">
                Investor
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="lenCk" onchange="valueChanged()">
                Lender
              </label>

              <label class="checkbox-inline">
                <input type="checkbox" id="otrCk" onchange="valueChanged()">
                Other
              </label>
            </div>
                    </div>

                     <div class="col-md-5">
                 
                    </div>


                      
        <div class="row" id="oc">
                    <div class="col-md-12">
            <fieldset class="customFieldset" >
                
              <h6 style="color: #a976f5;"> Operating Company</h6>         
            
                <div class="row " >
                <div class="col-md-5">
                  <div class="row">
                  <!--   -->
                  <div class="col-md-6">
                    <div class="form-group mt-3">
                    <label class="display-block text-semibold">Company Type</label>
                    <label class="radio-inline">
                      <input type="radio" name="radio-inline-left" class="styled" checked="checked">
                      Large Co
                    </label>

                    <label class="radio-inline">
                      <input type="radio" name="radio-inline-left" class="styled">
                      Prospect
                    </label>
                  </div>
                    
                  </div>
                  <div class="col-md-6">
                    <label>&nbsp;</label>
                     <div class="form-group">
                  <label>Publicly Traded? <sup>*</sup></label>
                  <input name="" type="checkbox" value="">
                  </div>
                  </div>
                  </div>
                </div>

                   <div class="col-md-3">

                         <div class="form-group">
                  <label>Parent Company / Investors / PrivateEquity <sup>*</sup></label>
                      <select id="done1"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Parent Company">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      <optgroup label="Investors">
                        <option value="IL" >PrivateEquity</option>
                        <option value="IA">Investors</option>
                        <option value="KS">Parent</option>
                      
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        <option value="MA">Massachusetts</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                  </div>
                  
                </div>

                   <div class="col-md-3">

                      <div class="form-group">
                  <label>Subsidiary Companies / Affiliate Companies<sup>*</sup></label>           
                    <select id="done2"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Subsidiary Companies</option>
                        <option value="CO">Affiliate Companies</option>
                        <option value="ID">TBP</option>
                      
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >Subsidiary Companies</option>
                        <option value="IA">Affiliate Companies</option>
                        <option value="KS">TBP</option>
                        <option value="KY">Kentucky</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        
                      </optgroup>
                    </select>
                  </div>
                
                </div>


                
                </div>
            
               
            </fieldset>
                    </div>
                  </div>

                         <div class="row" id="inv">
                    <div class="col-md-12">
            <fieldset class="customFieldset">
              
              <h6 style="color: #a976f5;">Investor</h6>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="multi-select-full">
					<label>&nbsp;</label>
                       <select id="done3"  class="selectpicker form-control" multiple data-done-button="true">
                        <option value="1">PE/Buyout</option>
                        <option value="2">Venture Capital</option>
                        <option value="3">Fund of Fund</option>
                        <option value="4">Institutional LP</option>
                        <option value="5">Individual Accredited</option>
                        <option value="6">Individual UHNW</option>
                        <option value="7">Family Office</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-4">  
                  <div class="form-group">
                    <label>Parent Company / Investors / PrivateEquity </label>
                    <select id="done4"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Parent</option>
                        <option value="CO">Investors</option>
                        <option value="ID">PrivateEquity</option>
                        
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >PrivateEquity</option>
                        <option value="IA">Investors</option>
                        <option value="KS">Parent</option>
                      
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        <option value="MA">Massachusetts</option>
                        <option value="WV">West Virginia</option>
                      </optgroup>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                      <div class="form-group">
                  <label>Subsidiary Companies / Affiliate Companies<sup>*</sup></label>           
                    <select id="done5"  class="selectpicker form-control" multiple data-done-button="true">
                      <optgroup label="Mountain Time Zone">
                        <option value="AZ">Subsidiary Companies</option>
                        <option value="CO">Affiliate Companies</option>
                        <option value="ID">TBP</option>
                      
                      </optgroup>
                      <optgroup label="Central Time Zone">
                        <option value="IL" >Subsidiary Companies</option>
                        <option value="IA">Affiliate Companies</option>
                        <option value="KS">TBP</option>
                        <option value="KY">Kentucky</option>
                      </optgroup>
                      <optgroup label="Eastern Time Zone">
                        <option value="CT">Connecticut</option>
                        <option value="FL" >Florida</option>
                        
                      </optgroup>
                    </select>
                  </div>
              </div>
              </div>

            

            </fieldset>
                    </div>
                  </div>

                        <div class="row" id="len">
                    <div class="col-md-12">
            <fieldset class="customFieldset">
              
              <h6 style="color: #a976f5;">Lender</h6>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="multi-select-full">
                        <select class="form-control" name="color2" onchange='CheckColors2(this.value);'>
                        <option value="1">Working capital</option>
                        <option value="2">Senior Debt</option>
                        <option value="3">Mz Debt</option>
                        <option value="4">Venture Debt</option>
                        <option value="5" onclick="showOther()">Other</option>
                      </select>
                    </div>
                    </div>
                </div>
                <div class="col-md-6"><input  type="text" name="color2" placeholder="Enter If Other Type" id="color2" class="form-control" style="display:none;"></div>
              </div>
            </fieldset>
             <script type="text/javascript">
function CheckColors2(val){
 var element=document.getElementById('color2');
 if(val=='5')
   element.style.display='block';
 else  
   element.style.display='none';
}

</script> 
                    </div>
                  </div>

                     <div class="row" id="otr">
                    <div class="col-md-12">
            <fieldset class="customFieldset">
             
              <h6 style="color: #a976f5;">Other</h6>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <div class="multi-select-full">
                     <select class="form-control" name="color" onchange='CheckColors(this.value);'>
                        <option value="1">Accountants</option>
                        <option value="2">Lawyers</option>
                        <option value="3">Investment Banker/Broker</option>
                        <option value="4">Media</option>
                        <option value="5" onclick="showOther1()">Other</option>
                      </select>
                    </div>
                    </div>
                </div>
                 <div class="col-md-6"><input  type="text" name="color" placeholder="Enter If Other Type" id="color" class="form-control" style="display:none;"></div>
              </div>
            </fieldset>

                    </div>
                  </div>

            <script type="text/javascript">
function CheckColors(val){
 var element=document.getElementById('color');
 if(val=='5')
   element.style.display='block';
 else  
   element.style.display='none';
}

</script> 
                     


                  </div>
                       



  <div class="row" >
                     <h5> </h5>
                    <div class="col-md-4" >
                      <label>&nbsp;</label>
                    <div class="form-group">
                        <label>Business Category <sup>*</sup></label>
                    


                        <div class="radio-box border" >
                        
                        <div class="form-group text-muted" style="border:1px solid #ddd; padding: 10px;" >
                    <label class="display-block text-semibold">Software</label>
                    <label class="checkbox-inline">
                      <input type="checkbox" checked="checked">
                      IT
                    </label>

                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Cloud Infrastructure
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      CRM

                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Data Analytics

                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      HR/HCM


                    </label>
                      <label class="checkbox-inline">
                      <input type="checkbox">
                      Supply Chain



                    </label>
                    <a href="" class="display-block" style="padding: 20px;" data-toggle="modal" data-target="#treeModal">More</a>
              </div>

                      </div>
                    </div>
                    </div>
                   
                     <div class="col-md-4" >
                      <label>&nbsp;</label>
                   <div class="form-group">
                        <label>&nbsp;</label>
                       

                        <div class="radio-box border">
                        
                        <div class="form-group text-muted" style="border:1px solid #ddd; padding: 20px;" >
                    <label class="display-block text-semibold">IT Services</label>
                    <label class="checkbox-inline">
                      <input type="checkbox" checked="checked">
                      IT
                    </label>

                    <label class="checkbox-inline">
                      <input type="checkbox">
                      CRM
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      HR/HCM
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox">
                      Supply Chain
                    </label>

                    <a href="" class="display-block" style="padding: 20px;" data-toggle="modal" data-target="#treeModal1">More</a>
                  </div>

                      </div>
                    </div>
                    </div>
                       <div class="col-md-4">
                            <label>&nbsp;</label>
                   <div class="form-group">
                        <label>&nbsp;</label>
                       

                        <div class="radio-box border">
                        
                        <div class="form-group text-muted" style="border:1px solid #ddd; padding:30px 20px;" >
                    <label class="display-block text-semibold">Others</label>
                

                   <input type="text" name="" class="form-control">
                  
 <a href="" class="display-block" style="padding: 0px;" data-toggle="modal" data-target="#treeModal1">&nbsp;</a>
                  </div>

                      </div>
                    </div>


                    </div>
                    
                  </div>


                  <div class="row mt-5 mb-5" >
                  	
                  	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				
				<a href="organization-edit.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"> Save</button></a>
				
				<a href="organization-edit.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"> Cancel</button></a>
				
                </div>
            </div>

                  </div>
		  <!---edit screen--->
 

</div>


                   


<div class="modal fade" id="treeModal" tabindex="-1" role="dialog" aria-labelledby="treeModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="treeModalLabel">Select Industries</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="tree">
      <ul>
      <li class="jstree-open">Software
        <ul>
          <li>IT
            <ul>
              <li>IT (Database-related)</li>
              <li>IT (Network)</li>
            </ul>
          </li>
          <li>Cloud Infrastructure</li>
          <li>CRM
            <ul>
              <li>CRM (Marketing)</li>
              <li>IT (Events)</li>
            </ul>
          </li>
          <li>Data Analytics</li>
          <li>HR/HCM</li>
          <li>Supply Chain</li>
        </ul>
      </li>
      </ul>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="treeModal1" tabindex="-1" role="dialog" aria-labelledby="treeModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="treeModalLabel1">Select Industries</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div id="tree1">
      <ul>
      <li class="jstree-open">IT Services
        <ul>
          <li>IT</li>
          <li>CRM</li>
          <li>HR/HCM</li>
          <li>Supply Chain</li>
          <li>Cloud</li>
          <li>Data Analytics</li>
        </ul>
      </li>
      </ul>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

          

                 
                      


                        <!---	======================================================contact  row 1======================================== -->

                        	<div class="organization-tab">
                        		<div class="col-md-6">
                        				<div class="panel panel-white">
									<div class="panel-heading">
										<h6 class="panel-title">
											<a data-toggle="collapse" href="#collapsible-controls-group1" class="text-bold text-teal"><i class="fa fa-address-book-o"></i> Contact  </a>
										</h6>
										
										<div class="heading-elements">

							<ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_team" ><i class=" icon-plus3"></i> New Contact</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>

										
					                	</div>  
									</div>
									<div id="collapsible-controls-group1" class="panel-collapse collapse ">
										<div class="panel-body">
											
										<table class="table" id="makeEditable">
                      <thead>
                            <tr>
                              
                            <th><input type="checkbox"></th>
                              <th>Contact Name</th>
                              <th>Title</th>
                              <th>Email</th>
                              <th>Phone</th>
                                <th>Action</th>
                            </tr>  
                        </thead>  
                        <tbody>
                          <tr>
                            
                         <td><input type="checkbox"></td>
                           <td>Kees Wurth</td>
                            <td>CEO / Owner</td>
                             <td>mail@mail.com</td>
                             <td>123247439</td>
                              
                              </tr>
                               <tr>
                              <td><input type="checkbox"></td>
                            <td>Kees Wurth</td>
                            <td>CEO / Owner</td>
                             <td>mail@mail.com</td>
                             <td>123247439</td>
                                
                              </tr>
							  
							  <tr>
                            
                         <td><input type="checkbox"></td>
                           <td>Kees Wurth</td>
                            <td>CEO / Owner</td>
                             <td>mail@mail.com</td>
                             <td>123247439</td>
                              
                              </tr>

						 <tr>
                            
                         <td><input type="checkbox"></td>
                           <td>Kees Wurth</td>
                            <td>CEO / Owner</td>
                             <td>mail@mail.com</td>
                             <td>123247439</td>
                              
                              </tr>


						 <tr>
                            
                         <td><input type="checkbox"></td>
                           <td>Kees Wurth</td>
                            <td>CEO / Owner</td>
                             <td>mail@mail.com</td>
                             <td>123247439</td>
                              
                              </tr>	  

                        </tbody>      
                    </table>
										
										</div>
									</div>
									
								
								</div>
                        		</div>
                        		
                        	
                            <div class="col-md-6">
                            <div class="panel panel-white">
                  <div class="panel-heading" style="background-color: #eee; padding: 10px 10px;" >
                    <h6 class="panel-title">
                      <a class="collapsed text-bold text-indigo" data-toggle="collapse" href="#tab7"> <i class="fa fa-bullseye"></i> Engagement  </a>
                    </h6>

                    <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="engagement-create.php"><i class=" icon-plus3"></i> New Engagement</a></li>
                                                    
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
                  </div>
                  <div id="tab7" class="panel-collapse collapse">
                    <div class="panel-body">
                      <table class="table">
                        <thead>
                          <tr>
                           
                             <th>Title</th>
							 <th>Purpose</th>
							 <th>Revenue</th>
							 <th>Status</th>
							 <th>Active Matches</th>
							 <th>New Matches</th>
							 
                              <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            
                            <td><a href="engagements.php">Velocity</a></td>
							<td><a href="engagements.php">Buy huge capital company</a></td>
                            <td><a href="engagements.php">$3.5m</a></td>
							<td><a href="engagements.php">Active</a></td>
							<td><a href="view-buy-side-screen-active-screen.php">8</a></td>
							<td><a href="view-buy-side-screen.php">7</a></td>
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php">View Blind Profile</a></li>
												   
												   <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php">Archived</a></li>
													
													<li><a class="dropdown-item" href="#">Update Status</a></li>
													
													
													<li><a class="dropdown-item" href="engagement-create.php">Add engagment target</a></li>
			   


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
							
                          </tr>
						  
						  
						  <tr>
                            
                            <td><a href="engagements.php">Velocity</a></td>
							<td><a href="engagements.php">Buy huge capital company</a></td>
                            <td><a href="engagements.php">$3.5m</a></td>
							<td><a href="engagements.php">Active</a></td>
							<td><a href="view-buy-side-screen-active-screen.php">8</a></td>
							<td><a href="view-buy-side-screen.php">7</a></td>
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php">View Blind Profile</a></li>
												   
												   <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php">Archived</a></li>
													
													<li><a class="dropdown-item" href="#">Update Status</a></li>
													
													<li> <a class="dropdown-item" href="#"> Comments</a></li>
													
													<li><a class="dropdown-item" href="#">Add engagment target</a></li>
			   


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
							
                          </tr>
						  
						  <tr>
                            
                            <td><a href="engagements.php">Velocity</a></td>
							<td><a href="engagements.php">Buy huge capital company</a></td>
                            <td><a href="engagements.php">$3.5m</a></td>
							<td><a href="engagements.php">Active</a></td>
							<td><a href="view-buy-side-screen-active-screen.php">8</a></td>
							<td><a href="view-buy-side-screen.php">7</a></td>
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php">View Blind Profile</a></li>
												   
												   <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php">Archived</a></li>
													
													<li><a class="dropdown-item" href="#">Update Status</a></li>
													
													
													<li><a class="dropdown-item" href="engagement-create.php">Add engagment target</a></li>
			   


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
							
                          </tr>
						  
						  <tr>
                            
                            <td><a href="engagements.php">Velocity</a></td>
							<td><a href="engagements.php">Buy huge capital company</a></td>
                            <td><a href="engagements.php">$3.5m</a></td>
							<td><a href="engagements.php">Active</a></td>
							<td><a href="view-buy-side-screen-active-screen.php">8</a></td>
							<td><a href="view-buy-side-screen.php">7</a></td>
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="view-buy-side-screen-blind-profile.php">View Blind Profile</a></li>
												   
												   <li><a class="dropdown-item" href="view-buy-side-screen-archived-screen.php">Archived</a></li>
													
													<li><a class="dropdown-item" href="#">Update Status</a></li>
													
													
													<li><a class="dropdown-item" href="engagement-create.php">Add engagment target</a></li>
			   


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
							
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                            </div>
							
							
                            <div class="col-md-6" style="display:none">
                            <div class="panel panel-white">
                  <div class="panel-heading" style="background-color: #eee; padding: 10px 10px;" >
                    <h6 class="panel-title">
                      <a class="collapsed text-bold text-indigo" data-toggle="collapse" href="#tab17"> <i class="icon-target"></i> Engagement Target </a>
                    </h6>

                    <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="engagement-create.php"><i class=" icon-plus3"></i> New Engagement Target</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
                  </div>
                  <div id="tab17" class="panel-collapse collapse">
                    <div class="panel-body">
                      <table class="table">
                        <thead>
                          <tr>
                            
                             <th>Title</th>
							 <th>Purpose</th>
							 <th>Revenue</th>
							 <th>Status</th>
							 
                              <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            
                            <td><a href="engagements.php">Velocity</a></td>
							<td><a href="engagements.php">Buy huge capital company</a></td>
                            <td><a href="engagements.php">$3.5m</a></td>
							<td><a href="engagements.php">Active</a></td>
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												    <a class="dropdown-item" href="view-buy-side-screen-active-screen.php">Active Matches</a>
												   
												   <a class="dropdown-item" href="view-buy-side-screen.php">New Matches</a>
												   
												   <a class="dropdown-item" href="view-buy-side-screen-blind-profile.php">View Blind Profile</a>
												   
												   <a class="dropdown-item" href="view-buy-side-screen-archived-screen.php">Archived</a>
            <li><a class="dropdown-item" href="#">Update Status</a>
           <li> <a class="dropdown-item" href="#"> Comments</a>
			<li><a class="dropdown-item" href="#">Add engagment target</a></li>
			   


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
							
                          </tr>
						  
						  
						  <tr>
                           
                            <td><a href="engagements.php">Velocity</a></td>
							<td><a href="engagements.php">Buy huge capital company</a></td>
                            <td><a href="engagements.php">$3.5m</a></td>
							<td><a href="engagements.php">Active</a></td>
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="view-buy-side-screen-active-screen.php">Active Matches</a>
												   
												   <a class="dropdown-item" href="view-buy-side-screen.php">New Matches</a>
												   
												   <a class="dropdown-item" href="view-buy-side-screen-blind-profile.php">View Blind Profile</a>
												   
												   <a class="dropdown-item" href="view-buy-side-screen-archived-screen.php">Archived</a>
            <li><a class="dropdown-item" href="#">Update Status</a>
           <li> <a class="dropdown-item" href="#"> Comments</a>
			<li><a class="dropdown-item" href="#">Add engagment target</a></li>
			   


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
							
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                            </div>
							
                            <div class="col-md-6">
                          <div class="panel panel-white">
                  <div class="panel-heading" style="background-color: #eee; padding: 10px 10px;">
                    <h6 class="panel-title">
                      <a class="collapsed text-bold text-green" data-toggle="collapse" href="#tabactivities">  <i class="fa fa-arrows"></i> Activities</a>
                    </h6>

                    
                  <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_activity"><i class=" icon-plus3"></i> New Task</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
                  </div>
                  <div id="tabactivities" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                        <thead>
                          <tr>
                          
                            <th>Subject</th>
							<th>Due Date</th>
                              <th>Contact</th>
							  <th>Assigned To</th>
                                <th>Related To</th>
								<th>Status</th>
                                <th>Completion Date</th>
								
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="btnDelete" data-id="1">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                           <td><a href="open-activities.php">Complete</a></td>
                            <td><a href="open-activities.php">10/02/2019</a></td>
							
                             
                          </tr>
						  
						  
						  
                           <tr class="btnDelete" data-id="2">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                            <td><a href="open-activities.php">Cancelled</a></td>
                            <td><a href="open-activities.php">10/02/2019</a></td>
							
                             
                          </tr>
						  
						  
						    <tr class="btnDelete" data-id="3">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                            <td><a href="open-activities.php">Complete</a></td>
                            <td><a href="open-activities.php">10/02/2019</a></td>
							
                             
                          </tr>
						  
						  
						    <tr class="btnDelete" data-id="4">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                            <td><a href="open-activities.php">Cancelled</a></td>
                            <td><a href="open-activities.php">10/02/2019</a></td>
							
                             
                          </tr>
						  
						  
						    <tr class="btnDelete" data-id="5">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                            <td><a href="open-activities.php">Complete</a></td>
                            <td><a href="open-activities.php">10/02/2019</a></td>
							
                            
                          </tr>
						  
						   
						  

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                            </div>
							
							<!----open activities---->
							<div class="col-md-6">
                          <div class="panel panel-white">
                  <div class="panel-heading" style="background-color: #eee; padding: 10px 10px;">
                    <h6 class="panel-title">
                      <a class="collapsed text-bold text-green" data-toggle="collapse" href="#tabopenactivities">  <i class="fa fa-area-chart"></i>Open Activities</a>
                    </h6>

                    <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_activity"><i class=" icon-plus3"></i> New Task</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
                  </div>
                  <div id="tabopenactivities" class="panel-collapse collapse">
                    <div class="panel-body">
                        <table class="table">
                        <thead>
                          <tr>
                          
                            <th>Subject</th>
							<th>Due Date</th>
                              <th>Contact</th>
							  <th>Assigned To</th>
                                <th>Related To</th>
								<th>Status</th>
                                <th>Action</th>
								<th></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="btnDelete" data-id="1">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                           <td><a href="open-activities.php">Open</a></td>
                            
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                  
												   
												   <li><a class="dropdown-item" href="#"  id="bEdit" type="button" class="btn btn-sm btn-default legitRipple" onclick="rowEdit(this);">Edit</a></li>
												   
												   <li><a class="dropdown-item" href="#"  data-toggle="modal" data-target="#modal_notes">Add Notes</a></li>
												   
												   <li class="dropdown-submenu">
												<a tabindex="-1" href="#">Update Status <i class="icon-menu9"></i></a>
												   <ul class="dropdown-menu">
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Open</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Complete</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Cancelled</a></li>
													
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">In Progess</a></li>
													</ul>
												   
												   </li>
												   
												   <li><a  class="btnDelete" href="">Delete</a></li>
												   
												   
           
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                             
                          </tr>
						  
						  
						  
                           <tr class="btnDelete" data-id="2">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                            <td><a href="open-activities.php">In Progess</a></td>
                            
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                  
												   
												   <li><a class="dropdown-item" href="#">Edit</a></li>
												   
												   <li><a class="dropdown-item" href="#"  data-toggle="modal" data-target="#modal_notes">Add Notes</a></li>
												   
												   <li class="dropdown-submenu">
												<a tabindex="-1" href="#">Update Status <i class="icon-menu9"></i></a>
												   <ul class="dropdown-menu">
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Open</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Complete</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Cancelled</a></li>
													
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">In Progess</a></li>
													</ul>
												   
												   </li>
												   
												   <li><a  class="btnDelete" href="">Delete</a></li>
												   
												   
           
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                             
                          </tr>
						  
						  
						    <tr class="btnDelete" data-id="3">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                            <td><a href="open-activities.php">Open</a></td>
                            
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                  
												   
												   <li><a class="dropdown-item" href="#">Edit</a></li>
												   
												   <li><a class="dropdown-item" href="#"  data-toggle="modal" data-target="#modal_notes">Add Notes</a></li>
												   
												   <li class="dropdown-submenu">
												<a tabindex="-1" href="#">Update Status <i class="icon-menu9"></i></a>
												   <ul class="dropdown-menu">
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Open</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Complete</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Cancelled</a></li>
													
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">In Progess</a></li>
													</ul>
												   
												   </li>
												   
												   <li><a  class="btnDelete" href="">Delete</a></li>
												   
												   
           
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                             
                          </tr>
						  
						  
						    <tr class="btnDelete" data-id="4">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                            <td><a href="open-activities.php">Open</a></td>
                            
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                  
												   
												   <li><a class="dropdown-item" href="#">Edit</a></li>
												   
												   <li><a class="dropdown-item" href="#"  data-toggle="modal" data-target="#modal_notes">Add Notes</a></li>
												   
												   <li class="dropdown-submenu">
												<a tabindex="-1" href="#">Update Status <i class="icon-menu9"></i></a>
												   <ul class="dropdown-menu">
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Open</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Complete</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Cancelled</a></li>
													
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">In Progess</a></li>
													</ul>
												   
												   </li>
												   
												   <li><a class="dropdown-item"  class="btnDelete" href="">Delete</a></li>
												   
												   
           
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                             
                          </tr>
						  
						  
						    <tr class="btnDelete" data-id="5">
                           
                            <td><a href="open-activities.php">Email: Introducton</a></td>
							<td><a href="open-activities.php">10/12/2019</a></td>
                            <td><a href="open-activities.php">David</a></td>
                            <td><a href="open-activities.php">Jhonty</a></td>
							<td><a href="open-activities.php">Velocity Project</a></td>
                            <td><a href="open-activities.php">Complete</a></td>
                            
							<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                  
												   
												   <li><a class="dropdown-item" href="#">Edit</a></li>
												   
												   <li><a class="dropdown-item" href="#"  data-toggle="modal" data-target="#modal_notes">Add Notes</a></li>
												   
												   <li class="dropdown-submenu">
												<a tabindex="-1" href="#">Update Status <i class="icon-menu9"></i></a>
												   <ul class="dropdown-menu">
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Open</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Complete</a></li>
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">Cancelled</a></li>
													
													<li><a tabindex="-1" href="#"  data-toggle="modal" data-target="#modal_update-status">In Progess</a></li>
													</ul>
												   
												   </li>
												   
												   <li><a class="dropdown-item"  class="btnDelete" href="">Delete</a></li>
												   
												   
           
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                            
                          </tr>
						  
						   
						  

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                            </div>
							
							<!--- open activities---->
                          
                        		<div class="col-md-6">
                        			<div class="panel panel-white">
									<div class="panel-heading crd-head" style="background-color: #eee; padding: 10px 10px;" >
										<h6 class="panel-title">
											<a class="collapsed text-bold text-success" data-toggle="collapse" href="#tab3"><i class="fa fa-paperclip"></i> Notes and Attachments</a>
										</h6>

										
										 <div class="heading-elements">
                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="#"  data-toggle="modal" data-target="#modal_notes" ><i class=" icon-plus3"></i> New Notes</a></li>
                                                    <li><a href="#"><i class="icon-cross3"></i> Delete</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
									</div>
									<div id="tab3" class="panel-collapse collapse ">
										<div class="panel-body">
										<table class="table" id="makeEditable2">
                        <thead>
                          <tr>
                            
                             <th>Type</th>
							 <th>Name</th>
							 <th>Notes</th>
                              <th>Modified On</th>
							  <th>Modified By</th>
							  <th>Action</th>
                          </tr> 
                          </thead>
                          <tbody>
                            <tr>
                              <td>Details Correction</td>
							  <td>Sanjay Grover</td>
							  <td>Address Change Of Company</td>
							  <td>03/03/2020</td>
							  <td>Rahul Gupta</td>
                              
                            </tr>
							
							<tr>
                              <td>Details Correction</td>
							  <td>Sanjay Grover</td>
							  <td>Address Change Of Company</td>
							  <td>03/03/2020</td>
							  <td>Rahul Gupta</td>
                              
                            </tr>
							
							<tr>
                              <td>Details Correction</td>
							  <td>Sanjay Grover</td>
							  <td>Address Change Of Company</td>
							  <td>03/03/2020</td>
							  <td>Rahul Gupta</td>
                              
                            </tr>
							
							<tr>
                              <td>Details Correction</td>
							  <td>Sanjay Grover</td>
							  <td>Address Change Of Company</td>
							  <td>03/03/2020</td>
							  <td>Rahul Gupta</td>
                              
                            </tr>
							
							<tr>
                              <td>Details Correction</td>
							  <td>Sanjay Grover</td>
							  <td>Address Change Of Company</td>
							  <td>03/03/2020</td>
							  <td>Rahul Gupta</td>
                              
                            </tr>
                            
                          </tbody>          
                      </table> </div></div></div></div>
					  
				

<!---Campagain-->
<div class="col-md-6">
                        			<div class="panel panel-white">
									<div class="panel-heading crd-head" style="background-color: #eee; padding: 10px 10px;" >
										<h6 class="panel-title">
											<a class="collapsed text-bold text-success" data-toggle="collapse" href="#camptab3"><i class="fa fa-paperclip"></i>Campaigns</a>
										</h6>

										
										 <div class="heading-elements">
                      <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="create-campaign.php"><i class=" icon-plus3"></i> Add New Campaign</a></li>
                                                    
                                                </ul>
                                            </li>
                                        </ul>
                            </div>
									</div>
									<div id="camptab3" class="panel-collapse collapse ">
										<div class="panel-body">
										<table class="table">
                        <thead>
                          <tr>
                            
                             <th>Subject</th>
							 <th>Campaign Status</th>
							 <th>Bounced</th>
                              <th>Open Count</th>
							  <th>Last Open Date</th>
							  <th>Last Response</th>
                          </tr> 
                          </thead>
                          <tbody>
                            <tr>
                              <td>Velocity : Production Promotion</td>
							  <td>Running</td>
							  <td>2 times</td>
							  <td>16</td>
							  <td>10/23/2020</td>
							  <td>Contact By Email</td>
                              
                            </tr>
							
							 <tr>
                              <td>Velocity : Production Promotion</td>
							  <td>Running</td>
							  <td>2 times</td>
							  <td>16</td>
							  <td>10/23/2020</td>
							  <td>Contact By Email</td>
                              
                            </tr>
							
							
							 <tr>
                              <td>Velocity : Production Promotion</td>
							  <td>Running</td>
							  <td>2 times</td>
							  <td>16</td>
							  <td>10/23/2020</td>
							  <td>Contact By Email</td>
                              
                            </tr>
							
							
							 <tr>
                              <td>Velocity : Production Promotion</td>
							  <td>Running</td>
							  <td>2 times</td>
							  <td>16</td>
							  <td>10/23/2020</td>
							  <td>Contact By Email</td>
                              
                            </tr>
							
							 <tr>
                              <td>Velocity : Production Promotion</td>
							  <td>Running</td>
							  <td>2 times</td>
							  <td>16</td>
							  <td>10/23/2020</td>
							  <td>Contact By Email</td>
                              
                            </tr>
                            
                          </tbody>          
                      </table> </div></div></div></div>

<!---campagain--->				
					  
					 

              <!--=======================================================Row Design  ====================================------------>
                  <div class="row" >
                    
				<div class="col-md-12">
                        			<div class="panel panel-white engagment">
									<div class="panel-heading " style="background-color: #eee; padding: 10px 10px;" >
										<h6 class="panel-title ">
											<a class="collapsed text-bold text-info" data-toggle="collapse" href="#collapsible-controls-group2" ><i class="fa fa-credit-card-alt"></i>Buy Side Screen </a>
										</h6>

										<div class="heading-elements">
											  <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="create_buy_side_screen.php">New Screen </a></li>
                                                   
                                                </ul>
                                            </li>
                                        </ul>
					                	</div>
									</div>
									<div id="collapsible-controls-group2" class="panel-collapse collapse">
										<div class="panel-body">
                      <div id="exTab2">
  <div class="panel-default"> 
    <div class="panel-heading">
      <div class="panel-title">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#organization-edit1" data-toggle="tab">Screen Details</a>
          </li>
          <li><a href="#organization-edit2" data-toggle="tab">Blind Profile</a>
          </li>
          <li><a href="#organization-edit3" data-toggle="tab">Active Matches</a>
          </li>
		  
		  <li><a href="#organization-edit4" data-toggle="tab">New Matches</a>
          </li>
		  
		  <li><a href="#organization-edit5" data-toggle="tab">Archived Matches</a>
          </li>
		  
		  
        </ul>
      </div>
    </div>
    
    <div class="panel-body">
      <div class="tab-content">
        <div class="tab-pane active" id="organization-edit1">
		<div class="content">
								<div class="row">
								
								
								
								<div class="col-md-12">
								<p class="company-name">Velocity Media Partners</p>
								<p>Velocity Want to buy big capital companies with great turnover. $5M+ ARR SaaS business in construction industry</p>
								
								
								
								<h3 class="company-name">Company Description</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
									<div class="col-md-12">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold"> ideal size of the company you are targeting</span></td>
												<td>500+ </td>
											</tr>
											<tr>
												<td><span class="text-semibold">kind of debt do you provide </span></td>
												<td>Senior debt </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Are you interested in looking at turnaround/restructuring opportunities?</span></td>
												<td>Yes  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Industry classification : </span></td>
												<td>Agriculture > Cisco  </td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td>Software > IT > Cloud</td>
											</tr>
											
											
											
										
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td>Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2 class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>

						
						
					</div>
											
								</div>
</div>

							</div>
		
         
		  
		  <div class="row">
        	<div class="col-md-12">
			
			
			
			
            	<div class="form-group thanks-button text-right">
				
				
			<a href="edit-buy-side-screen.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
				
				<a href="#"><button type="button" style="background-color:#e6e5e5; color:#000;" class="btn btn-default legitRipple">Archive</button></a>
				
                </div>
            </div>
        </div> 
		  
		  
		  
        </div>
		
		
        <div class="tab-pane" id="organization-edit2">
          
		  
		  <div class="content" style="margin-bottom: 20px;">
								<div class="row" style="margin-bottom: 20px;">
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-8">
								<p class="company-name">Velocity Media Partners</p>
								<br/>
								<h3> Project Brilliant is a 2000+ well captilized global digital transformation service firm.</h3>
								<p>Vlocity Industry Cloud CRM Apps deliver the superior, industry-specific, digital and omnichannel customer experiences to transform your business. Vlocity is a leading provider of industry-specific cloud and mobile software for the world’s top communications, media and entertainment, energy, utilities, insurance, health, and government organizations</p>
								</div>
								
								<div class="col-md-4">
								<h2 class="company-name">Company Financials</h2>
								<table class="table table-striped table-bordered">
						<tr>
						<th><b>Target Revenue</b></th>
						
						<th><b>Target EBITDA</b> </th>
						
						

						</tr>
						
					<tr>
					<td>10-25M</td>
					
					<td>$1-5M</td>
					
					</tr>
					
					
					</table>
								</div>
								
								</div>
								<div class="row">
								
								
									<div class="col-md-4">
									<h3>Target Offerings</h3>
									<ul>
									
									<li>Cloud Services</li>
									
									<li>PaaS infrastructure</li>
									
									<li>Microsoft Stack</li>
									
									<li>SaaS cloud ERP infrastructure</li>
									
									<li>SAP Hana</li>
									<li>Success factor</li>
									<li>Workday</li>
									<li>Salesforce</li>
									
									<li>SaaS Cloud verticle Applications</li>
									
									<li>BFSI</li>
									<li>CPG</li>
									<li>Retail</li></ul>
									
									
									</div>
									
									
									<div class="col-md-4">
									
										
										<h3>Targets Verticals</h3>
										
										<ul>
										<li>Technology</li>
										<li>BFSI</li>
										<li>Retail</li>
										<li>CPG</li>
										<li>Utilities</li>
										
										</ul>
										
										<h3>Targets Customers</h3>
									<ul>
									<li>US Based </li>
									<li>Global 2000 companies </li>
									<li>Referenceable Client Base</li>
									
									</ul>
									
									
									
										
									</div>
									
									<div class="col-md-4">
									
										
										
										
									
									<h3>Target Business</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US </li>
									
									</ul>
										
										<h3>Investment considerations</h3>
										
										<ul>
										<li>Owner/Founder run companies</li>
										<li>Preferable no instituional capital</li>
										<li>Managment team to stay for at least 2 years post transaction</li>
										<li>Company should have well trained Empolyees</li>
										<li>Company should had great reputation in market</li>
										<li>Company should be in growth turnover</li>
										
										
										</ul>
										
									</div>
									
									
										
											
								</div>
<div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
				<a href="edit-buy-side-blind-profile.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
				
				
                </div>
            </div>
        </div> 
							</div>
		  
		  
		  
        </div>
		
		
        <div class="tab-pane" id="organization-edit3">
          <div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														<th>Download</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
												</tbody>
											</table>
										</div>
        </div>
		
		<div class="tab-pane" id="organization-edit4">
          <div class="row">
											<h1 style="text-align:center;margin-bottom: 20px;">Thank You!</h1>
										<p>Now that you have created a Buy Side Screen, you will be assigned a TBP account manager who will schedule a time will walk you though the next steps.</p>
										<p>Meanwhile, to assist the process you can complete the <a href="create-buy-side-blind.php" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Click here to open form">following forms to start work on a Blind Profile</a>. Your Blind Profile will be published after review with TBP.</p>
										<div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Revenue</th>
														<th>EBITDA</th>
														<th>Email</th>
														
														<th>Actions</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td>
														
														
														
														
														<a data-toggle="modal" href="#myModal">Sell side screen</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														
														<td> <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal12">Pursue</button>
						 
						</div></td>
														
						
						
													</tr>
													
													
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#myModal">buy side engagment </a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														<td> <div id="pageMessages"><button class="btn btn-success" onclick="createAlert('','your profile moved to archived.','','success',true,true,'pageMessages');">Pursue</button></div>
						  
						</td>
														
														
						
						
													</tr>
													
												</tbody>
											</table>
											
											
										</div>
								</div>
									
		
        </div>
		
		<div class="tab-pane" id="organization-edit5">
          <div>
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														
														<th>Target<sup>,</sup>s Revenue</th>
														<th>Target<sup>,</sup>s EBITDA</th>
														<th>Active It</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
												</tbody>
											</table>										</div>
        </div>
		
      </div>
    </div>
  </div>
										</div>
									</div>
								</div>
                        		</div>	

								
	<div class="row">							
  <div class="col-md-12">
                        			<div class="panel panel-white engagment">
									<div class="panel-heading " style="background-color: #eee; padding: 10px 10px;" >
										<h6 class="panel-title ">
											<a style="color:green;" class="collapsed text-bold text-success" data-toggle="collapse" href="#collapsible-controls-group3" ><i style="color:green;" class="fa fa-search-plus"></i>Sell Side Screen </a>
										</h6>

										<div class="heading-elements">
											  <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                <ul class="dropdown-menu dropdown-menu-right">
                                                   <li><a href="create_sell_side_screen.php">New Screen </a></li>
                                                   
                                                </ul>
                                            </li>
                                        </ul>
					                	</div>
									</div>
									<div id="collapsible-controls-group3" class="panel-collapse collapse">
										<div class="panel-body">   

									<div id="exTab2">
  <div class="panel panel-default"> 
    <div class="panel-heading">
      <div class="panel-title">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#sell-side-organization1" data-toggle="tab">Screen Details</a>
          </li>
          <li><a href="#sell-side-organization2" data-toggle="tab">Blind Profile</a>
          </li>
          <li><a href="#sell-side-organization3" data-toggle="tab">Active Matches</a>
          </li>
		  
		  <li><a href="#sell-side-organization4" data-toggle="tab">New Matches</a>
          </li>
		  
		  <li><a href="#sell-side-organization5" data-toggle="tab">Archived Matches</a>
          </li>
		  
		  
        </ul>
      </div>
    </div>
    
    <div class="panel-body">
      <div class="tab-content ">
        <div class="tab-pane active" id="sell-side-organization1">
		<div class="row">
								
								<div class="col-md-12">
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
								<p class="subtitle">Over 150,000 companies, both big and small, are growing their business with Salesforce</p>
								<p><b>Headquater : </b> Business Address. SALESFORCE TOWER 415 MISSION STREET 3RD FL. SAN FRANCISCO CA 94105.</p>
								
								
								<h3 class="company-name">Company Description</h3>
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
									<div class="col-md-12">
									
					
					<div class="col-md-7">
					<h2  class="company-name">Company Regarding FAQ,S</h2>
					<table class="table table-striped">
											
											
											
											
											<tr>
												<td><span class="text-semibold">Ideal timeline for selecting a termsheet and starting diligence?</span></td>
												<td>As soon as Possible </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Company undergoing a turnaround/restructuring? </span></td>
												<td>Yes </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Management team expect to stay on after the deal?	</span></td>
												<td>Management stay  </td>
											</tr>
												
												
												<tr>
												<td><span class="text-semibold">Targets Vertical area of focus : </span></td>
												<td>Software</td>
											</tr>
										
											<tr>
												<td><span class="text-semibold"> Which end market(s) does this company sell into? :</span></td>
												<td>Software > IT > Cloud</td>
											</tr>
											
											
												
											
											
											
											<tr>
												<td><span class="text-semibold">What is your relationship to this company? * </span></td>
												<td>CEO/CFO</td>
											</tr>
													<tr>
												<td><span class="text-semibold">Targets offerings and verticals * </span></td>
												<td>Software > IT</td>
											</tr>
											
											<tr>
												<td><span class="text-semibold">Transaction Objective </span></td>
												<td> Currency management & aka dated exchange rates</td>
											</tr>
											
										</table>
					
					</div>
				
										
										
										
										<div class="col-md-5">
									
					
						
							<h2  class="company-name">Company Financials</h2>
							
							<table class="table table-striped table-bordered">
						<tr>
						<th><b>Historical financials audited</b></th>
						
						<th><b>Fiscal year end</b> </th>
						
						

						</tr>
						
					<tr>
					<td>Yes</td>
					
					<td>January</td>
					
					</tr>
					
					
					</table>
	<br/><br/><br/>
						
						<table class="table table-striped table-bordered">
						<tr>
						<th>FY </th>
						<th><b>2018 Actuals</b></th>
						<th><b>2019 Actuals</b></th>
						<th><b>2020 Projected</b></th>
						

						</tr>
						
					<tr>
					<td><b>Revenue</b></td>
					<td>$13</td>
					<td>$15</td>
					<td>$18</td>
					</tr>
					
					<tr>
					<td><b>EBITDA</b></td>
					<td>$13</td>
					<td>$25</td>
					<td>$38</td>
					</tr>
					</table>
					</div>
											
								</div>

											
								</div>
		
         
		  
		  <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                       <a href="edit-sell-side-screen.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
				
				<a href="#"><button type="button" style="background-color:#e6e5e5; color:#000;" class="btn btn-default legitRipple">Archive</button></a>
	
	
                </div>
            </div>
        </div> 
		  
		  
		  
        </div>
		
		
        <div class="tab-pane" id="sell-side-organization2">
          
		  
		  <div class="content">
								<div class="row">
								<div class="col-md-12"><p style="float:right;"><a href="https://www.interactivebrokers.com/download/FDBrokersGetStarted.pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p></div>
								<div class="col-md-8">
								
								<p class="company-name">Salesforce.com (SFDC) Q4 FY19</p>
							<br/>
								
								
								<p>Salesforce.com announced its acquisition of MapAnything, the leader of Location-of-Things (LoT) solutions, to enhance its Sales and Service Clouds with a market-leading, geo-analytical, intelligence platform. Salesforce owned 14% of MapAnything prior to acquiring the remaining 86% for $225 million. The deal officially closed on May 31, 2019.</p>
								</div>
								
								
								<div class="col-md-4">
								<h3 class="company-name">Business Details</h3>
									<ul>
									<li><span class="text-semibold">HQ. :</span> US East</li>
									<li><span class="text-semibold">Offices :</span> US, India, Latin America</li>
									<li><span class="text-semibold">Employees :</span> 200+</li>
									</ul>
								</div>
								
									<div class="col-md-6">
									<h3 class="company-name">Service Offerings</h3>
									<div>
									
									<li>Product Devlopment, testing and Maintenance</li>
									<li>Security & infrastructure Services</li>
									<li>Prepackaged software implementation and Support</li>
									<li>Digital Assets Management</li>
									<li>Microsoft, Oracle, and SAP Maintenance</li>
									<li>Software devlopment  & Maintenance</li>
									
									</ul></div>
									
									
										<h3 class="company-name">Customers</h3>
										<div>
									<ul>
									<li>100+ mid market and enterprise customers </li>
									<li>Marquee accounts, including fortune 500 companies </li>
									<li>Primary verticle
									<ul>
									<li>BFSI</li>
									<li>Education</li>
									<li>Health Care</li>
									</ul>
									
									</li>
									
									</ul>
									
									</div></div>
									
									<div class="col-md-6">
									
									
										<table class="table">
											<h3 class="company-name">Financials</h3>
											
											<tr>
											<td style="border:none;"></td>
												<td style="border:none; font-size:24px;"><b><u>2017</u></b></td>
												</tr>
												
												
												<tr>
												<td><span class="text-semibold"> Revenue :</span></td>
												<td>$110m - $120m</td>
											</tr>
											<tr>
												<td><span class="text-semibold"> Revenue Growth : </span></td>
												<td>10%YoY </td>
											</tr>
											<tr>
												<td><span class="text-semibold">Gross Margin :	</span></td>
												<td>30-40%  </td>
											</tr>
												<tr>
												<td><span class="text-semibold"> EBITDA Margin :</span></td>
												<td>5-10%</td>
											</tr>
												
										
										</table>
										
										
										
										
										
										<h3 class="company-name">Investment Consideration</h3>
										
										<div><ul>
										<li>Diversified Offerings</li>
										<li>No Concentration, Largest customer is <10% of revenue</li>
										<li>Founder owned and operated</li>
										<li>Capital efficent, No Outside Capital</li>
										<li>40% recurring revenue</li>
										<li><b>Scale:</b> Sizeable enough to win deals in the mid-market but there are many oppourtunities to scale further via M&amp;A</li>
										
										<li><b>Profitable growth:</b> The company can improve this by partnering with the right private equity frim</li> 
										</ul></div>
										
									</div>
										
											
								</div>
 <div class="row">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                        <a href="edit-sell-side-blind-profile.php"><button type="button" style="background-color:green; color:#fff;" class="btn btn-default legitRipple"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a>
						
						
	
	
                </div>
            </div>
        </div> 
							</div>
		  
		  
        </div>
		
		
        <div class="tab-pane" id="sell-side-organization3">
          <div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														
														<th>Headline</th>
														<th>Company Name</th>
														<th>Contact Name</th>
														<th>Target’s Check Size</th>
														<th>Target’s Revenue</th>
														<th>Target’s EBITDA</th>
														
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Salesforce</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Jhone watson</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-5M</a></td>
															       
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Jhone watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$10-15M</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce.com (SFDC)</a></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Jhone watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Salesforce Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Salesforce</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Jhone watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Nivita Construction</a></td>
														<td><a data-toggle="modal" href="#myModal">Nivita</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Sanju watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Nivita Production</a></td>
														<td><a data-toggle="modal" href="#myModal">Nivita</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Sanju watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Active</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														<td><a data-toggle="modal" href="#myModal">TZP Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$20-25M</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
												   <a class="dropdown-item" href="#"><i class="fa fa-upload" aria-hidden="true"></i> Upload Document</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-cog" aria-hidden="true"></i> Configure NDA</a>
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-exchange" aria-hidden="true"></i> Convert to Engagment</a>
												   
												   
												   
												   <a class="dropdown-item" href="#"><i class="fa fa-magic" aria-hidden="true"></i>  Pass</a>
            <li><a class="dropdown-item" href="#"><i class="fa fa-envelope" aria-hidden="true"></i> Send Email</a>
           <li> <a class="dropdown-item" href="#"><i class="fa fa-comment-o" aria-hidden="true"></i>  Comments</a>
			<li><a class="dropdown-item" href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Sign NDA</a></li>
			<li><a class="dropdown-item" href="#"><i class="fa fa-file-o" aria-hidden="true"></i>   Download Signed NDA</a></li>     


                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
													</tr>
													
												</tbody>
											</table>
										</div>
        </div>
		
		<div class="tab-pane" id="sell-side-organization4">
          <div class="row">
											<h1 style="text-align:center;margin-bottom: 20px;">Thank You!</h1>
										
										<p>Now that you have created a Sell Side Screen, you will be assigned a TBP account manager who will schedule a time will walk you though the next steps.</p>
										<p>Meanwhile, to assist the process you can complete the <a href="sell-side-blind-profile.php" data-popup="popover" title="" data-placement="top" data-trigger="hover" data-content="Click here to open form">following forms to start work on a Blind Profile</a>. Your Blind Profile will be published after review with TBP.</p>
										<div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														<th>Target’s Check Size</th>
														<th>Target’s Revenue</th>
														<th>Target’s EBITDA</th>
														<th>Email</th>
														
														<th>Actions</th>
														
														
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#myModal">Buy side screen</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1-5M</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														
														<td> <button type="button" class="btn btn-success" data-toggle="modal" data-target="#myModal12">Pursue</button>
						 
						</div></td>
														
						
						
													</tr>
													
													
													<tr class="bg-default">
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">DRAFT</span></td>
														<td><a data-toggle="modal" href="#myModal">Sell side engagment </a></td>
														
														<td><a data-toggle="modal" href="#myModal">$10-15M</a></td>
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.3M</a></td>
														
														
														
														<td><a href="mailto:seller@gmail.com"><i style="color:green; padding-right:15px; font-size:25px; text-align:center;" class="fa fa-envelope" aria-hidden="true"></i> </a></td>
														
														<td> <div id="pageMessages"><button class="btn btn-success" onclick="createAlert('','your profile moved to archived.','','success',true,true,'pageMessages');">Pursue</button></div>
						  
						</td>
														
														
						
						
													</tr>
													
												</tbody>
											</table>
											
											
										</div>
								</div>
									
		
        </div>
		
		<div class="tab-pane" id="sell-side-organization5">
          <div>
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														<th>Date</th>
														<th>Status</th>
														<th>Headline</th>
														<th>Company Name</th>
														<th>Contact Name</th>
														<th>Target Check Size</th>
														<th>Target<sup>,</sup>s Revenue</th>
														<th>Target<sup>,</sup>s EBITDA</th>
														<th>Active It</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">03-11-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Comvest Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$6m</a></td>
														
														<td><a data-toggle="modal" href="#myModal"> $1.3M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
														<tr class="bg-default">
														<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-15-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														<td><a data-toggle="modal" href="#myModal">TZP Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-25-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-26-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td>04-27-2020</td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-28-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														
														<td><a data-toggle="modal" href="#myModal">Velocity Media Partners</a></td>
														<td><a data-toggle="modal" href="#myModal">Velocity Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$13m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$3.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													<td><a data-toggle="modal" href="#myModal">04-29-2020</a></td>
													<td><span class="badge badge-default">Archived</span></td>
														<td><a data-toggle="modal" href="#myModal">TZP Group</a></td>
														<td><a data-toggle="modal" href="#myModal">TZP Limited</a></td>
														
														<td><a data-toggle="modal" href="#myModal">Akshay watson</a></td>
														<td><a data-toggle="modal" href="#myModal">$1-$5m</a></td>
														<td><a data-toggle="modal" href="#myModal">$3m</a></td>
														
														<td><a data-toggle="modal" href="#myModal">$1.5M</a></td>
														
														<td><label class="switch">
  <input type="checkbox">
  <span class="slider round"></span>
</label></td>
													</tr>
													
												</tbody>
											</table>										</div>
        </div>
		
      </div>
    </div>
  </div>
                        	</div>


                       </div></div></div> 


        

</div></div>



<script type="text/javascript">
  
  function showata(){
    document.getElementById('editData').style.display = "block";
    document.getElementById('hideData').style.display = "none";
  }
</script>

                        <!----contentprev--->

                    </div>
                </div>
            </div>
           
       

     
<?php require_once('footer.php'); ?>

   