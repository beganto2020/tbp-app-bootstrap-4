<?php require_once('header.php'); ?>
   
     <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2" href="#"><i class="fa fa-bars"></i> </a>
           
        </div>
            

        </nav>
        </div>

      
            
        <div class="wrapper wrapper-content animated fadeInRight">
			<div class="page-header-content row">
            <div class="page-title col-md-7">
              <h4><i class="fa fa-arrow-circle-left position-left"></i> <span class="text-semibold">View Campaign Details </span></h4>
            </div>

           <div class="heading-elements col-md-5">
               <div class="heading-btn-group">
                <div class="form-group thanks-button text-right">
                        
						<a class="btn  btn-default float-right mr-1" href="edit-campaign.php" role="button">Edit</a>
						<a class="btn  btn-default float-right mr-1" href="create-campaign.php" role="button">Create New Campaign</a>
						
                </div>
                                
                            </div>
            </div>
          </div>
		
		
            <div class="row">
                <div class="col-lg-12">
                <div class="ibox ">
                   
                    <div class="ibox-content">

                        <div class="panel panel-default"> 
    <div class="panel-heading">
      <div class="panel-title">
        <ul class="nav nav-tabs">
          <li class="active">
            <a href="#1" data-toggle="tab">Campaign Details</a>
          </li>
          <li><a href="#2" data-toggle="tab">Participants</a>
          </li>
         
        </ul>
      </div>
    </div>
    
    <div class="panel-body">
      <div class="tab-content ">
        <div class="tab-pane active" id="1">
		<div class="row">
								
								
						<div class="col-md-12">
								<p class="company-name">Project : Velocity</p>
								
								
								</div>
								
					<div class="col-md-8">
							
							<table class="table table-striped table-bordered">
						
					<tr>
					<td style="text-align:center;"><b>Type</b></td>
					<td style="text-align:center;">Spotlight</td>
					</tr>
						
					<tr>
					<td style="text-align:center;"><b>Realted To</b> </td>
					<td style="text-align:center;">Email marketing</td>
					</tr>
						
					<tr>
					<td style="text-align:center;"><b>Attachments</b> </td>
					<td style="text-align:center;">Stock.pdf, current market status.pdf, Employees.doc</td>
					
					</tr>
					<tr>
					<td style="text-align:center;"><b>Content</b> </td>
					<td style="text-align:center;">Autoresize anchor autolink charmap code codesample directionality fullpage help hr image imagetools insertdatetime link lists media nonbreaking pagebreak preview print searchreplace table template textpattern toc visualblocks visualchars</td>
					
					</tr>
					
					</table></div>		
					
					
					
					
					
					</div>
					
					
					
								
         
		  
		  <div class="row" style="margin-top:30px;">
        	<div class="col-md-12">
            	<div class="form-group thanks-button text-right">
                       <a href="#"><button type="button" class="btn btn-primary legitRipple">Publish</button></a>
					   
					   <a href="#"><button type="button" class="btn btn-primary legitRipple">Run</button></a>
					   
					   <a href="#"><button type="button" class="btn btn-primary legitRipple">Stop</button></a>
					   
					   <a href="#"><button type="button" class="btn btn-primary legitRipple">Complete</button></a>
				
			
	
	
                </div>
            </div></div></div>
        
		
		
		
		
        <div class="tab-pane" id="2">
          <div class="">
											
											<table class="table table-togglable table-hover">
												<thead>
													<tr>
													<th><input type="checkbox"></th>
														
														<th>Name</th>
														
														<th>Email</th>
														<th>Status</th>
														<th>Bounced</th>
														<th>Opened</th>
														<th>Opened Date</th>
														<th>Response</th>
														
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													
													
														<td>Sanjeev Grover</td>
														
														<td>Abc@gamil.com</td>
														
														<td>Published</td>
														
														<td>xyz@gmail.com</td>
															       
														<td>16 Times</td>
														
														<td>06-10-2019</td>
														
														<td><a data-toggle="modal" href="#myModal">06-15-2019</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right" style="list-style-type:none;">
                                                   <li>
												   <a class="dropdown-item" href="#">Schedule</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Un-Schedule</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Delete</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Add More</a>
												   
												   <ul style="list-style-type:none;">
            <li><a class="dropdown-item" href="#">Copy from Screen Matches</a></li>
           <li><a class="dropdown-item" href="#">Copy from Another Campaign</a></li>
			<li><a class="dropdown-item" href="#">Copy from Engagement</a></li>
			<li><a class="dropdown-item" href="#">Search</a></li>     


                                                </ul>
                                            </li></ul></li>
                                        </ul>
                                    </td>
													</tr>
													
														<tr class="bg-default">
													<td><input type="checkbox"></td>
													
													
														<td>Sanjeev Grover</td>
														
														<td>Abc@gamil.com</td>
														
														<td>Published</td>
														
														<td>xyz@gmail.com</td>
															       
														<td>16 Times</td>
														
														<td>06-10-2019</td>
														
														<td><a data-toggle="modal" href="#myModal">06-15-2019</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right" style="list-style-type:none;">
                                                   <li>
												   <a class="dropdown-item" href="#">Schedule</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Un-Schedule</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Delete</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Add More</a>
												   
												   <ul style="list-style-type:none;">
            <li><a class="dropdown-item" href="#">Copy from Screen Matches</a></li>
           <li><a class="dropdown-item" href="#">Copy from Another Campaign</a></li>
			<li><a class="dropdown-item" href="#">Copy from Engagement</a></li>
			<li><a class="dropdown-item" href="#">Search</a></li>     


                                                </ul>
                                            </li></ul></li>
                                        </ul>
                                    </td>
													</tr>
													
													<tr class="bg-default">
													<td><input type="checkbox"></td>
													
													
														<td>Sanjeev Grover</td>
														
														<td>Abc@gamil.com</td>
														
														<td>Published</td>
														
														<td>xyz@gmail.com</td>
															       
														<td>16 Times</td>
														
														<td>06-10-2019</td>
														
														<td><a data-toggle="modal" href="#myModal">06-15-2019</a></td>
														
														
														<td class="text-center">
                                        <ul class="icons-list">
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                    <i class="icon-menu9"></i>
                                                </a>

                                                   <ul class="dropdown-menu dropdown-menu-right" style="list-style-type:none;">
                                                   <li>
												   <a class="dropdown-item" href="#">Schedule</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Un-Schedule</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Delete</a></li>
												   
												   <li><a class="dropdown-item" href="#"> Add More</a>
												   
												   <ul style="list-style-type:none;">
            <li><a class="dropdown-item" href="#">Copy from Screen Matches</a></li>
           <li><a class="dropdown-item" href="#">Copy from Another Campaign</a></li>
			<li><a class="dropdown-item" href="#">Copy from Engagement</a></li>
			<li><a class="dropdown-item" href="#">Search</a></li>     


                                                </ul>
                                            </li></ul></li>
                                        </ul>
                                    </td>
													</tr>
													
											
												</tbody>
											</table>
										</div>
        </div>
		
								
		
        </div>
		
		
      </div>
    </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
       
       
<?php require_once('footer.php'); ?>

   